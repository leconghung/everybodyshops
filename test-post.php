<!doctype html>
<html lang="en-US">
    <head >
        <script>
    var require = {
        "baseUrl": "https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US"
    };
</script>
        <meta charset="utf-8"/>
<meta name="description" content="The unmistakable fragrance of pansies fills the air in an open hoop house at Laurel Nursery and Garden Center. The..."/>
<meta name="keywords" content="Magento, Varien, E-commerce"/>
<meta name="robots" content="index,follow"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<title>everybodyshops.com Plant pansies in early spring for instant color | EverybodyShops</title>
<link  rel="stylesheet" type="text/css"  media="all" href="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/mage/calendar.css" />
<link  rel="stylesheet" type="text/css"  media="all" href="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/css/styles-m.css" />
<link  rel="stylesheet" type="text/css"  media="all" href="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/css/ytextend.css" />
<link  rel="stylesheet" type="text/css"  media="all" href="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/css/yttheme.css" />
<link  rel="stylesheet" type="text/css"  media="all" href="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/css/css-fix.css" />
<link  rel="stylesheet" type="text/css"  media="all" href="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/css/custom.css" />
<link  rel="stylesheet" type="text/css"  media="all" href="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/Magento_Swatches/css/swatches.css" />
<link  rel="stylesheet" type="text/css"  media="all" href="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/FishPig_WordPress/css/wordpress.css" />
<link  rel="stylesheet" type="text/css"  media="screen and (min-width: 768px)" href="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/css/styles-l.css" />
<link  rel="stylesheet" type="text/css"  media="print" href="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/css/print.css" />
<link  rel="stylesheet" type="text/css"  media="all" href="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/mage/gallery/gallery.css" />
<script  type="text/javascript"  src="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/requirejs/require.js"></script>
<script  type="text/javascript"  src="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/mage/requirejs/mixins.js"></script>
<script  type="text/javascript"  src="https://www.everybodyshops.com/pub/static/version1495658372/_requirejs/frontend/Sm/market/en_US/secure/requirejs-config.js"></script>
<link  rel="canonical" href="https://www.everybodyshops.com/content/plant-pansies-now-for-instant-color/" />
<link  rel="icon" type="image/x-icon" href="https://www.everybodyshops.com/pub/media/favicon/stores/1/favicon.png" />
<link  rel="shortcut icon" type="image/x-icon" href="https://www.everybodyshops.com/pub/media/favicon/stores/1/favicon.png" />
        
<link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:700' />
<link  rel="stylesheet" type="text/css"  media="all" href="https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/css/config_1.css" />

<!--CUSTOM CSS-->
<style>
	/*garden*/
.category-everybody-gardens .header-style-5 .header-bottom .menu-horizontal-hd5 .sm_megamenu_wrapper_horizontal_menu .sm_megamenu_menu > li.sm_megamenu_actived > a, 
.category-everybody-gardens .header-style-5 .header-bottom .menu-horizontal-hd5 .sm_megamenu_wrapper_horizontal_menu .sm_megamenu_menu > li:hover > a {
	background: #7bae37 none repeat scroll 0 0;
}

.category-everybody-gardens .header-style-5 .header-bottom {
    border-bottom: 4px solid #7bae37;
}

.category-everybody-gardens .owl-controls .owl-nav div:hover, 
.category-everybody-gardens .page-title::before, 
.category-everybody-gardens button:hover, 
.category-everybody-gardens .cart-container .action.continue:hover, 
.category-everybody-gardens a.action.primary:hover, 
.category-everybody-gardens .sambar-inner::before,
.category-everybody-gardens .block .block-title::before,
.category-everybody-gardens .label-product.label-new,
.category-everybody-gardens .toolbar .modes .modes-mode.mode-grid:hover, 
.category-everybody-gardens .toolbar .modes .modes-mode.mode-grid.active,
.category-everybody-gardens .toolbar .modes .modes-mode.mode-list:hover, 
.category-everybody-gardens .toolbar .modes .modes-mode.mode-list.active,
.category-everybody-gardens .sm_quickview_handler:hover,
.category-everybody-gardens #sm_slider_price .ui-slider-range,
.category-everybody-gardens .toolbar .pages .pages-items .item.current,
.category-everybody-gardens .toolbar .pages .pages-items .item:hover,
.category-everybody-gardens .products-grid .item .item-inner .box-info .bottom-action .btn-action.btn-cart:hover,
.category-everybody-gardens .info-box-detail .product-options-bottom .box-tocart .fieldset .actions button, 
.category-everybody-gardens .info-box-detail .product-add-form .box-tocart .fieldset .actions button,
.category-everybody-gardens .vertical-style .resp-tabs-list li.resp-tab-active::before,
.category-everybody-gardens .review-form .action.submit.primary,
.category-everybody-gardens .detail-title::before {
	background-color:#7bae37;
}

.category-everybody-gardens .sm_quickview_handler:hover,
.category-everybody-gardens .toolbar .pages .pages-items .item.current,
.category-everybody-gardens .toolbar .pages .pages-items .item:hover{
	color:white;
	border-color:white !important;
}
.category-everybody-gardens .label-product.label-new::before {
	border-top:4px solid #7bae37;
}
.category-everybody-gardens a:hover,
.category-everybody-gardens .related-wrapper .owl-controls .owl-nav div:hover, 
.category-everybody-gardens .upsell-wrapper .owl-controls .owl-nav div:hover, 
.category-everybody-gardens .resp-vtabs .resp-tabs-list li:hover, 
.category-everybody-gardens .resp-vtabs .resp-tabs-list li.resp-tab-active,
.category-everybody-gardens .price-box .price,
.category-everybody-gardens .info-box-detail .product-info-stock-sku .available span{
	color:#7bae37;
}
.category-everybody-gardens #sm_slider_price .ui-slider-handle,
.category-everybody-gardens .fotorama__thumb-border,
.category-everybody-gardens .direction_rtl .breadcrumbs .items .item:hover, 
.category-everybody-gardens .breadcrumbs .items .item:hover,
.category-everybody-gardens .direction_rtl .breadcrumbs .items .item:hover::before, 
.category-everybody-gardens .breadcrumbs .items .item:hover::before, 
.category-everybody-gardens .direction_rtl .breadcrumbs .items .item:hover::after, 
.category-everybody-gardens .breadcrumbs .items .item:hover::after{
	border-color:#7bae37 !important;
}

.category-everybody-gardens .review-form .action.submit.primary:focus, 
.category-everybody-gardens .review-form .action.submit.primary:active, 
.category-everybody-gardens .review-form .action.submit.primary:hover {
	background-color:#444;
}

/*craves*/
.category-everybody-craves .header-style-5 .header-bottom .menu-horizontal-hd5 .sm_megamenu_wrapper_horizontal_menu .sm_megamenu_menu > li.sm_megamenu_actived > a, 
.category-everybody-craves .header-style-5 .header-bottom .menu-horizontal-hd5 .sm_megamenu_wrapper_horizontal_menu .sm_megamenu_menu > li:hover > a {
	background: #ea6823 none repeat scroll 0 0;
}

.category-everybody-craves .header-style-5 .header-bottom {
    border-bottom: 4px solid #ea6823;
}

.category-everybody-craves .owl-controls .owl-nav div:hover, 
.category-everybody-craves .page-title::before, 
.category-everybody-craves button:hover, 
.category-everybody-craves .cart-container .action.continue:hover, 
.category-everybody-craves a.action.primary:hover, 
.category-everybody-craves .sambar-inner::before,
.category-everybody-craves .block .block-title::before,
.category-everybody-craves .label-product.label-new,
.category-everybody-craves .toolbar .modes .modes-mode.mode-grid:hover, 
.category-everybody-craves .toolbar .modes .modes-mode.mode-grid.active,
.category-everybody-craves .toolbar .modes .modes-mode.mode-list:hover, 
.category-everybody-craves .toolbar .modes .modes-mode.mode-list.active,
.category-everybody-craves .sm_quickview_handler:hover,
.category-everybody-craves #sm_slider_price .ui-slider-range,
.category-everybody-craves .toolbar .pages .pages-items .item.current,
.category-everybody-craves .toolbar .pages .pages-items .item:hover,
.category-everybody-craves .products-grid .item .item-inner .box-info .bottom-action .btn-action.btn-cart:hover,
.category-everybody-craves .info-box-detail .product-options-bottom .box-tocart .fieldset .actions button, 
.category-everybody-craves .info-box-detail .product-add-form .box-tocart .fieldset .actions button,
.category-everybody-craves .vertical-style .resp-tabs-list li.resp-tab-active::before,
.category-everybody-craves .review-form .action.submit.primary,
.category-everybody-craves .detail-title::before {
	background-color:#ea6823;
}

.category-everybody-craves .sm_quickview_handler:hover,
.category-everybody-craves .toolbar .pages .pages-items .item.current,
.category-everybody-craves .toolbar .pages .pages-items .item:hover{
	color:white;
	border-color:white !important;
}
.category-everybody-craves .label-product.label-new::before {
	border-top:4px solid #ea6823;
}
.category-everybody-craves a:hover,
.category-everybody-craves .related-wrapper .owl-controls .owl-nav div:hover, 
.category-everybody-craves .upsell-wrapper .owl-controls .owl-nav div:hover, 
.category-everybody-craves .resp-vtabs .resp-tabs-list li:hover, 
.category-everybody-craves .resp-vtabs .resp-tabs-list li.resp-tab-active,
.category-everybody-craves .price-box .price,
.category-everybody-craves .info-box-detail .product-info-stock-sku .available span {
	color:#ea6823;
}
.category-everybody-craves #sm_slider_price .ui-slider-handle,
.category-everybody-craves .fotorama__thumb-border,
.category-everybody-craves .direction_rtl .breadcrumbs .items .item:hover, 
.category-everybody-craves .breadcrumbs .items .item:hover,
.category-everybody-craves .direction_rtl .breadcrumbs .items .item:hover::before, 
.category-everybody-craves .breadcrumbs .items .item:hover::before, 
.category-everybody-craves .direction_rtl .breadcrumbs .items .item:hover::after, 
.category-everybody-craves .breadcrumbs .items .item:hover::after{
	border-color:#ea6823 !important;
}
.category-everybody-craves .review-form .action.submit.primary:focus, 
.category-everybody-craves .review-form .action.submit.primary:active, 
.category-everybody-craves .review-form .action.submit.primary:hover {
	background-color:#444;
}


/*adventures*/
.category-everybody-adventures .header-style-5 .header-bottom .menu-horizontal-hd5 .sm_megamenu_wrapper_horizontal_menu .sm_megamenu_menu > li.sm_megamenu_actived > a, 
.category-everybody-adventures .header-style-5 .header-bottom .menu-horizontal-hd5 .sm_megamenu_wrapper_horizontal_menu .sm_megamenu_menu > li:hover > a {
	background: #005825 none repeat scroll 0 0;
}

.category-everybody-adventures .header-style-5 .header-bottom {
    border-bottom: 4px solid #005825;
}

.category-everybody-adventures .owl-controls .owl-nav div:hover, 
.category-everybody-adventures .page-title::before, 
.category-everybody-adventures button:hover, 
.category-everybody-adventures .cart-container .action.continue:hover, 
.category-everybody-adventures a.action.primary:hover, 
.category-everybody-adventures .sambar-inner::before,
.category-everybody-adventures .block .block-title::before,
.category-everybody-adventures .label-product.label-new,
.category-everybody-adventures .toolbar .modes .modes-mode.mode-grid:hover, 
.category-everybody-adventures .toolbar .modes .modes-mode.mode-grid.active,
.category-everybody-adventures .toolbar .modes .modes-mode.mode-list:hover, 
.category-everybody-adventures .toolbar .modes .modes-mode.mode-list.active,
.category-everybody-adventures .sm_quickview_handler:hover,
.category-everybody-adventures #sm_slider_price .ui-slider-range,
.category-everybody-adventures .toolbar .pages .pages-items .item.current,
.category-everybody-adventures .toolbar .pages .pages-items .item:hover,
.category-everybody-adventures .products-grid .item .item-inner .box-info .bottom-action .btn-action.btn-cart:hover,
.category-everybody-adventures .info-box-detail .product-options-bottom .box-tocart .fieldset .actions button, 
.category-everybody-adventures .info-box-detail .product-add-form .box-tocart .fieldset .actions button,
.category-everybody-adventures .vertical-style .resp-tabs-list li.resp-tab-active::before,
.category-everybody-adventures .review-form .action.submit.primary,
.category-everybody-adventures .detail-title::before {
	background-color:#005825;
}

.category-everybody-adventures .sm_quickview_handler:hover,
.category-everybody-adventures .toolbar .pages .pages-items .item.current,
.category-everybody-adventures .toolbar .pages .pages-items .item:hover{
	color:white;
	border-color:white !important;
}
.category-everybody-adventures .label-product.label-new::before {
	border-top:4px solid #005825;
}
.category-everybody-adventures a:hover,
.category-everybody-adventures .related-wrapper .owl-controls .owl-nav div:hover, 
.category-everybody-adventures .upsell-wrapper .owl-controls .owl-nav div:hover, 
.category-everybody-adventures .resp-vtabs .resp-tabs-list li:hover, 
.category-everybody-adventures .resp-vtabs .resp-tabs-list li.resp-tab-active,
.category-everybody-adventures .price-box .price,
.category-everybody-adventures .info-box-detail .product-info-stock-sku .available span{
	color:#005825;
}
.category-everybody-adventures #sm_slider_price .ui-slider-handle,
.category-everybody-adventures .fotorama__thumb-border,
.category-everybody-adventures .direction_rtl .breadcrumbs .items .item:hover, 
.category-everybody-adventures .breadcrumbs .items .item:hover,
.category-everybody-adventures .direction_rtl .breadcrumbs .items .item:hover::before, 
.category-everybody-adventures .breadcrumbs .items .item:hover::before, 
.category-everybody-adventures .direction_rtl .breadcrumbs .items .item:hover::after, 
.category-everybody-adventures .breadcrumbs .items .item:hover::after{
	border-color:#005825 !important;
}
.category-everybody-adventures .review-form .action.submit.primary:focus, 
.category-everybody-adventures .review-form .action.submit.primary:active, 
.category-everybody-adventures .review-form .action.submit.primary:hover {
	background-color:#444;
}



.footer-style-5 .socials-footer .socials-wrap ul li a {
font-size: 18px;
display: block;
width: 40px;
height: 40px;
text-align: center;
line-height: 40px;
color: #000000;
-webkit-border-radius: 50%;
border-radius: 50%;
transition: all 0.2s ease 0s;
-webkit-transition: all 0.2s ease 0s;
}

.footer-style-5 .socials-footer .socials-wrap ul li a:hover {
background-color:#565656 !important;
border-radius: 50%;
}

.footer-style-5 .socials-footer .socials-wrap ul li.facebook-social a,
.footer-style-5 .socials-footer .socials-wrap ul li.twitter-social a,
.footer-style-5 .socials-footer .socials-wrap ul li.instagram-social a,
.footer-style-5 .socials-footer .socials-wrap ul li.google-social a,
.footer-style-5 .socials-footer .socials-wrap ul li.pinterest-social a,
.footer-style-5 .socials-footer .socials-wrap ul li.linkedin-social a,
.footer-style-5 .socials-footer .socials-wrap ul li.youtube-social a{
	background-color:white;
}

.logo > img {
float:left;
}

/* Products Styles - Products you might be interested in... */
.post-entry div.featured-prod-container div.title-home h3 {
	padding-bottom:0.5em;
	}

.post-entry div.featured-prod-container div.slider-prod-container div.slider-prod.owl-carousel.owl-theme.owl-loaded div.owl-stage-outer div.owl-stage div.owl-item.active div.item.item-post div.info-blog div.date-post-title h3.postTitle a {
	color: #444444;
	font-size: 0.5em;
	font-weight: bold;
	}

.post-entry div.featured-prod-container div.slider-prod-container div.slider-prod.owl-carousel.owl-theme.owl-loaded div.owl-stage-outer div.owl-stage div.owl-item.active div.item.item-post div.image-blog.static-image {
	min-height:180px;
	}

div.post-entry div.featured-prod-container div.slider-prod-container {
	padding:5px; 
	border-bottom: 6px solid #5C5C5C;
	border-top: 4px solid #C6C6C6;
	}
.owl-carousel .owl-item {-moz-user-select: none;}
.slider-prod .item.item-post .image-blog {
	display:-webkit-box;
	display:-ms-flexbox;
	display:flex;
	-webkit-box-align:center;
	-ms-flex-align:center;
	align-items:center;
	} 
.slider-prod .item.item-post .image-blog{
	float: left;
	min-height: 1px;
	padding-left: 8px;
        padding-right: 8px;
	position: relative;
	}
.owl-theme .owl-controls {
	padding-top: 6px;
	}

/*sidebar wordpress adjunct compare products - hidden*/
div.col-lg-3.col-md-3 div.sidebar.sidebar-additional div.block.block-compare{ display:none;
}

/*Wordpress Plugin Page Edits*/
#maincontent > div > div > div > div.col-lg-9.col-md-9 > div.page-title-wrapper > h1 > span {
	font-size: 1.8em;
    font-weight: normal;
    line-height: 1em;
}
div.column.main div.post-entry div.post-image a img {
    margin-right: 20px !important;
	min-width: 320px;
	max-width: 870px;
	margin-bottom: 20px;	
}
div.column.main div.post-entry div.post-image a img::after {
    clear:both;
	margin-bottom: 20px;
}
.post-image {
    float: none;
    margin-right: 20px !important;
}</style>

<script type="text/javascript">
	require([
		'domReady!',
		'jquerybootstrap',
		'yttheme'
	], function ($) {
		//console.log('Test Done');
	});
</script>


<!--CUSTOM JS-->

<script type="text/javascript">
	require([
		'jquery',
		'domReady!'
	], function ($) {
		$('body').addClass("home-5-style");
		$('body').addClass("header-4-style");

		
			});

</script>

<!--LISTING CONFIG-->
<style>

{
	font-family:Droid Sans;
}
	
@media (min-width: 1200px) {

	.container{
		width: 1170px;
		max-width: 1170px;
	}


	/*==1 COLUMN==*/
	
	.col1-layout .category-product.products-grid .item{
		width: 25%;
	}
	
	.col1-layout .category-product.products-grid .item:nth-child(4n+1){
		clear:both;
	}
	
	/*==2 COLUMNS==*/
	
	.col2-layout .category-product.products-grid .item{
		width: 33.333333333333%;
	}
	
	.col2-layout .category-product.products-grid .item:nth-child(3n+1){
		clear:both;
	}

	/*==3 COLUMNS==*/
	
	.col3-layout .category-product.products-grid .item{
		width: 50%;
	}
	
	.col3-layout .category-product.products-grid .item:nth-child(2n+1){
		clear:both;
	}
}

@media (min-width: 992px) and (max-width: 1199px) {

	/*==1 COLUMN==*/
	
	.col1-layout .category-product.products-grid .item{
		width: 25%;
	}
	
	.col1-layout .category-product.products-grid .item:nth-child(4n+1){
		clear:both;
	}

	/*==2 COLUMNS==*/
	
	.col2-layout .category-product.products-grid .item{
		width: 33.333333333333%;
	}
	
	.col2-layout .category-product.products-grid .item:nth-child(3n+1){
		clear:both;
	}

	/*==3 COLUMNS==*/
	
	.col3-layout .category-product.products-grid .item{
		width: 100%;
	}
	
	.col3-layout .category-product.products-grid .item:nth-child(1n+1){
		clear:both;
	}
}

@media (min-width: 768px) and (max-width: 991px) {

	/*==1 COLUMN==*/
	
	.col1-layout .category-product.products-grid .item{
		width: 33.333333333333%;
	}
	
	.col1-layout .category-product.products-grid .item:nth-child(3n+1){
		clear:both;
	}

	/*==2 COLUMNS==*/
	
	.col2-layout .category-product.products-grid .item{
		width: 33.333333333333%;
	}
	
	.col2-layout .category-product.products-grid .item:nth-child(3n+1){
		clear:both;
	}

	/*==3 COLUMNS==*/
	
	.col3-layout .category-product.products-grid .item{
		width: 100%;
	}
	
	.col3-layout .category-product.products-grid .item:nth-child(1n+1){
		clear:both;
	}
}

@media (min-width: 481px) and (max-width: 767px) {

	/*==1 COLUMN==*/
	
	.col1-layout .category-product.products-grid .item{
		width: 50%;
	}
	
	.col1-layout .category-product.products-grid .item:nth-child(2n+1){
		clear:both;
	}

	/*==2 COLUMNS==*/
	
	.col2-layout .category-product.products-grid .item{
		width: 50%;
	}
	
	.col2-layout .category-product.products-grid .item:nth-child(2n+1){
		clear:both;
	}

	/*==3 COLUMNS==*/
	
	.col3-layout .category-product.products-grid .item{
		width: 100%;
	}
	
	.col3-layout .category-product.products-grid .item:nth-child(1n+1){
		clear:both;
	}
}

@media (max-width: 480px) {

	/*==1 COLUMN==*/
	
	.col1-layout .category-product.products-grid .item{
		width: 100%;
	}
	
	.col1-layout .category-product.products-grid .item:nth-child(1n+1){
		clear:both;
	}

	/*==2 COLUMNS==*/
	
	.col2-layout .category-product.products-grid .item{
		width: 100%;
	}
	
	.col2-layout .category-product.products-grid .item:nth-child(1n+1){
		clear:both;
	}

	/*==3 COLUMNS==*/
	
	.col3-layout .category-product.products-grid .item{
		width: 100%;
	}
	
	.col3-layout .category-product.products-grid .item:nth-child(1n+1){
		clear:both;
	}
}

</style>

<script type="text/javascript">
//<![CDATA[
	var enable_ajax_cart = 1;
	var enable_ajax_compare = 1;
	var enable_ajax_wishlist = 1;
	var smcartpro_effect = 'click';
	var cartpro_baseurl = 'https://www.everybodyshops.com/';
	var isLoggedIn = 0;
	var is_page_wishlist = 0;
	var currencyCode = '$';
	var confirm_countdown_number = 5;
	
	function loadaJax(){
		if(enable_ajax_compare && enable_ajax_compare == 1)
			_eventCompare();
	
		if(enable_ajax_wishlist && enable_ajax_wishlist == 1)				
			_eventWishlist();
	
		if(enable_ajax_cart && enable_ajax_cart == 1)
			_eventCart();
		if(smcartpro_effect && smcartpro_effect == 'hover')
			_hoverCart();
	}
	
	function _resetModal(){	
		var $ = (typeof $ !== 'undefined') ? $ : jQuery.noConflict();
		$('.cpmodal-iframe').empty();
		$('.cpmodal-message').empty();
		$('.cpmodal-viewcart').addClass('cartpro-hidden');
		$('.cartpro-wishlist').addClass('cartpro-hidden');	
		$('.cartpro-compare').addClass('cartpro-hidden');
		$('.cartpro-time').addClass('cartpro-hidden');	
	}
	
	function _hoverCart(){
		var $ = (typeof $ !== 'undefined') ? $ : jQuery.noConflict();
		$( ".minicart-wrapper" ).click(function() {
		  return false;
		});			
		$( ".minicart-wrapper" )
		  .mouseover(function() {
			$(this).addClass('active');
			$('.showcart',this).addClass('active');
			$('.ui-widget-content',this).show();
		  })
		  .mouseout(function() {
			$(this).removeClass('active');
			$('.showcart',this).removeClass('active');
			$('.ui-widget-content',this).hide();
		});
	}
	
	function _eventCart(){
		var $ = (typeof $ !== 'undefined') ? $ : jQuery.noConflict();
		$(".action.tocart").removeAttr("data-mage-init");
		$(".action.tocart").unbind('click').click(function(e) {
		   if(typeof $("#cartpro_modal form[id^=product_addtocart_form_]") !== 'undefined' && $("#cartpro_modal form[id^=product_addtocart_form_]").length){
				$('#cartpro_modal form[id^=product_addtocart_form_]').mage('validation', {
					radioCheckboxClosest: '.nested',
					submitHandler: function (form) {
						var widget = $(form).catalogAddToCart({
							bindSubmit: false
						});
						widget.catalogAddToCart('submitForm', $(form));
						return false;
					}
				});				   
		   }else if ($(this).parents('#product_addtocart_form').length){
			   
		   }else{
				e.preventDefault();
				_addCart($(this));
				return false;				   
		   }
		});				
	}
	
	function _eventCompare(){
		var $ = (typeof $ !== 'undefined') ? $ : jQuery.noConflict();
		$(".action.tocompare").unbind('click').click(function(e) {				   
			e.preventDefault();				
			_addCompare($(this));
			return false;
		});		

		$("#product-comparison .action.delete").unbind('click').click(function(e) {	
			e.preventDefault();
			_removeCompare($(this));
			return false;
		});				
	}		
	
	
	function _removeCompare(_e){
			var $ = (typeof $ !== 'undefined') ? $ : jQuery.noConflict();
			$("#cartpro_process").addClass('cartpro-show');
			var post = $(_e).data('post');
			$.ajax({
				url: 'https://www.everybodyshops.com/cartpro/compare/remove',
				data : post.data,
				type:'POST',
				dataType:'json',
				success:function(json){
					if (typeof json.items_markup !== 'undefined'){
						if(json.items_markup.success == 1){
							var message = json.items_markup.message;
							var block_compare = json.items_markup.block_compare;
							$('.cpmodal-message').replaceWith(message);
							$('#product-comparison').replaceWith(block_compare);
							loadaJax();							
						}
					}
					$("#cartpro_process").removeClass('cartpro-show');
				}
			});			
	}
	
	function _addCompare(_e){
			var $ = (typeof $ !== 'undefined') ? $ : jQuery.noConflict();
			_resetModal();
			$("#cartpro_process").addClass('cartpro-show');	
			$('html').addClass('cartpro-block');	
			var post = $(_e).data('post');
			var params = {};
			params = post.data;
			params.form_key = $('input[name=form_key]').val();
			$.ajax({
				url: 'https://www.everybodyshops.com/cartpro/compare/add',
				data : params,
				type:'POST',
				dataType:'json',
				success:function(json){
					if (typeof json.items_markup !== 'undefined'){
						if(json.items_markup.success == 1){
							var message = json.items_markup.message;
							var nb_items = json.items_markup.nb_items;
							var catalog_compare_sidebar = json.items_markup.catalog_compare_sidebar;
							$('.cpmodal-message').html(message);
							$('.compare .counter.qty').html(nb_items+' Items');
							$('.sidebar.sidebar-additional block.block-compare').replaceWith(catalog_compare_sidebar);
						}
					}
					$("#cartpro_process").removeClass('cartpro-show');
					$("#cartpro_modal").addClass('cartpro-show');
					
					$('.cartpro-compare').removeClass('cartpro-hidden');
					$('.cartpro-time').removeClass('cartpro-hidden');
					start_time(confirm_countdown_number);
				}
			});	
		return true;		
	}
	
	function _addCart(_e){
		var $ = (typeof $ !== 'undefined') ? $ : jQuery.noConflict();
		_resetModal();				
		$("#cartpro_process").addClass('cartpro-show');
		$('html').addClass('cartpro-block');	
		var action = '';
		if($(_e).data('post')){
			 action = $(_e).data('post').action;	
		}else{
			 action = $(_e).closest('form').attr('action');
		}
		
		var params = {};
		if($('body').hasClass('catalog-product-compare-index')){
			params.product = $(_e).closest('.product.info').find('.price-final_price').data('product-id'); 
		}else{
			if($(_e).data('post'))
				params = $(_e).data('post').data;	
			else if($(_e).closest('form').length)
				params = $(_e).closest('form').serializeArray();
			else 
				params.product = $(_e).closest('.product-item-info').find('.price-box').data('product-id'); 
		}
		params.form_key = $('input[name=form_key]').val();
		if(!params.product)
			params.product = $(_e).closest('.product-item-info').find('.price-box').data('product-id'); 
		$.ajax({
			url: 'https://www.everybodyshops.com/cartpro/cart/add',
			data : params,
			type: 'post',
			dataType: 'json',
			success:function(json){
				if(typeof json.items_markup.success !== 'undefined' && json.items_markup.success){
					$.ajax({
						url: 'https://www.everybodyshops.com/cartpro/product/view',
						data : params,
						type: 'post',
						dataType: 'json',
						success:function(json){
							if(json.items_markup){										
								$('.cpmodal-iframe').html(json.items_markup);
								$('.cpmodal-message').html(json.name_product);
								$("#cartpro_process").removeClass('cartpro-show');
								$("#cartpro_modal").addClass('cartpro-show');
								$('html').addClass('cartpro-block');
								loadaJax();
							}	
						}
					});									
				}else{
					$.ajax({
						url: action,
						data : params,
						type: 'post',
						dataType: 'json',
						success:function(json){
							$("#cartpro_process").removeClass('cartpro-show');	
							$("html").removeClass('cartpro-block');
						}
					});					
				}
			}
		});	
		return false;					
	}
	
	
	function _eventWishlist(){	
		var $ = (typeof $ !== 'undefined') ? $ : jQuery.noConflict();	
			$(".action.towishlist").unbind('click').click(function(e) {	
				e.preventDefault();							
				if(isLoggedIn){	
					_addWishlist($(this));
					return false;
				}else{
					var check =  confirm('You must login first !');
					if(check == true){
						return true;
					} else {
						return false;
					}
				}	
			});	

		$("#wishlist-view-form .btn-remove.delete").click(function(e) {
			$("#cartpro_process").addClass('cartpro-show');	
			e.preventDefault();
			var post = $(this).data('post-remove');
			$.ajax({
				url: 'https://www.everybodyshops.com/cartpro/wishlist/remove',
				data : post.data,
				dataType:'json',
				success:function(json){
					if (typeof json.items_markup !== 'undefined'){
						if(json.items_markup.success == 1){
							var message = json.items_markup.message;
							var block_wishlist = json.items_markup.block_wishlist;
							$('#wishlist-view-form').replaceWith(block_wishlist);
							$('.cpmodal-message').replaceWith(message);
							loadaJax();							
						}
					}
					$("#cartpro_process").removeClass('cartpro-show');
				}
			});
			return false;
		});	
		
	}
	
	function _addWishlist(_e){
		var $ = (typeof $ !== 'undefined') ? $ : jQuery.noConflict();
		_resetModal();
		$("#cartpro_process").addClass('cartpro-show');		
		var post = $(_e).data('post');
		$.ajax({
			url: 'https://www.everybodyshops.com/cartpro/wishlist/add',
			data : post.data,
			dataType:'json',
			success:function(json){
				if (typeof json.items_markup !== 'undefined'){
					if(json.items_markup.success == 1){
						var message = json.items_markup.message;
						var nb_items = json.items_markup.nb_items;
						$('.cpmodal-message').html(message);
					}
				}
				$("#cartpro_process").removeClass('cartpro-show');
				$("#cartpro_modal").addClass('cartpro-show');
				$('.cartpro-wishlist').removeClass('cartpro-hidden');
				$('.cartpro-time').removeClass('cartpro-hidden');
				start_time(confirm_countdown_number);
			}
		});	
		return false;			
	}		
	
	function start_time(timer){
		var $ = (typeof $ !== 'undefined') ? $ : jQuery.noConflict();
		if (timer == 0){
	      	clearTimeout(timeout);
			$("#cartpro_modal").removeClass('cartpro-show');
			$("html").removeClass('cartpro-block');
			 _resetModal();
			return false;
	    }
		timeout = setTimeout(function(){
			timer--;
			$(".cpmodal-time").html(timer+' s');
			start_time(timer);
	  	}, 1000);
	}	
//]]>    
</script>

<script type="text/javascript">
	require([
		"jquery",
		"jquery/ui",
		"accordion",
		"mage/collapsible",
		"mage/redirect-url",
        "Magento_Catalog/product/view/validation",
        "Magento_Catalog/js/catalog-add-to-cart"		
	], function ($) {			
		$(document).ready(function($){	
			loadaJax();
			$(".cp-close").click(function(e) {
				$("#cartpro_modal").removeClass('cartpro-show');
				$('html').removeClass('cartpro-block');
				 _resetModal();
			});		
		});		
	});
</script>
    </head>
    <body data-container="body" data-mage-init='{"loaderAjax": {}, "loader": { "icon": "https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/images/loader-2.gif"}}' class="is-blog wordpress-post-view page-layout-2columns-left">
            <script>
        require.config({
            deps: [
                'jquery',
                'mage/translate',
                'jquery/jquery-storageapi'
            ],
            callback: function ($) {
                'use strict';

                var dependencies = [],
                    versionObj;

                $.initNamespaceStorage('mage-translation-storage');
                $.initNamespaceStorage('mage-translation-file-version');
                versionObj = $.localStorage.get('mage-translation-file-version');

                if (versionObj.version !== '45e523ae04675a1c5ce94a6ab5697a99200a346e') {
                    dependencies.push(
                        'text!js-translation.json'
                    );

                }

                require.config({
                    deps: dependencies,
                    callback: function (string) {
                        if (typeof string === 'string') {
                            $.mage.translate.add(JSON.parse(string));
                            $.localStorage.set('mage-translation-storage', string);
                            $.localStorage.set(
                                'mage-translation-file-version',
                                {
                                    version: '45e523ae04675a1c5ce94a6ab5697a99200a346e'
                                }
                            );
                        } else {
                            $.mage.translate.add($.localStorage.get('mage-translation-storage'));
                        }
                    }
                });
            }
        });
    </script>

<script type="text/x-magento-init">
    {
        "*": {
            "mage/cookies": {
                "expires": null,
                "path": "/",
                "domain": ".www.everybodyshops.com",
                "secure": false,
                "lifetime": "3600"
            }
        }
    }
</script>
    <noscript>
        <div class="message global noscript">
            <div class="content">
                <p>
                    <strong>JavaScript seems to be disabled in your browser.</strong>
                    <span>For the best experience on our site, be sure to turn on Javascript in your browser.</span>
                </p>
            </div>
        </div>
    </noscript>
<div class="page-wrapper">
<div class="header-container header-style-4">
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6 col-sm-6 customer-action-header">
					<div class="customer-action-hd">
						<span class="welcome-text">

							<span class="greet welcome" data-bind="scope: 'customer'">
								<!-- ko if: customer().fullname  -->
								<span data-bind="text: new String('Welcome, %1!').replace('%1', customer().firstname)">
								</span>
								<!-- /ko -->
								<!-- ko ifnot: customer().fullname  -->
								<span data-bind="html:'Welcome to everybodyshops.com'"></span>
								<!-- /ko -->
							</span>
							<script type="text/x-magento-init">
							{
								"*": {
									"Magento_Ui/js/core/app": {
										"components": {
											"customer": {
												"component": "Magento_Customer/js/view/customer"
											}
										}
									}
								}
							}
							</script>

						</span>
						
												
													<a href="https://www.everybodyshops.com/customer/account/create/" title="Create Your Account">Join Free</a> or <a href="https://www.everybodyshops.com/customer/account/login/" title="Sign In">Sign in</a>
											</div>
				</div>
				
				<div class="col-lg-4 col-md-4 col-sm-6 call-free-header">
					<div class="call-free">Call us toll free: <b>(800) 228-0142</b></div>
				</div>
				
				<div class="col-lg-4 col-md-6 header-top-links">
					<div class="toplinks-wrapper"><ul class="header links"><li class="myaccount-link">
	<a href="https://www.everybodyshops.com/customer/account/" title="My Account">My Account</a>
</li><li class="link wishlist" data-bind="scope: 'wishlist'">
    <a href="https://www.everybodyshops.com/wishlist/">My Wish List        <!-- ko if: wishlist().counter -->
        <span data-bind="text: wishlist().counter" class="counter qty"></span>
        <!-- /ko -->
    </a>
</li>
<script type="text/x-magento-init">
    {
        "*": {
            "Magento_Ui/js/core/app": {
                "components": {
                    "wishlist": {
                        "component": "Magento_Wishlist/js/view/wishlist"
                    }
                }
            }
        }
    }

</script>
<li class="checkout-link">
	<a href="https://www.everybodyshops.com/checkout/" title="Checkout">Checkout</a>
</li><li class="authorization-link" data-label="or">
    <a href="https://www.everybodyshops.com/customer/account/login/">
        Sign In    </a>
</li>
</ul></div>				</div>
			</div>
		</div>
	</div>
	
	<div class="header-middle">
		<div class="container">
			<div class="row"><!-- Went from a 2 and 10 to a 3 and 9 for LG and MD - Smy-->
				<div class="col-lg-3 col-md-3 col-sm-4 logo-header">
					<div class="logo-wrapper"><h1 class="logo-content">
    <strong class="logo">
		<a class="logo" href="https://www.everybodyshops.com/" title="Everybody Shops">
			<img src="https://www.everybodyshops.com/pub/media/logo/stores/1/Store-Logo_Fullcolor-05.09.17-rev3-wftoar-300.png"
				 alt="Everybody Shops"
				 width="300"				 height="99"			/>

		</a>
	</strong>
</h1>

</div>				</div>
				
				<div class="col-lg-9 col-md-9 col-sm-8 header-middle-right">
					<div class="middle-right-content">
												<div class="language-wrapper">
</div>						<div class="compare-header-wrapper"><div class="item link compare" data-bind="scope: 'compareProducts'" data-role="compare-products-link">
    <a class="action compare no-display" title="Compare Products"
       data-bind="attr: {'href': compareProducts().listUrl}, css: {'no-display': !compareProducts().count}"
    >
        <span class="counter qty" data-bind="text: compareProducts().countCaption"></span>
    </a>
</div>
<script type="text/x-magento-init">
{"[data-role=compare-products-link]": {"Magento_Ui/js/core/app": {"components":{"compareProducts":{"component":"Magento_Catalog\/js\/view\/compare-products"}}}}}
</script>
</div>					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="header-bottom">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 sidebar-megamenu">
					<div class="megamenu-content-sidebar">
						<div class="btn-megamenu">
							<a href="javascript:void(0)" title="All Categories">All Categories</a>
						</div>
						
						<div class="menu-ver-content">
							<div class="navigation-wrapper">

<nav class="navigation " role="navigation">
    <ul data-mage-init='{"menu":{"responsive":true, "expanded":true, "position":{"my":"left top","at":"left bottom"}}}'>
        <li  class="level0 nav-1 first level-top parent"><a href="https://www.everybodyshops.com/everybody-adventures.html"  class="level-top" ><span>Everybody Adventures</span></a><ul class="level0 submenu"><li  class="level1 nav-1-1 first"><a href="https://www.everybodyshops.com/everybody-adventures/hunting-shooting.html" ><span>Hunting &amp; Shooting</span></a></li><li  class="level1 nav-1-2"><a href="https://www.everybodyshops.com/everybody-adventures/fishing.html" ><span>Fishing</span></a></li><li  class="level1 nav-1-3"><a href="https://www.everybodyshops.com/everybody-adventures/paddling-boating.html" ><span>Paddling &amp; Boating</span></a></li><li  class="level1 nav-1-4"><a href="https://www.everybodyshops.com/everybody-adventures/camping.html" ><span>Camping</span></a></li><li  class="level1 nav-1-5"><a href="https://www.everybodyshops.com/everybody-adventures/hiking-backpacking.html" ><span>Hiking &amp; Backpacking</span></a></li><li  class="level1 nav-1-6"><a href="https://www.everybodyshops.com/everybody-adventures/winter-snow-sports.html" ><span>Winter &amp; Snow Sports</span></a></li><li  class="level1 nav-1-7"><a href="https://www.everybodyshops.com/everybody-adventures/ea-apparel.html" ><span>Apparel</span></a></li><li  class="level1 nav-1-8 last"><a href="https://www.everybodyshops.com/everybody-adventures/subscription.html" ><span>Subscriptions</span></a></li></ul></li><li  class="level0 nav-2 level-top parent"><a href="https://www.everybodyshops.com/everybody-craves.html"  class="level-top" ><span>Everybody Craves</span></a><ul class="level0 submenu"><li  class="level1 nav-2-1 first"><a href="https://www.everybodyshops.com/everybody-craves/beverages.html" ><span>Barware</span></a></li><li  class="level1 nav-2-2"><a href="https://www.everybodyshops.com/everybody-craves/food.html" ><span>Gadgets</span></a></li><li  class="level1 nav-2-3"><a href="https://www.everybodyshops.com/everybody-craves/appliances-electronics.html" ><span>Appliances &amp; Electronics</span></a></li><li  class="level1 nav-2-4"><a href="https://www.everybodyshops.com/everybody-craves/cook-s-tools.html" ><span>Cook's Tools</span></a></li><li  class="level1 nav-2-5"><a href="https://www.everybodyshops.com/everybody-craves/textiles-decor.html" ><span>Textiles &amp; Decor</span></a></li><li  class="level1 nav-2-6 last"><a href="https://www.everybodyshops.com/everybody-craves/cutlery.html" ><span>Cutlery</span></a></li></ul></li><li  class="level0 nav-3 last level-top parent"><a href="https://www.everybodyshops.com/everybody-gardens.html"  class="level-top" ><span>Everybody Gardens</span></a><ul class="level0 submenu"><li  class="level1 nav-3-1 first"><a href="https://www.everybodyshops.com/everybody-gardens/apparel.html" ><span>Apparel</span></a></li><li  class="level1 nav-3-2"><a href="https://www.everybodyshops.com/everybody-gardens/information.html" ><span>Information</span></a></li><li  class="level1 nav-3-3 last"><a href="https://www.everybodyshops.com/everybody-gardens/tools.html" ><span>Tools</span></a></li></ul></li>    </ul>
</nav>

<script type="text/javascript">
	require([
		'jquery'
	], function ($) {
		var limit;
		
					limit = 13;
				
		var i = 0;
		var items = $('.navigation > ul > li').length;
		
		if(items > limit){
			$('.navigation').append('<div class="more-w"><span class="more-view">More Categories</span></div>');
			
			$('.navigation > ul > li').each(function(){
				i++;
				if( i > limit ){
					$(this).css('display', 'none');
				}
			});
			
			$('.navigation .more-w > .more-view').click(function(){
				if($(this).hasClass('open')){
					i=0;
					$('.navigation > ul > li').each(function(){
						i++;
						if(i>limit){
							$(this).slideUp(200);
						}
					});
					$(this).removeClass('open');
					$('.more-w').removeClass('active-i');
					$(this).text('More Categories');
				}else{
					i=0;
					$('.navigation > ul > li').each(function(){
						i++;
						if(i>limit){
							$(this).slideDown(200);
						}
					});
					$(this).addClass('open');
					$('.more-w').addClass('active-i');
					$(this).text('Close Menu');
				}
			});

		}

	});
</script>
</div>						</div>
					</div>
				</div>
				
				<div class="navigation-mobile-container">
					

<!--COLLAPSE-->

<!--SIDEBAR-->
	<div class="nav-mobile-container sidebar-type">
		<div class="btn-mobile">
			<a id="sidebar-button" class="button-mobile sidebar-nav" title="Categories"><i class="fa fa-bars"></i><span class="hidden">Categories</span></a>
		</div>
		<nav class="navigation-mobile">
			<ul>
				<li  class="level0 nav-1 first level-top parent"><a href="https://www.everybodyshops.com/everybody-adventures.html"  class="level-top" ><span>Everybody Adventures</span></a><ul class="level0 submenu"><li  class="level1 nav-1-1 first"><a href="https://www.everybodyshops.com/everybody-adventures/hunting-shooting.html" ><span>Hunting &amp; Shooting</span></a></li><li  class="level1 nav-1-2"><a href="https://www.everybodyshops.com/everybody-adventures/fishing.html" ><span>Fishing</span></a></li><li  class="level1 nav-1-3"><a href="https://www.everybodyshops.com/everybody-adventures/paddling-boating.html" ><span>Paddling &amp; Boating</span></a></li><li  class="level1 nav-1-4"><a href="https://www.everybodyshops.com/everybody-adventures/camping.html" ><span>Camping</span></a></li><li  class="level1 nav-1-5"><a href="https://www.everybodyshops.com/everybody-adventures/hiking-backpacking.html" ><span>Hiking &amp; Backpacking</span></a></li><li  class="level1 nav-1-6"><a href="https://www.everybodyshops.com/everybody-adventures/winter-snow-sports.html" ><span>Winter &amp; Snow Sports</span></a></li><li  class="level1 nav-1-7"><a href="https://www.everybodyshops.com/everybody-adventures/ea-apparel.html" ><span>Apparel</span></a></li><li  class="level1 nav-1-8 last"><a href="https://www.everybodyshops.com/everybody-adventures/subscription.html" ><span>Subscriptions</span></a></li></ul></li><li  class="level0 nav-2 level-top parent"><a href="https://www.everybodyshops.com/everybody-craves.html"  class="level-top" ><span>Everybody Craves</span></a><ul class="level0 submenu"><li  class="level1 nav-2-1 first"><a href="https://www.everybodyshops.com/everybody-craves/beverages.html" ><span>Barware</span></a></li><li  class="level1 nav-2-2"><a href="https://www.everybodyshops.com/everybody-craves/food.html" ><span>Gadgets</span></a></li><li  class="level1 nav-2-3"><a href="https://www.everybodyshops.com/everybody-craves/appliances-electronics.html" ><span>Appliances &amp; Electronics</span></a></li><li  class="level1 nav-2-4"><a href="https://www.everybodyshops.com/everybody-craves/cook-s-tools.html" ><span>Cook's Tools</span></a></li><li  class="level1 nav-2-5"><a href="https://www.everybodyshops.com/everybody-craves/textiles-decor.html" ><span>Textiles &amp; Decor</span></a></li><li  class="level1 nav-2-6 last"><a href="https://www.everybodyshops.com/everybody-craves/cutlery.html" ><span>Cutlery</span></a></li></ul></li><li  class="level0 nav-3 last level-top parent"><a href="https://www.everybodyshops.com/everybody-gardens.html"  class="level-top" ><span>Everybody Gardens</span></a><ul class="level0 submenu"><li  class="level1 nav-3-1 first"><a href="https://www.everybodyshops.com/everybody-gardens/apparel.html" ><span>Apparel</span></a></li><li  class="level1 nav-3-2"><a href="https://www.everybodyshops.com/everybody-gardens/information.html" ><span>Information</span></a></li><li  class="level1 nav-3-3 last"><a href="https://www.everybodyshops.com/everybody-gardens/tools.html" ><span>Tools</span></a></li></ul></li>			</ul>
		</nav>
		
		<script type="text/javascript">
			require([
				'jquery'
			], function ($) {
				$('#sidebar-button').click(function(){
					$('body').toggleClass('navbar-active');
					$(this).toggleClass('active');
				});
				
				$('.navigation-mobile > ul li').has('ul').append( '<span class="touch-button"><span>open</span></span>' );

				$('.touch-button').click(function(){
					$(this).prev().slideToggle(200);
					$(this).toggleClass('active');
					$(this).parent().toggleClass('parent-active');
				});

			});
		</script>
		
	</div>
				</div>
				
				<div class="col-lg-9 col-md-9 searchbox-header">
					<div class="search-cart">
						<div class="search-wrapper">
	<div id="sm_searchbox21014402921497034202" class="sm-searchbox">
		
		
		<div class="sm-searchbox-content">
			<form class="form minisearch" id="searchbox_mini_form" action="https://www.everybodyshops.com/catalogsearch/result/" method="get">
				<div class="field searchbox">
					<div class="control">
						<select class="cat searchbox-cat" name="cat">
							<option value="">All Categories</option>
															<option value="3"  >- - Everybody Adventures</option>
															<option value="19"  >- - - - Hunting & Shooting</option>
															<option value="20"  >- - - - Fishing</option>
															<option value="21"  >- - - - Paddling & Boating</option>
															<option value="22"  >- - - - Camping</option>
															<option value="23"  >- - - - Hiking & Backpacking</option>
															<option value="24"  >- - - - Winter & Snow Sports</option>
															<option value="25"  >- - - - Apparel</option>
															<option value="26"  >- - - - Subscriptions</option>
															<option value="4"  >- - Everybody Craves</option>
															<option value="11"  >- - - - Barware</option>
															<option value="12"  >- - - - Gadgets</option>
															<option value="15"  >- - - - Appliances & Electronics</option>
															<option value="16"  >- - - - Cook's Tools</option>
															<option value="17"  >- - - - Textiles & Decor</option>
															<option value="18"  >- - - - Cutlery</option>
															<option value="7"  >- - Everybody Gardens</option>
															<option value="8"  >- - - - Apparel</option>
															<option value="9"  >- - - - Information</option>
															<option value="10"  >- - - - Tools</option>
													</select>
						
						<input id="searchbox"
							   data-mage-init='{"quickSearch":{
									"formSelector":"#searchbox_mini_form",
									"url":"https://www.everybodyshops.com/search/ajax/suggest/",
									"destinationSelector":"#searchbox_autocomplete"}
							   }'
							   type="text"
							   name="q"
							  
							   onfocus="if(this.value=='Enter keywords to search...') this.value='';" onblur="if(this.value=='') this.value='Enter keywords to search...';" value="Enter keywords to search..." 
							   class="input-text input-searchbox"
							   maxlength="128"
							   role="combobox"
							   aria-haspopup="false"
							   aria-expanded="true"
							   aria-autocomplete="both"
							   autocomplete="off"/>
						<div id="searchbox_autocomplete" class="search-autocomplete"></div>
												
					</div>
				</div>
				<div class="actions">
					<button type="submit" title="Search" class="btn-searchbox">
						<span>Search</span>
					</button>
				</div>
			</form>
		</div>
		
				
			</div>


<script type="text/javascript">
	require([
		'jquery'
	], function ($) {
			var searchbox = $('#sm_searchbox21014402921497034202');
			var firt_load = 5;

			clickMore($('.sm-searchbox-more', searchbox));
			function clickMore(more)
			{
				more.click(function () {
					var that = $(this);
					var sb_ajaxurl = that.attr('data-ajaxmore');
					var count = that.attr('data-count');
					count = parseInt(count);
					if (firt_load >= count) {
						count = count + parseInt(firt_load);
					}
					$.ajax({
						type: 'POST',
						url: sb_ajaxurl,
						data: {
							is_ajax: 1,
							count_term: count
						},
						success: function (data) {
							$('.sm-searchbox-keyword', searchbox).html(data.htm);
							clickMore($('a.sm-searchbox-more',searchbox));
							$('a.sm-searchbox-more', searchbox).attr({
								'data-count': count + parseInt(firt_load)
							});
						},
						dataType: 'json'
					});
				});
			}

	});
</script></div>						
						<div class="minicart-header">
							<div class="minicart-content">
								<div class="cart-wrapper">
<div data-block="minicart" class="minicart-wrapper">
    <a class="action showcart" href="https://www.everybodyshops.com/checkout/cart/"
       data-bind="scope: 'minicart_content'">
        <span class="text"><span class="df-text">Shopping Cart</span><span class="hidden">My Cart - </span></span>
        <span class="counter qty empty"
              data-bind="css: { empty: !!getCartParam('summary_count') == false }, blockLoader: isLoading">
            <span class="counter-number"><!-- ko text: getCartParam('summary_count') --><!-- /ko --></span>
            <span class="counter-label">
			<!-- ko if: getCartParam('summary_count') -->
                <!-- ko text: getCartParam('summary_count') --><!-- /ko -->
                <!-- ko i18n: 'items' --><!-- /ko -->
            <!-- /ko -->
			
			<!-- ko if: getCartParam('summary_count') == 0 -->
                <!-- ko i18n: '0 item' --><!-- /ko -->
            <!-- /ko -->
            </span>
			
			<span class="price-minicart">
				
					<!-- ko foreach: getRegion('subtotalContainer') -->
						<!-- ko template: getTemplate() --><!-- /ko -->
					<!-- /ko -->
				
			</span>
        </span>
    </a>
            <div class="block block-minicart empty"
             data-role="dropdownDialog"
             data-mage-init='{"dropdownDialog":{
                "appendTo":"[data-block=minicart]",
                "triggerTarget":".showcart",
                "timeout": "2000",
                "closeOnMouseLeave": false,
                "closeOnEscape": true,
                "triggerClass":"active",
                "parentClass":"active",
                "buttons":[]}}'>
            <div id="minicart-content-wrapper" data-bind="scope: 'minicart_content'">
                <!-- ko template: getTemplate() --><!-- /ko -->
            </div>
                    </div>
        <script>
        window.checkout = {"shoppingCartUrl":"https:\/\/www.everybodyshops.com\/checkout\/cart\/","checkoutUrl":"https:\/\/www.everybodyshops.com\/checkout\/","updateItemQtyUrl":"https:\/\/www.everybodyshops.com\/checkout\/sidebar\/updateItemQty\/","removeItemUrl":"https:\/\/www.everybodyshops.com\/checkout\/sidebar\/removeItem\/","imageTemplate":"Magento_Catalog\/product\/image_with_borders","baseUrl":"https:\/\/www.everybodyshops.com\/","minicartMaxItemsVisible":5,"websiteId":"1","customerLoginUrl":"https:\/\/www.everybodyshops.com\/customer\/account\/login\/","isRedirectRequired":false,"autocomplete":"off","captcha":{"user_login":{"isCaseSensitive":false,"imageHeight":50,"imageSrc":"","refreshUrl":"https:\/\/www.everybodyshops.com\/captcha\/refresh\/","isRequired":false},"guest_checkout":{"isCaseSensitive":false,"imageHeight":50,"imageSrc":"","refreshUrl":"https:\/\/www.everybodyshops.com\/captcha\/refresh\/","isRequired":false}}};
    </script>
    <script type="text/x-magento-init">
    {
        "[data-block='minicart']": {
            "Magento_Ui/js/core/app": {"components":{"minicart_content":{"children":{"subtotal.container":{"children":{"subtotal":{"children":{"subtotal.totals":{"config":{"display_cart_subtotal_incl_tax":0,"display_cart_subtotal_excl_tax":1,"template":"Magento_Tax\/checkout\/minicart\/subtotal\/totals"},"children":{"subtotal.totals.msrp":{"component":"Magento_Msrp\/js\/view\/checkout\/minicart\/subtotal\/totals","config":{"displayArea":"minicart-subtotal-hidden","template":"Magento_Msrp\/checkout\/minicart\/subtotal\/totals"}}},"component":"Magento_Tax\/js\/view\/checkout\/minicart\/subtotal\/totals"}},"component":"uiComponent","config":{"template":"Magento_Checkout\/minicart\/subtotal"}}},"component":"uiComponent","config":{"displayArea":"subtotalContainer"}},"item.renderer":{"component":"uiComponent","config":{"displayArea":"defaultRenderer","template":"Magento_Checkout\/minicart\/item\/default"},"children":{"item.image":{"component":"Magento_Catalog\/js\/view\/image","config":{"template":"Magento_Catalog\/product\/image","displayArea":"itemImage"}},"checkout.cart.item.price.sidebar":{"component":"uiComponent","config":{"template":"Magento_Checkout\/minicart\/item\/price","displayArea":"priceSidebar"}}}},"extra_info":{"component":"uiComponent","config":{"displayArea":"extraInfo"}},"promotion":{"component":"uiComponent","config":{"displayArea":"promotion"}}},"config":{"itemRenderer":{"default":"defaultRenderer","simple":"defaultRenderer","virtual":"defaultRenderer"},"template":"Magento_Checkout\/minicart\/content"},"component":"Magento_Checkout\/js\/view\/minicart"}},"types":[]}        },
        "*": {
            "Magento_Ui/js/block-loader": "https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/images/loader-1.gif"
        }
    }
    </script>
</div>


</div>							</div>
						</div>
						
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>



	<script type="text/javascript">
		require([
			'jquery'
		], function ($) {
			$window = $(window);
			if($('.header-bottom').length){
				menu_offset_top = $('.header-bottom').offset().top;
				function processScroll() {
					var scrollTop = $window.scrollTop();
					if ( scrollTop >= menu_offset_top) {
						$('.header-bottom').addClass('menu-on-top');
						$('body').addClass('body-on-top');
					} else if (scrollTop <= menu_offset_top) {
						$('.header-bottom').removeClass('menu-on-top');
						$('body').removeClass('body-on-top');
					}
				}
				processScroll();
				$window.scroll(function(){
					processScroll();
				});
			}
		});
	</script>
<div class="breadcrumbs">
	<div class="container">
		<ul class="items">
							<li class="item home">
									<a href="https://www.everybodyshops.com" title="Go to Home Page">
						Home					</a>
								</li>
							<li class="item blog">
									<a href="https://www.everybodyshops.com/content" title="">
						Content					</a>
								</li>
							<li class="item post">
									<strong>Plant pansies in early spring for instant color</strong>
								</li>
					</ul>
	</div>
</div>



<main id="maincontent" class="page-main"><a id="contentarea" tabindex="-1"></a>
<div class="columns col2-layout">
	<div class="container">
		<div class="row">
		
		
			<div class="col-lg-3 col-md-3">
				<div class="sidebar sidebar-main">
					<div class="wp-sidebar">
						<div class="block block-blog block-logo">
						<style>	
							#craves_logo {
								background-image: url("https://www.everybodyshops.com/pub/media/catalog/category/craves300_tall.jpg");
								width:100%;
								height: auto;
								max-width: 320px;
								background-color: #321321;
								}
						</style>
							<div class="block-title">
								<strong>Logo Test Area</strong>
							</div>
							<div class="block-content">
						  		<div id="craves_logo"><!--This will be a dynamic class that change based off of post category.-->
								    &nbsp;
								</div>
							</div>
						</div>
						
						
						
<!--.multi_logo {
    background: url("https://www.everybodyshops.com/pub/media/catalog/category/craves270.jpg") 0 0 no-repeat;
    width: 300px; /* match logo width */
    height: 79px; /* match logo height */
    text-indent: -9999px; /* hides text */
	}-->
	
						
						
						
						
<style>
.category-everybody-adventures .multi_logo {
	background-image: url("") 0 0 no-repeat;
	width:100%;
	height: auto;
	max-width: 300px;
	background-color: #321321;
	}
	
.category-everybody-craves .multi_logo {
	background-image: url("https://www.everybodyshops.com/pub/media/catalog/category/craves270.jpg") 0 0 no-repeat;
	width:100%;
	height: auto;
	max-width: 270px;
	background-color: #321321;
	}
/*typo in gardens logo name*/	
.category-everybody-gardens .multi_logo {
	background-image: url("https://www.everybodyshops.com/pub/media/catalog/category/gardens270.jpg") 0 0 no-repeat;
	width:100%;
	height: auto;
	max-width: 270px;
	background-color: #321321;
	}
</style>

<div class="block block-blog block-logo">
	<div class="block-title">
		<strong>Multiple Logo Test</strong>
	</div>
	<div class="block-content">
		<div class="multi_logo"><!--This will be a dynamic class that change based off of post category.-->
			Category Logo Everybody Shops
		</div>
	</div>
</div>					
						
					
						
						
						
						<div class="block block-blog block-php-code-widget">
							<div class="block-title">
								<strong>PHP Test Area</strong>
							</div>
							<div class="block-content">
						  	
							<?php /*Recent Posts Sort by Category*/
							add_filter( 'widget_posts_args', 'my_widget_posts_args');
							function my_widget_posts_args($args) {
								if ( is_category() ) { //adds the category parameter if we display a category
									$cat = get_queried_object();
									return array(
										'posts_per_page' => 7,//set the number you want here 
										'no_found_rows' => true, 
										'post_status' => 'publish', 
										'ignore_sticky_posts' => true,
										'cat' => $cat -> term_id//the current category id [not sure this is floating through FishPig]
										 );
								}
								else {//keeps the normal behaviour if we are not in category context
									return $args;
								}  ?>
							</div>
						</div>
						<div class="block block-blog block-social">
							<div class="block-title">
								<strong>Social Test Area</strong>
							</div>
							<div class="block-content">
							  <a target="_blank" href="https://www.facebook.com/triblive">
								  <img width="" height="" alt=" fb" style="border: none; display: inline;" src="http://triblive.com/csp/mediapool/sites/TribLIVE/assets/specialprojects/SocialMedia/facebook25.png">
							  </a>
							  <a target="_blank" href="https://twitter.com/triblive/">
								  <img width="" height="" style="border: 0px none; display: inline;" alt=" tw" src="http://triblive.com/csp/mediapool/sites/TribLIVE/assets/specialprojects/SocialMedia/twitter25.png">
							  </a>
							</div>
						</div>
						
						
						<div class="block block-blog block-php-code-widget">
							<div class="block-title">
								<strong>PHP Test Area</strong>
							</div>
							<div class="block-content">
							  php
							</div>
						</div>
						
						
						<div class="block block-blog block-blog-search">
							<div class="block-title">
								<strong>Search</strong>
							</div>
							<div class="block-content">
							<form method="get" action="https://www.everybodyshops.com/content/search/" id="blog-search-form">
								<div class="blog-search">
								   <input type="text" name="s" class="input-text" title="Search Posts" value=""/>
								   <button class="button" title="Search" type="submit"><span><span>Search</span></span></button>
									</div>
								</form>
							</div>
						</div>
						<div class="block block-blog block-recent-posts">
							<div class="block-title">
								<strong>Recent Posts</strong>
							</div>
							<div class="block-content">
								<ul id="wp-89c">
								<li class="item">
								<a href="https://www.everybodyshops.com/content/plant-pansies-now-for-instant-color/" title="Plant pansies in early spring for instant color">Plant pansies in early spring for instant color</a>
								</li>
								<li class="item">
								<a href="https://www.everybodyshops.com/content/the-latest-kitchen-trend-is-dark-and-daring/" title="The latest kitchen trend is dark and daring">The latest kitchen trend is dark and daring</a>
								</li>
								<li class="item">
								<a href="https://www.everybodyshops.com/content/how-to-use-a-baitcasting-reel-and-other-fishing-tips/" title="How to use a baitcasting reel and other fishing tips">How to use a baitcasting reel and other fishing tips</a>
								</li>
								<li class="item">
								<a href="https://www.everybodyshops.com/content/six-tips-for-better-fishing-this-spring-and-summer/" title="Six tips for better fishing this spring and summer">Six tips for better fishing this spring and summer</a>
								</li>
								</ul>
							</div>
						</div>

						<div class="block block-blog block-blog-categories">
							<div class="block-title">
								<strong>Categories</strong>
							</div>
							<div class="block-content">
								<ul class="level0" id="wp-category-list">
																		<li class="level0 item">
										<a  class="level0" href="https://www.everybodyshops.com/content/category/everybodyadventures/" title="Everybody Adventures">
											Everybody Adventures							</a>													</li>
														<li class="level0 item">
										<a  class="level0" href="https://www.everybodyshops.com/content/category/everybody-craves/" title="Everybody Craves">
											Everybody Craves							</a>													</li>
														<li class="level0 item">
										<a  class="level0" href="https://www.everybodyshops.com/content/category/everybodygardens/" title="Everybody Gardens">
											Everybody Gardens							</a>													</li>
															</ul>
							</div>
						</div>
					</div>
				</div>
   
   
   <div class="sidebar sidebar-additional">
   		<div class="block block-compare" data-bind="scope: 'compareProducts'" data-role="compare-products-sidebar">
    <div class="block-title">
        <strong id="block-compare-heading" role="heading" aria-level="2">Compare Products</strong>
        <span class="counter qty no-display" data-bind="text: compareProducts().countCaption, css: {'no-display': !compareProducts().count}"></span>
    </div>
    <!-- ko if: compareProducts().count -->
    <div class="block-content no-display" aria-labelledby="block-compare-heading" data-bind="css: {'no-display': !compareProducts().count}">
        <ol id="compare-items" class="product-items product-items-names" data-bind="foreach: compareProducts().items">
                <li class="product-item">
                    <input type="hidden" class="compare-item-id" data-bind="value: id"/>
                    <strong class="product-item-name">
                        <a data-bind="attr: {href: product_url}, html: name" class="product-item-link"></a>
                    </strong>
                    <a href="#" data-bind="attr: {'data-post': remove_url}" title="Remove This Item" class="action delete">
                        <span>Remove This Item</span>
                    </a>
                </li>
        </ol>
        <div class="actions-toolbar">
            <div class="primary">
                <a data-bind="attr: {'href': compareProducts().listUrl}" class="action compare primary"><span>Compare</span></a>
            </div>
            <div class="secondary">
                <a id="compare-clear-all" href="#" class="action clear" data-post="{&quot;action&quot;:&quot;https:\/\/www.everybodyshops.com\/catalog\/product_compare\/clear\/&quot;,&quot;data&quot;:{&quot;uenc&quot;:&quot;&quot;}}">
                    <span>Clear All</span>
                </a>
            </div>
        </div>
    </div>
    <!-- /ko -->
    <!-- ko ifnot: compareProducts().count -->
    <div class="empty">You have no items to compare.</div>
    <!-- /ko -->
		</div>
<script type="text/x-magento-init">
{"[data-role=compare-products-sidebar]": {"Magento_Ui/js/core/app": {"components":{"compareProducts":{"component":"Magento_Catalog\/js\/view\/compare-products"}}}}}
</script>
	<p><!--<h1 style="color:#f1703d;">Has no content to show! - inside</h1>--></p>
	</div>
</div>
  

  
  
  
  
  
  
  <div class="col-lg-9 col-md-9">

   
   <div class="page-title-wrapper">
    <h1 class="page-title"
                >
        <span class="base" data-ui-id="page-title-wrapper" >Plant pansies in early spring for instant color</span>    </h1>
    </div>
<div class="page messages"><div data-placeholder="messages"></div>
<div data-bind="scope: 'messages'">
    <div data-bind="foreach: { data: cookieMessages, as: 'message' }" class="messages">
        <div data-bind="attr: {
            class: 'message-' + message.type + ' ' + message.type + ' message',
            'data-ui-id': 'message-' + message.type
        }">
            <div data-bind="html: message.text"></div>
        </div>
    </div>
    <div data-bind="foreach: { data: messages().messages, as: 'message' }" class="messages">
        <div data-bind="attr: {
            class: 'message-' + message.type + ' ' + message.type + ' message',
            'data-ui-id': 'message-' + message.type
        }">
            <div data-bind="html: message.text"></div>
        </div>
    </div>
</div>
<script type="text/x-magento-init">
    {
        "*": {
            "Magento_Ui/js/core/app": {
                "components": {
                        "messages": {
                            "component": "Magento_Theme/js/view/messages"
                        }
                    }
                }
            }
    }
</script>
</div>
   
   <div class="column main"><input name="form_key" type="hidden" value="66iaq3Jz4GXGXIa3" /><div id="authenticationPopup" data-bind="scope:'authenticationPopup'" style="display: none;">
    <script>
        window.authenticationPopup = {"customerRegisterUrl":"https:\/\/www.everybodyshops.com\/customer\/account\/create\/","customerForgotPasswordUrl":"https:\/\/www.everybodyshops.com\/customer\/account\/forgotpassword\/","baseUrl":"https:\/\/www.everybodyshops.com\/"};
    </script>
    <!-- ko template: getTemplate() --><!-- /ko -->
    <script type="text/x-magento-init">
        {
            "#authenticationPopup": {
                "Magento_Ui/js/core/app": {"components":{"authenticationPopup":{"component":"Magento_Customer\/js\/view\/authentication-popup","children":{"messages":{"component":"Magento_Ui\/js\/view\/messages","displayArea":"messages"},"captcha":{"component":"Magento_Captcha\/js\/view\/checkout\/loginCaptcha","displayArea":"additional-login-form-fields","formId":"user_login","configSource":"checkout"}}}}}            },
            "*": {
                "Magento_Ui/js/block-loader": "https://www.everybodyshops.com/pub/static/version1495658372/frontend/Sm/market/en_US/images/loader-1.gif"
            }
        }
    </script>
</div>
<script type="text/x-magento-init">
{"*":{"Magento_Customer\/js\/section-config":{"sections":{"stores\/store\/switch":"*","directory\/currency\/switch":"*","*":["messages"],"customer\/account\/logout":"*","customer\/account\/loginpost":"*","customer\/account\/createpost":"*","customer\/ajax\/login":["checkout-data","cart"],"catalog\/product_compare\/add":["compare-products"],"catalog\/product_compare\/remove":["compare-products"],"catalog\/product_compare\/clear":["compare-products"],"sales\/guest\/reorder":["cart"],"sales\/order\/reorder":["cart"],"checkout\/cart\/add":["cart"],"checkout\/cart\/delete":["cart"],"checkout\/cart\/updatepost":["cart"],"checkout\/cart\/updateitemoptions":["cart"],"checkout\/cart\/couponpost":["cart"],"checkout\/cart\/estimatepost":["cart"],"checkout\/cart\/estimateupdatepost":["cart"],"checkout\/onepage\/saveorder":["cart","checkout-data","last-ordered-items"],"checkout\/sidebar\/removeitem":["cart"],"checkout\/sidebar\/updateitemqty":["cart"],"rest\/*\/v1\/carts\/*\/payment-information":["cart","checkout-data","last-ordered-items"],"rest\/*\/v1\/guest-carts\/*\/payment-information":["cart","checkout-data"],"rest\/*\/v1\/guest-carts\/*\/selected-payment-method":["cart","checkout-data"],"rest\/*\/v1\/carts\/*\/selected-payment-method":["cart","checkout-data"],"multishipping\/checkout\/overviewpost":["cart"],"paypal\/express\/placeorder":["cart","checkout-data"],"paypal\/payflowexpress\/placeorder":["cart","checkout-data"],"review\/product\/post":["review"],"authorizenet\/directpost_payment\/place":["cart","checkout-data"],"braintree\/paypal\/placeorder":["cart","checkout-data"],"wishlist\/index\/add":["wishlist"],"wishlist\/index\/remove":["wishlist"],"wishlist\/index\/updateitemoptions":["wishlist"],"wishlist\/index\/update":["wishlist"],"wishlist\/index\/cart":["wishlist","cart"],"wishlist\/index\/fromcart":["wishlist","cart"],"wishlist\/index\/allcart":["wishlist","cart"],"wishlist\/shared\/allcart":["wishlist","cart"],"wishlist\/shared\/cart":["cart"],"cartpro\/cart\/add":["cart","messages"],"cartpro\/compare\/remove":["compare-products","messages"],"cartpro\/compare\/add":["compare-products","messages"]},"clientSideSections":["checkout-data"],"baseUrls":["https:\/\/www.everybodyshops.com\/"]}}}</script>
<script type="text/x-magento-init">
{"*":{"Magento_Customer\/js\/customer-data":{"sectionLoadUrl":"https:\/\/www.everybodyshops.com\/customer\/section\/load\/","cookieLifeTime":"3600","updateSessionUrl":"https:\/\/www.everybodyshops.com\/customer\/account\/updateSession\/"}}}</script>
<script type="text/x-magento-init">
    {
        "body": {
            "pageCache": {"url":"https:\/\/www.everybodyshops.com\/page_cache\/block\/render\/id\/14\/","handles":["default","wordpress_post_view","wordpress_post_view_14","wordpress_default"],"originalRequest":{"route":"wordpress","controller":"post","action":"view","uri":"\/content\/plant-pansies-now-for-instant-color\/"},"versionCookieName":"private_content_version"}        }
    }
</script>
<script>
	function _SmQuickView(){
		var $ = (typeof $ !== 'undefined') ? $ : jQuery.noConflict();
		var	pathbase = 'quickview/index/index',
			_item_cls = $('.products-grid .item .item-inner .box-image, .products-list .item .item-inner .box-image-list'),
			_item_cls_show_button = $('.products-grid .item .item-inner .box-image, .products-list .item .item-inner .box-image-list'),
			_base_url = 'https://www.everybodyshops.com/';
		var baseUrl = _base_url + pathbase;
		var _arr = [];
		if(_item_cls.length > 0 && _item_cls_show_button.length > 0){
			_item_cls.each(function() {
				var $this = $(this);
				if($this.parent().parent().find("a.sm_quickview_handler").length <= 0) {
					if( $this.find('a').length > 0 ){
						var _href =	(typeof $($this.find('a')[0]).attr('href') !== 'undefined' && $($this.find('a')[0]).attr('href').replace(_base_url,"") == '#') ? $($this.parent().find('a')[0]) : $($this.find('a')[0]);
						if(typeof _href.attr('href') !== 'undefined'){
							var	producturlpath = _href.attr('href').replace(_base_url,"");
							producturlpath = ( producturlpath.indexOf('index.php') >= 0 ) ? producturlpath.replace('index.php/','') : producturlpath;
							var	reloadurl = baseUrl+ ("/path/"+producturlpath).replace(/\/\//g,"/"),
								_quickviewbutton = "<a class='sm_quickview_handler' title='Quick View' href='"+reloadurl+"'>Quick View</a>";
							_arr.push(_quickviewbutton);
						}
					}
				}
			});
			var count = 0 ;
			_item_cls_show_button.each(function () {
				if(typeof _arr[count] != 'undefined' && $(this).find("a.sm_quickview_handler").length <= 0)
				{
					$(this).append(_arr[count]);
					count ++;
				}
			});
			_SmFancyboxView();
		}
	}

	function _SmFancyboxView(){
		var $ = (typeof $ !== 'undefined') ? $ : jQuery.noConflict();
		$('.sm_quickview_handler').fancybox({
						width : '1000px',
			height : '600px',
						autoSize       :  0,
			title          : 'null',
			scrolling      : 'auto',
			type           : 'ajax',
			ajax :{
				type : 'POST',
				dataType : 'json',
				headers  : { 'X-fancyBox': true }

			},
			openEffect     : 'none',
			closeEffect    : 'fade',
			helpers        :{
				title:  null,
								overlay : {
					showEarly : true
				}
							},
			beforeLoad: function(){

			},
			afterLoad:function(){
				var _current =  'https://www.everybodyshops.com/content/plant-pansies-now-for-instant-color/';
				if(typeof this.content.sucess !== 'undefined' && this.content.sucess == true){
					var _title = '<div class="smqv-title"><h1>'+this.content.title+'</h1></div>', _content = this.content.item_mark ;
					this.inner.html('<div id="sm_quickview" class="sm-quickview">'+_title+_content+'</div>');
				}
			},
			beforeShow : function(){
				var _bundle_slide	=  $('#bundle-slide');
				if(_bundle_slide.length){
					_bundle_slide.on('click', function(){
						$('.fancybox-inner').addClass('smqv-height-content');
						var  _bundleOptionsContainer = $('.bundle-options-container .product-add-form');
						_bundleOptionsContainer.show();
						$('html, body, .fancybox-wrap').animate({
							scrollTop: _bundleOptionsContainer.offset().top
						}, 600);
					});
				}

			},
			afterShow : function(){
				if($('#quickview-gallery-placeholder').length){
					$('#quickview-gallery-placeholder').removeClass('gallery-loading');
				}

			},
			beforeChange: function(){

			},
			beforeClose: function(){
			},
			afterClose:function(){
				$('.zoomContainer').remove();
			}
		});
	}
</script>


<script type="text/javascript">
	require([
		'jquery',
		'jqueryfancyboxpack'
	], function ($) {
		_SmQuickView();
	});
</script><div class="post-entry">		
		<div class="post-image">
			<a href="https://www.everybodyshops.com/content/plant-pansies-now-for-instant-color/" title="Plant pansies in early spring for instant color"><img src="https://www.everybodyshops.com/wp/wp-content/uploads/2017/04/blog-gtr-LIV-pansies5-033117.jpg" alt="Plant pansies in early spring for instant color"/></a>
		</div>
	

		
			<div class="post-content"><p>The unmistakable fragrance of pansies fills the air in an open hoop house at Laurel Nursery and Garden Center. The wonderful aroma originates on a long bench covered with both pansies and violas in a multitude of cheery colors.<br />
Ken Heese, his father John and son Jacob all work together running the operation which has been a Latrobe area gardening institution since 1974. Ken has been helping customers grow the right plant at the right time for the past 36 years and this is when he recommends planting pansies.</p>
<div id="attachment_1379" class="wp-caption aligncenter">
<p class="wp-caption-text">This is a bench filled with pansies at Laurel Nursery and Garden Center in Latrobe.</p>
</div>
<p>“They like the cooler weather,” he says. “They thrive in these pre-80 degree temperatures and perform very well in the early spring.”<br />
Not only can they be planted in the ground, pansies and violas also are great for growing in pots too. “Fantastic container plant as long as (customers) don’t leave it in the hot sun while they are at work,” he says, stressing that morning sun is best for pansies.</p>
<div class="fluid-width-video-wrapper"><iframe id="_ytid_80651" class="__youtube_prefs__" src="https://www.youtube.com/embed/hHDWJCrqTgY?enablejsapi=1&amp;origin=http://everybodygardens.triblive.com&amp;autoplay=0&amp;cc_load_policy=0&amp;iv_load_policy=1&amp;loop=0&amp;modestbranding=0&amp;rel=1&amp;showinfo=1&amp;playsinline=1&amp;controls=2&amp;autohide=2&amp;theme=dark&amp;color=red&amp;" width="300" height="150" allowfullscreen="allowfullscreen" data-no-lazy="1" data-mce-fragment="1"></iframe>
<p class="hide-if-no-js"></p>
</div>
<p>&nbsp;</p>
<p>&nbsp;</p></div>
	<div class="featured-prod-container" style="clear:both; margine-top:2em;">
	<div class="title-home">
		<h3><span>Products you might be interested in...</span></h3>
	</div>
	<div class="slider-prod-container">
		<div class="slider-prod">
			 			<div class="item item-post">
			
																									<div class="image-blog static-image">
					<a href="https://www.everybodyshops.com/women-s-braided-sun-hat-black-white-upf-50.html" class="img-link" title="Women's Braided Sun Hat - Black &amp; White UPF 50+">
					<img src="https://www.everybodyshops.com/pub/media/catalog/product/e/g/egbkwhitehat.jpg" alt="" />
					</a>
				</div>
				 			
					
				<div class="info-blog">
						<div class="date-post-title">
																			<h3 class="postTitle"><a href="https://www.everybodyshops.com/women-s-braided-sun-hat-black-white-upf-50.html">Women's Braided Sun Hat - Black &amp; White UPF 50+</a></h3>
						</div>
						<div class="product-price">Price: $22.99</div>
						
							
					</div>
				</div>

			 			<div class="item item-post">
			
																									<div class="image-blog static-image">
					<a href="https://www.everybodyshops.com/egsloggers-5114qp-10.html" class="img-link" title="Sloggers - sz-10">
					<img src="https://www.everybodyshops.com/pub/media/catalog/product/e/g/egsloggersprp.jpg" alt="" />
					</a>
				</div>
				 			
					
				<div class="info-blog">
						<div class="date-post-title">
																			<h3 class="postTitle"><a href="https://www.everybodyshops.com/egsloggers-5114qp-10.html">Sloggers - sz-10</a></h3>
						</div>
						<div class="product-price">Price: $29.99</div>
						
							
					</div>
				</div>

			 			<div class="item item-post">
			
																									<div class="image-blog static-image">
					<a href="https://www.everybodyshops.com/egsloggers-5017cwp-9.html" class="img-link" title="Sloggers - sz-9">
					<img src="https://www.everybodyshops.com/pub/media/catalog/product/e/g/egsloggerspnk_1.jpg" alt="" />
					</a>
				</div>
				 			
					
				<div class="info-blog">
						<div class="date-post-title">
																			<h3 class="postTitle"><a href="https://www.everybodyshops.com/egsloggers-5017cwp-9.html">Sloggers - sz-9</a></h3>
						</div>
						<div class="product-price">Price: $39.99</div>
						
							
					</div>
				</div>

			 			<div class="item item-post">
			
																									<div class="image-blog static-image">
					<a href="https://www.everybodyshops.com/egsloggers-5117fly-10.html" class="img-link" title="Sloggers - sz-10">
					<img src="https://www.everybodyshops.com/pub/media/catalog/product/e/g/egsloggers_blk.jpg" alt="" />
					</a>
				</div>
				 			
					
				<div class="info-blog">
						<div class="date-post-title">
																			<h3 class="postTitle"><a href="https://www.everybodyshops.com/egsloggers-5117fly-10.html">Sloggers - sz-10</a></h3>
						</div>
						<div class="product-price">Price: $29.99</div>
						
							
					</div>
				</div>

			 			<div class="item item-post">
			
																									<div class="image-blog static-image">
					<a href="https://www.everybodyshops.com/egsloggers-5114bp-7.html" class="img-link" title="Sloggers - sz-7">
					<img src="https://www.everybodyshops.com/pub/media/catalog/product/e/g/egsloggersflr_3.jpg" alt="" />
					</a>
				</div>
				 			
					
				<div class="info-blog">
						<div class="date-post-title">
																			<h3 class="postTitle"><a href="https://www.everybodyshops.com/egsloggers-5114bp-7.html">Sloggers - sz-7</a></h3>
						</div>
						<div class="product-price">Price: $29.99</div>
						
							
					</div>
				</div>

			 			<div class="item item-post">
			
																									<div class="image-blog static-image">
					<a href="https://www.everybodyshops.com/egsloggers-5016fp-9.html" class="img-link" title="Sloggers - sz-9">
					<img src="https://www.everybodyshops.com/pub/media/catalog/product/e/g/egsloggers_prp_1.jpg" alt="" />
					</a>
				</div>
				 			
					
				<div class="info-blog">
						<div class="date-post-title">
																			<h3 class="postTitle"><a href="https://www.everybodyshops.com/egsloggers-5016fp-9.html">Sloggers - sz-9</a></h3>
						</div>
						<div class="product-price">Price: $39.99</div>
						
							
					</div>
				</div>

			 			<div class="item item-post">
			
																					 			
					
				<div class="info-blog">
						<div class="date-post-title">
																			<h3 class="postTitle"><a href="https://www.everybodyshops.com/tote-bag-with-hand-tools.html">Tote Bag with hand tools</a></h3>
						</div>
						<div class="product-price">Price: $21.95</div>
						
							
					</div>
				</div>

			 			<div class="item item-post">
			
																									<div class="image-blog static-image">
					<a href="https://www.everybodyshops.com/egsloggers-5116cdy-9.html" class="img-link" title="Sloggers - sz-9">
					<img src="https://www.everybodyshops.com/pub/media/catalog/product/e/g/egsloggers_yel_1.jpg" alt="" />
					</a>
				</div>
				 			
					
				<div class="info-blog">
						<div class="date-post-title">
																			<h3 class="postTitle"><a href="https://www.everybodyshops.com/egsloggers-5116cdy-9.html">Sloggers - sz-9</a></h3>
						</div>
						<div class="product-price">Price: $29.99</div>
						
							
					</div>
				</div>

			 			<div class="item item-post">
			
																									<div class="image-blog static-image">
					<a href="https://www.everybodyshops.com/egsloggers-5016cdy-6.html" class="img-link" title="Sloggers - sz-6">
					<img src="https://www.everybodyshops.com/pub/media/catalog/product/e/g/egsloggersdfd_4.jpg" alt="" />
					</a>
				</div>
				 			
					
				<div class="info-blog">
						<div class="date-post-title">
																			<h3 class="postTitle"><a href="https://www.everybodyshops.com/egsloggers-5016cdy-6.html">Sloggers - sz-6</a></h3>
						</div>
						<div class="product-price">Price: $39.99</div>
						
							
					</div>
				</div>

			 			<div class="item item-post">
			
																									<div class="image-blog static-image">
					<a href="https://www.everybodyshops.com/egsloggers-5301bk-10.html" class="img-link" title="Sloggers - sz-10">
					<img src="https://www.everybodyshops.com/pub/media/catalog/product/e/g/egsloggers_5301bk_1.jpg" alt="" />
					</a>
				</div>
				 			
					
				<div class="info-blog">
						<div class="date-post-title">
																			<h3 class="postTitle"><a href="https://www.everybodyshops.com/egsloggers-5301bk-10.html">Sloggers - sz-10</a></h3>
						</div>
						<div class="product-price">Price: $34.95</div>
						
							
					</div>
				</div>

			 			<div class="item item-post">
			
																									<div class="image-blog static-image">
					<a href="https://www.everybodyshops.com/egsloggers-5016cdy-10.html" class="img-link" title="Sloggers - sz-10">
					<img src="https://www.everybodyshops.com/pub/media/catalog/product/e/g/egsloggersdfd.jpg" alt="" />
					</a>
				</div>
				 			
					
				<div class="info-blog">
						<div class="date-post-title">
																			<h3 class="postTitle"><a href="https://www.everybodyshops.com/egsloggers-5016cdy-10.html">Sloggers - sz-10</a></h3>
						</div>
						<div class="product-price">Price: $39.99</div>
						
							
					</div>
				</div>

			 			<div class="item item-post">
			
																									<div class="image-blog static-image">
					<a href="https://www.everybodyshops.com/egsloggers-5017cwp-10.html" class="img-link" title="Sloggers - sz-10">
					<img src="https://www.everybodyshops.com/pub/media/catalog/product/e/g/egsloggerspnk.jpg" alt="" />
					</a>
				</div>
				 			
					
				<div class="info-blog">
						<div class="date-post-title">
																			<h3 class="postTitle"><a href="https://www.everybodyshops.com/egsloggers-5017cwp-10.html">Sloggers - sz-10</a></h3>
						</div>
						<div class="product-price">Price: $39.99</div>
						
							
					</div>
				</div>

					</div>
	</div>
</div>

<script type="text/javascript">
	require([
		'jquery',
		'owlcarousel'
	], function ($) {
					var prod_slider = $(".slider-prod");
					prod_slider.owlCarousel({				
						nav: true,
						dots:false,
						margin: 30,
						
						responsive: {
							0: {
								items:1
							},
							480: {
								items:2
							},
							768: {
								items:4
							},
							991: {
								items:4
							},						
							1200: {
								items:6
							}
						}
					});	  
				});
</script>



</div><div class="post-meta">			<div class="post-categories post-meta-item"><strong>Categories:</strong> <a href="https://www.everybodyshops.com/content/category/everybodygardens/">Everybody Gardens</a></div>
			<div class="post-date post-meta-item"><strong>Posted On:</strong> April 27, 2017</div>
	<div class="post-user post-meta-item"><strong>Posted By:</strong> <a href="https://www.everybodyshops.com/content/author/everyadmin/">EverybodyShops</a></div>
</div></div></div></div>



</div>




</div></main><footer class="page-footer"><div class="footer footer-wrapper">
<div class="footer-container footer-style-5">
	<div class="footer-wrapper">
		<div class="footer-top-services">
			<div class="container">
							</div>
		</div>
		
		<div class="container">
			<div class="socials-footer">
				<div class="socials-wrap">
	<div class="title-follow">Follow Us</div>
	<ul>
				<li class="li-social facebook-social">
			<a title="Facebook" href="https://www.facebook.com/EverybodyShops" target="_blank"> 
				<span class="fa fa-facebook icon-social"></span><span class="name-social">Facebook</span> 
			</a>
		</li>
				
				<li class="li-social twitter-social">
			<a title="Twitter" href="https://twitter.com/Everybody_Shops" target="_blank"> 
				<span class="fa fa-twitter icon-social"></span> <span class="name-social">Twitter</span> 
			</a>
		</li>
				
				<li class="li-social instagram-social">
			<a title="Instagram" href="https://www.instagram.com/everybody_shops/" target="_blank"> 
				<span class="fa fa-instagram icon-social"></span> <span class="name-social">Instagram</span> 
			</a>
		</li>
				
				<li class="li-social youtube-social">
			<a title="YouTube" href="https://www.youtube.com/channel/UCxkNEobnDuwTbCLp7Q9-14w" target="_blank"> 
				<span class="fa fa-youtube icon-social"></span> <span class="name-social">YouTube</span> 
			</a>
		</li>
				
				<li class="li-social pinterest-social">
			<a title="Pinterest" href="https://www.pinterest.com/everybodyshops/" target="_blank"> 
				<span class="fa fa-pinterest icon-social"></span> <span class="name-social">Pinterest</span> 
			</a>
		</li>
			</ul>
</div>			</div>
			
			<div class="footer-top">
				<div class="row">
					<div class="col-md-3 col-sm-6 col-xs-12 about-store">
						<div class="footer-block">
<div class="footer-block-title">
<h3>About Market</h3>
</div>
<div class="footer-block-content">
<ul class="links-footer">
<li><a title="About Us" href="https://www.everybodyshops.com/about-us.html">About Us</a></li>
<li><a title="Market Reviews" href="#">Market Reviews</a></li>
<li><a title="Terms of Use" href="https://www.everybodyshops.com/terms/">Terms of Use</a></li>
<li><a title="Privacy Policy" href="#">Privacy Policy</a></li>
<li><a title="Site Map" href="#">Site Map</a></li>
</ul>
</div>
</div>					</div>
					
					<div class="col-md-3 col-sm-6 col-xs-12 customer-services">
						<div class="footer-block">
<div class="footer-block-title">
<h3>Customer Service</h3>
</div>
<div class="footer-block-content">
<ul class="links-footer">
<li><a title="Shipping Policy" href="#">Shipping Policy</a></li>
<li><a title="Compensation First" href="#">Compensation First</a></li>
<li><a title="My Account" href="https://www.everybodyshops.com/customer/account">My Account</a></li>
<li><a title="Return Policy" href="#">Return Policy</a></li>
<li><a title="Contact Us" href="https://www.everybodyshops.com/contact-us.html">Contact Us</a></li>
</ul>
</div>
</div>					</div>
					
					<div class="col-md-3 col-sm-6 col-xs-12 payment-shipping">
						<!--<div class="footer-block">
							<div class="footer-block-title">
								<h3>Payment & Shipping</h3>
							</div>
							
							<div class="footer-block-content">
								<ul class="links-footer">
									<li><a href="#" title="Terms of Use">Terms of Use</a></li>
									<li><a href="#" title="Payment Methods">Payment Methods</a></li>
									<li><a href="#" title="Shipping Guide">Shipping Guide</a></li>
									<li><a href="#" title="Locations We Ship To">Locations We Ship To</a></li>
									<li><a href="#" title="Estimated Delivery Time">Estimated Delivery Time</a></li>
								</ul>
							</div>
						</div>-->					</div>
					
					<div class="col-md-3 col-sm-6 col-xs-12 contact-footer">
						<div class="footer-block">
<div class="footer-block-title">
<h3>Contact Us</h3>
</div>
<div class="footer-block-content">
<ul class="links-contact">
<li class="add-icon">535media, LLC.<br>535 Keystone Drive,<br>Warrendale, PA 15086</li>
<li class="email-icon middle-content"><a href="mailto:everybodyshops@535mediallc.com">everybodyshops@535mediallc.com</a></li>
<li class="phone-icon middle-content">800-228-0142</li>
</ul>
</div>
</div>					</div>
					
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="bottom-content">
					<div class="row">
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 copyright-footer">
															<address>535media LLC &copy; 2017. All Rights Reserved. Designed by
<a title="535mediallc.com" target="_blank" href="http://www.535mediallc.Com">535mediallc.com</a>
				</address>
													</div>
						
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 footer-payment">
							<img src="https://www.everybodyshops.com/pub/media/wysiwyg/payment/payment-6.png" alt="Payment" />						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
			<a id="yt-totop" href="javascript:void(0)" title="Go to Top"></a>
	</div>




</div></footer>
<div id="cartpro_process" class="cartpro-process">
	<div class="cartpro-loadmark">
		<div class="cartpro-imageload">
			<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
			<div class="cartpro-text">
				Please wait...			</div>
		</div>
	</div>
</div>
<div id="cartpro_modal" class="cartpro-modal ">
	<div class="cpmodal-wrapper">
		<span class="cpmodal-close cp-close"><i class="fa fa-times"></i></span>
		<div class="cpmodal-message"></div>
		
		<div class="content-cpcart">
			<div class="cpmodal-iframe"></div>
			<div class="cpmodal-action">
				<a class="cpmodal-button cp-close cartpro-time" title="Continue"  href="#">
					<span class="cpmodal-time"></span>Continue</a>
				<a class="cpmodal-button  cpmodal-viewcart cpmodal-display  cartpro-hidden" 
					title="View cart & checkout" 
					href="https://www.everybodyshops.com/checkout/cart/">
					View cart & checkout				</a>
				<a  class="cpmodal-button  cartpro-wishlist cpmodal-display  cartpro-hidden " href="https://www.everybodyshops.com/wishlist/">Go to Wishlist</a>
				<a  class="cpmodal-button  cartpro-compare cpmodal-display  cartpro-hidden" href="https://www.everybodyshops.com/catalog/product_compare/index/uenc/aHR0cHM6Ly93d3cuZXZlcnlib2R5c2hvcHMuY29tL2NvbnRlbnQvcGxhbnQtcGFuc2llcy1ub3ctZm9yLWluc3RhbnQtY29sb3Iv/">Go to Compare</a>
			</div>
			<div class="cpmodal-form"> </div>
		</div>
	</div>
</div></div>    </body>
</html>
