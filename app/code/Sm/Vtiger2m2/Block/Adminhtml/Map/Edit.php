<?php

namespace Sm\Vtiger2m2\Block\Adminhtml\Map;

class Edit extends \Magesales\VtigerCrm\Block\Adminhtml\Map\Edit
{
    /**
     * Initialize  edit block
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_objectId = 'id';
        $this->_blockGroup = 'Magesales_VtigerCrm';
        $this->_controller = 'adminhtml_map';

        parent::_construct();

        $this->buttonList->update('save', 'label', __('Save Mapping'));
        $this->buttonList->add(
            'saveandcontinue',
            [
                'label' => __('Save and Continue Edit'),
                'class' => 'save',
                'data_attribute' => [
                    'mage-init' => [
                        'button' => ['event' => 'saveAndContinueEdit', 'target' => '#edit_form'],
                    ],
                ]
            ],
            -100
        );
        $this->buttonList->add(
            'updateallfields',
            [
                'label' => __('Update All Fields'),
            ],
            -90
        );

        $this->buttonList->update('delete', 'label', __('Delete'));

        // Update new delete link
        foreach ($this->buttonList->getItems() as $value) {
            if (isset($value['delete'])) {
                $deleteOnClick = $value['delete']['onclick'];
                $idIndex = strpos($deleteOnClick, 'id');
                $length = strpos($deleteOnClick, 'key') - $idIndex - 4;
                $key = substr($deleteOnClick, $idIndex + 3, $length);
                $newDeleteLink = $this->getUrl('vtiger2m2/map/delete/id/' . $key);
                $newDeleteLink = 'deleteConfirm(\'Are you sure you want to do this?\', \'' . $newDeleteLink . '\')';
                $this->buttonList->update('delete', 'onclick', $newDeleteLink);
            }
        }
    }
}
