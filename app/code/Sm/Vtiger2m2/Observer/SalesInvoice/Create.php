<?php
namespace Sm\Vtiger2m2\Observer\SalesInvoice;

use Magesales\VtigerCrm\Model\Data;
use Magento\Framework\Event\Observer;
use Magento\Store\Model\ScopeInterface;

class Create extends \Magesales\VtigerCrm\Observer\SalesInvoice\Create
{
    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        // Fix bug duplicate invoice
        $increment_id = $observer->getEvent()->getInvoice()->getIncrementId();
        if ($this->_scopeConfig->isSetFlag(Data::XML_PATH_ALLOW_SYNC_INVOICE, ScopeInterface::SCOPE_STORE)) {
            $this->_invoice->sync($increment_id);
        }
    }
}
