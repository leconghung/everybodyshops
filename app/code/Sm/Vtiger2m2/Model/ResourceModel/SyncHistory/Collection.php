<?php

namespace Sm\Vtiger2m2\Model\ResourceModel\SyncHistory;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Sm\Vtiger2m2\Model\SyncHistory;
use Sm\Vtiger2m2\Model\ResourceModel\SyncHistory as ResourceSyncHistory;

/**
 * Class Collection
 * @package Sm\Vtiger2m2\Model\ResourceModel\SyncHistory
 */
class Collection extends AbstractCollection
{
    /**
     * Id Field Name
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(SyncHistory::class, ResourceSyncHistory::class);
    }
}
