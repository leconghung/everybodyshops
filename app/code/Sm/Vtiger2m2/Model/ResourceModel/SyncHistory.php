<?php

namespace Sm\Vtiger2m2\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class SyncHistory
 * @package Sm\Vtiger2m2\Model\ResourceModel
 */
class SyncHistory extends AbstractDb
{
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('sync_history', 'id');
    }
}
