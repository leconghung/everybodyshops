<?php

namespace Sm\Vtiger2m2\Model;

use Magento\Framework\Model\AbstractModel;
use Sm\Vtiger2m2\Model\ResourceModel\SyncHistory as ResourceSyncHistory;

/**
 * Class SyncHistory
 * @package Sm\Vtiger2m2\Model
 */
class SyncHistory extends AbstractModel
{
    /**
     * Construction
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceSyncHistory::class);
    }
}
