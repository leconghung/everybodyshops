<?php

namespace Sm\Vtiger2m2\Model\SyncToMagento;

class Product
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    protected $_productAttributeCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var \Magesales\VtigerCrm\Model\Map
     */
    protected $_vtigerMap;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Zend\Log\Logger
     */
    protected $_logger;

    /**
     * @var \Magento\CatalogInventory\Api\StockRegistryInterface
     */
    protected $_stockRegistry;

    /**
     * @var \Sm\Vtiger2m2\Model\iConnector
     */
    protected $_connector;

    /**
     * @var \Magesales\VtigerCrm\Model\MapFactory
     */
    protected $_mapFactory;

    /**
     * @var \Magento\ConfigurableProduct\Model\Product\Type\Configurable
     */
    protected $_configurable;

    /**
     * @var \Magento\ConfigurableProduct\Helper\Product\Options\Factory
     */
    protected $_optionFactory;

    /**
     * @var \Magento\Eav\Model\Config
     */
    protected $_eavConfig;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var \Sm\Vtiger2m2\Helper\Data
     */
    protected $_vtigerHelper;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $_directoryList;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Gallery
     */
    protected $_galleryEntry;

    /**
     * Product constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magesales\VtigerCrm\Model\Map $map
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Sm\Vtiger2m2\Model\iConnector $iConnector
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute $attribute
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $productAttribute
     * @param \Magesales\VtigerCrm\Model\MapFactory $mapFactory
     * @param \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable
     * @param \Magento\ConfigurableProduct\Helper\Product\Options\Factory $optionFactory
     * @param \Magento\Eav\Model\Config $config
     * @param \Magento\Catalog\Api\ProductRepositoryInterface $productRepository
     * @param \Sm\Vtiger2m2\Helper\Data $vtigerHelper
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     * @param \Magento\Catalog\Model\ResourceModel\Product\Gallery $entry
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magesales\VtigerCrm\Model\Map $map,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Psr\Log\LoggerInterface $logger,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Sm\Vtiger2m2\Model\iConnector $iConnector,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable\Attribute $attribute,
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $productAttribute,
        \Magesales\VtigerCrm\Model\MapFactory $mapFactory,
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $configurable,
        \Magento\ConfigurableProduct\Helper\Product\Options\Factory $optionFactory,
        \Magento\Eav\Model\Config $config,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Sm\Vtiger2m2\Helper\Data $vtigerHelper,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        \Magento\Catalog\Model\ResourceModel\Product\Gallery $entry
    )
    {
        $this->_productAttributeCollectionFactory = $collectionFactory;
        $this->_productFactory = $productFactory;
        $this->_vtigerMap = $map;
        $this->_storeManager = $storeManager;
        $this->_stockRegistry = $stockRegistry;
        $this->_connector = $iConnector;
        $this->_mapFactory = $mapFactory;
        $this->_configurable = $configurable;
        $this->_optionFactory = $optionFactory;
        $this->_eavConfig = $config;
        $this->_productRepository = $productRepository;
        $this->_vtigerHelper = $vtigerHelper;
        $this->_directoryList = $directoryList;
        $this->_galleryEntry = $entry;

        // add log file
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/sync_vtiger2m2_product.log');
        $this->_logger = new \Zend\Log\Logger();
        $this->_logger->addWriter($writer);
    }

    /**
     * @param $productsData
     * @param $skuUsed
     * @param $configurableList
     * @param $syncSpecified
     * @param $specifiedList
     * @param $baseOnModified
     * @param $lastSyncTime
     * @return array
     */
    public function createProduct($productsData, &$skuUsed, &$configurableList, $syncSpecified, $specifiedList, $baseOnModified, $lastSyncTime)
    {
        $this->_logger->info('============================== Synchronizing product from vTgier to Magento =============================', []);

        $countUpdate = $countCreateNew = $countIgnore = $countError = 0;
        $countProducts = count($productsData['result']);
        foreach ($productsData['result'] as $aProduct) {
            try {
                // get data by mapped list
                $mappedField = $this->_vtigerHelper->mapField($aProduct, 'Products');

                // Sync specified
                if ($syncSpecified) {
                    if (!in_array($mappedField['sku'], $specifiedList)) {
                        continue;
                    }
                } else if ($baseOnModified) {
                    // check with modified time
                    if ($aProduct['modifiedtime'] < $lastSyncTime) {
                        continue;
                    }
                }

                $this->_logger->info('Checking... ', ['name' => $mappedField['name'], 'sku' => $mappedField['sku']]);

                // if don't have sku, jump to next loop
                if ($mappedField['sku'] == null) {
                    $this->_logger->info('>>> Error: can not update this product because sku is null: ', ['name' => $mappedField['name'], 'sku' => $mappedField['sku']]);
                    $countError++;
                    continue;
                }

                // add sku to used list
                $skuUsed[] = $mappedField['sku'];

                // check configurable
                if ($aProduct['cf_parent_product'] != null || $aProduct['cf_attributes_configurations'] != null) {
                    $configurableList[] = $aProduct;
                }

                /** @var \Magento\Catalog\Model\Product $_product */
                $_product = $this->_productFactory->create();
                $id = $_product->getIdBySku($mappedField['sku']);
                if ($id) {
                    /*
                     * The code below will update the product
                     */

                    $existedProduct = $this->_productFactory->create()->load($id);

                    // Configurable product will be updated later
                    if ($existedProduct->getTypeId() == 'configurable') {
                        $countIgnore++;
                        $this->_logger->info('>>> Temporary ignore this configurable product (it will be updated at next step!): ', []);
                        continue;
                    }

                    // Set store - website
                    $existedProduct->setStoreId(0);

                    // Stock
                    $stockArr = [];
                    $stockArr['use_config_manage_stock'] = 0;
                    $stockArr['manage_stock'] = 1;
                    // set data based on mapped fields
                    foreach ($mappedField as $key => $value) {
                        if (strpos($key, 'stock') !== false) {
                            if ($key == 'stock_qty') {
                                if ($value > 0) {
                                    $stockArr['is_in_stock'] = 1;
                                } else {
                                    $stockArr['is_in_stock'] = 0;
                                }
                            }
                            $stockKey = substr($key, 6, strlen($key) - 6);
                            $stockArr[$stockKey] = $value;
                        } else {
                            $existedProduct->setData($key, $value);
                        }
                    }
                    if (isset($mappedField['category_ids'])) {
                        $mappedField['category_ids'] = $this->_vtigerHelper->getCategoryIds($mappedField['category_ids']);
                    }
                    if (!isset($mappedField['visibility']) || (isset($mappedField['visibility']) && $mappedField['visibility'] == null)) {
                        $existedProduct->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
                    }

                    // Set image for product
                    $mediaPath = $this->_directoryList->getPath('media');
                    if (!file_exists($mediaPath . '/catalog/product/crm')) {
                        mkdir($mediaPath . '/catalog/product/crm', 0777, true);
                    }
                    $crmPath = $mediaPath . '/catalog/product/crm';
                    if (isset($aProduct['image_path']) && $aProduct['image_path'] != null) {
                        $newImageUsed = $imageUsed = [];
                        foreach ($aProduct['image_path'] as $imageItem) {
                            $splitter = explode('/', $imageItem);
                            $imageName = $splitter[count($splitter) - 1];
                            $newImageUsed[] = $imageName;
                        }
                        foreach ($existedProduct->getMediaGalleryImages()->getItems() as $key => $entry) {
                            $splitter = explode('/', $entry->getData('file'));
                            $imageName = $splitter[count($splitter) - 1];
                            $imageUsed[] = $imageName;
                        }

                        // Remove image
                        $countRm = 0;
                        $newImageUsed = $this->addMoreCaseImagePath($newImageUsed);
                        $mediaGalleryEntries = $existedProduct->getMediaGalleryEntries();
                        if ($mediaGalleryEntries != null) {
                            foreach ($mediaGalleryEntries as $key => $entry) {
                                $file = $entry->getFile();
                                $tmpArr = explode('/', $file);
                                $imageName = $tmpArr[count($tmpArr) - 1];
                                if (!in_array($imageName, $newImageUsed)) {
                                    //unset($mediaGalleryEntries[$key]);
                                    //$this->_galleryEntry->deleteGallery($entry->getId());
                                    $countRm++;
                                }
                            }
                            $existedProduct->setMediaGalleryEntries($mediaGalleryEntries);
                        }

                        // Reset thumbnail
                        if ($countRm > 0) {
                            if ($existedProduct->getMediaGalleryImages()->count() > 0) {
                                $lastImage = $existedProduct->getMediaGalleryImages()->getLastItem();
                                $lastImagePath = $crmPath . $lastImage->getData('file');
                                $this->_galleryEntry->deleteGallery($lastImage->getData('id'));
                                if (file_exists($lastImagePath)) {
                                    $existedProduct->addImageToMediaGallery($lastImagePath, array('image', 'thumbnail', 'small_image'), false, false);
                                }
                            }
                        }

                        // Add new image
                        foreach ($aProduct['image_path'] as $imageItem) {
                            $splitter = explode('/', $imageItem);
                            $imageName = $splitter[count($splitter) - 1];
                            if ($this->checkAddImageOrNot($imageName, $imageUsed)) {
                                $imagePath = $crmPath . '/' . $imageName;
                                $flag = true;
                                if (!file_exists($imagePath) && @getimagesize($imageItem)) {
                                    $flag = copy($imageItem, $imagePath);
                                } else if (!file_exists($imagePath) && !@getimagesize($imageItem)) {
                                    $flag = false;
                                }
                                if ($flag) {
                                    $existedProduct->addImageToMediaGallery($imagePath, array('image', 'thumbnail', 'small_image'), false, false);
                                }
                            }
                        }
                    } else {
                        // remove all image
                        $existingMediaGalleryEntries = $existedProduct->getMediaGalleryEntries();
                        if ($existingMediaGalleryEntries != null) {
                            foreach ($existingMediaGalleryEntries as $key => $entry) {
                                unset($existingMediaGalleryEntries[$key]);
                                $this->_galleryEntry->deleteGallery($entry->getId());
                            }
                            $existedProduct->setMediaGalleryEntries($existingMediaGalleryEntries);
                        }
                    }

                    $existedProduct->setStockData($stockArr);
                    $existedProduct->setQuantityAndStockStatus($stockArr);
                    $existedProduct->save();
                    // set stock by Stock Item
                    $this->setStockData($existedProduct, $stockArr);
                    $countUpdate++;
                    $this->_logger->info('>>> Updated: ', ['name' => $mappedField['name'], 'sku' => $mappedField['sku']]);
                } else {
                    /*
                     * The code below will create new product
                     */

                    // set default data
                    $_product->setTypeId(\Magento\Catalog\Model\Product\Type::TYPE_SIMPLE);
                    // attribute set id
                    $_product->setAttributeSetId($_product->getDefaultAttributeSetId());
                    // website id
                    $websiteId = $this->_storeManager->getDefaultStoreView()->getWebsiteId();
                    $_product->setWebsiteIds([$websiteId]);
                    $_product->setStoreId(0);
                    // set visibility and status
                    $_product->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
                    $_product->setStatus(\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED);

                    $stockArr = [];
                    $stockArr['use_config_manage_stock'] = 0;
                    $stockArr['manage_stock'] = 1;
                    // set data based on mapped fields
                    foreach ($mappedField as $key => $value) {
                        if (strpos($key, 'stock') !== false) {
                            if ($key == 'stock_qty') {
                                if ($value > 0) {
                                    $stockArr['is_in_stock'] = 1;
                                } else {
                                    $stockArr['is_in_stock'] = 0;
                                }
                            }
                            $stockKey = substr($key, 6, strlen($key) - 6);
                            $stockArr[$stockKey] = $value;
                        } else {
                            $_product->setData($key, $value);
                        }
                    }

                    if (isset($mappedField['category_ids'])) {
                        $mappedField['category_ids'] = $this->_vtigerHelper->getCategoryIds($mappedField['category_ids']);
                    }

                    // Set image for product
                    $mediaPath = $this->_directoryList->getPath('media');
                    if (!file_exists($mediaPath . '/catalog/product/crm')) {
                        mkdir($mediaPath . '/catalog/product/crm', 0777, true);
                    }
                    $crmPath = $mediaPath . '/catalog/product/crm';
                    if (isset($aProduct['image_path']) && $aProduct['image_path'] != null) {
                        foreach ($aProduct['image_path'] as $imageItem) {
                            $splitter = explode('/', $imageItem);
                            $imageName = $splitter[count($splitter) - 1];
                            $imagePath = $crmPath . '/' . $imageName;
                            $flag = true;
                            if (!file_exists($imagePath) && @getimagesize($imageItem)) {
                                $flag = copy($imageItem, $imagePath);
                            } else if (!file_exists($imagePath) && !@getimagesize($imageItem)) {
                                $flag = false;
                            }
                            if ($flag) {
                                $_product->addImageToMediaGallery($imagePath, array('image', 'thumbnail', 'small_image'), false, false);
                            }
                        }
                    }
                    $_product->setStockData($stockArr);
                    $_product->setQuantityAndStockStatus($stockArr);
                    $_product->save();

                    $this->_logger->info('>>> Created new product: ', ['name' => $mappedField['name'], 'sku' => $mappedField['sku']]);
                    $countCreateNew++;
                }
            } catch (\Exception $e) {
                $countError++;
                $this->_logger->info('>>> Error', [$e->getMessage()]);
            }
        }
        // log summary
        $this->_logger->info('======================================================================================================', []);
        $this->_logger->info('Checked ' . $countProducts . ' product(s): created:' . $countCreateNew . ' product(s), updated: '
            . $countUpdate . ' product(s), ignored: ' . $countIgnore . ' product(s), error: ' . $countError . ' product(s)');
        return [
            'create' => $countCreateNew,
            'update' => $countUpdate,
            'ignore' => $countIgnore,
            'error' => $countError
        ];
    }

    /**
     * @param $products
     * this list contain configurable product and its children
     * @return array
     */
    public function createConfigurableProduct($products)
    {
        $countConfig = 0;
        $this->_logger->info('================================== Update configurable product step ==================================', []);
        foreach ($products as $product) {
            try {
                // get data by mapped list
                $mappedField = $this->_vtigerHelper->mapField($product, 'Products');

                /** @var \Magento\Catalog\Model\Product $_product */
                $_product = $this->_productFactory->create();
                $pid = $_product->getIdBySku($product['productcode']);

                // TODO: check product has value of cf_attributes_configurations
                if ($product['cf_attributes_configurations'] != null) {
                    // add log
                    $this->_logger->info('Updating configurable product ... ', ['name' => $mappedField['name'], 'sku' => $mappedField['sku']]);

                    // children products - Product Ids Of Associated Products
                    $associatedProductIds = [];
                    $associatedProductSku = [];
                    foreach ($products as $subProduct) {
                        if ($subProduct['cf_parent_product'] == $product['id']) {
                            $associatedProductIds[] = $this->_productFactory->create()->getIdBySku($subProduct['productcode']);
                            $associatedProductSku[] = $subProduct['productcode'];
                        }
                    }
                    $this->_logger->info('+ Associated product', ['ids' => $associatedProductIds, 'skus' => $associatedProductSku]);

                    $configurable_product = $this->_productFactory->create()->load($pid);
                    $configurable_product->setTypeId('configurable');
                    $configurable_product->setStoreId(0);

                    // Stock
                    $stockItem = $this->_stockRegistry->getStockItem($pid);
                    $stockItem->setUseConfigManageStock(0);
                    $stockItem->setManageStock(1);
                    $stockArr = [];
                    $stockArr['use_config_manage_stock'] = 0;
                    $stockArr['manage_stock'] = 1;
                    $mappedField['stock_stock_id'] = 1;
                    // set data based on mapped fields
                    foreach ($mappedField as $key => $value) {
                        if (strpos($key, 'stock') !== false) {
                            if ($key == 'stock_qty') {
                                if ($value > 0) {
                                    $stockItem->setData('is_in_stock', 1);
                                    $stockArr['is_in_stock'] = 1;
                                } else {
                                    $stockItem->setData('is_in_stock', 0);
                                    $stockArr['is_in_stock'] = 0;
                                }
                            }
                            $stockKey = substr($key, 6, strlen($key) - 6);
                            $stockArr[$stockKey] = $value;
                            $stockItem->setData($stockKey, $value);
                        } else {
                            $configurable_product->setData($key, $value);
                        }
                    }
                    if (!isset($mappedField['visibility']) || (isset($mappedField['visibility']) && $mappedField['visibility'] == null)) {
                        $configurable_product->setVisibility(\Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH);
                    }
                    if (isset($mappedField['category_ids'])) {
                        $mappedField['category_ids'] = $this->_vtigerHelper->getCategoryIds($mappedField['category_ids']);
                    }
                    $stockItem->save();
                    $configurable_product->setStockData($stockArr);
                    $configurable_product->setQuantityAndStockStatus($stockArr);
                    $configurable_product->save();

                    $configurableAttributesData = [];
                    $position = 0;
                    // Super Attribute Used To Create Configurable Product
                    $superAttributes = $this->_vtigerHelper->getMagentoAttributeArr($product['cf_attributes_configurations']);
                    $this->_logger->info('+ Supper Attribute', ['attr' => $superAttributes]);
                    foreach ($superAttributes as $code) {
                        $attribute = $this->_eavConfig->getAttribute('catalog_product', $code);
                        $options = $attribute->getOptions();
                        array_shift($options); //remove the first option which is empty
                        $attributeValues = [];
                        foreach ($options as $option) {
                            $attributeValues[] = [
                                'label' => $option->getLabel(),
                                'attribute_id' => $attribute->getId(),
                                'value_index' => $option->getValue(),
                            ];
                        }
                        $configurableAttributesData[] = [
                            'attribute_id' => $attribute->getId(),
                            'code' => $attribute->getAttributeCode(),
                            'label' => $attribute->getStoreLabel(),
                            'position' => $position,
                            'values' => $attributeValues,
                        ];
                        $position++;
                    }

                    $configurableOptions = $this->_optionFactory->create($configurableAttributesData);
                    $extensionConfigurableAttributes = $configurable_product->getExtensionAttributes();
                    $extensionConfigurableAttributes->setConfigurableProductOptions($configurableOptions);
                    $extensionConfigurableAttributes->setConfigurableProductLinks($associatedProductIds);
                    $configurable_product->setExtensionAttributes($extensionConfigurableAttributes);
                    $this->_productRepository->save($configurable_product);

                    // add log
                    $this->_logger->info('>>> Updated configurable product: ', ['name' => $mappedField['name'], 'sku' => $mappedField['sku']]);
                }
            } catch (\Exception $e) {
                $this->_logger->info('>>> Error', [$e->getMessage()]);
            }
        }
        return [
            'config' => $countConfig
        ];
    }

    /**
     * @param $skuUsed
     * @param $allowDelete
     * @param $filterDate
     * @return int
     */
    public function deleteProduct($skuUsed, $allowDelete, $filterDate)
    {
        if ($allowDelete) {
            $countDelete = 0;
            try {
                $this->_logger->info('======================================================================================================', []);
                $this->_logger->info('List sku used: ', $skuUsed);
                $collection = $this->_productFactory->create()->getCollection()
                    ->addFieldToFilter('sku', ['nin' => $skuUsed])
                    ->addFieldToFilter('updated_at', ['gteq' => $filterDate])
                    ->addFieldToSelect('name');
                $countDelete = $collection->count();
                if ($countDelete > 0) {
                    $this->_logger->info('Total products will be deleted: ', [$countDelete]);
                    foreach ($collection as $product) {
                        $name = $product->getName();
                        $sku = $product->getSku();
                        $product->delete();
                        $this->_logger->info('Deleted product: ', ['name' => $name, 'sku' => $sku]);
                    }
                }
            } catch (\Exception $e) {
                $this->_logger->info('>>> Error when delete', [$e->getMessage()]);
            }
            return $countDelete;
        }
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @param array $stockData
     */
    public function setStockData($product, $stockData)
    {
        $stockItem = $this->_stockRegistry->getStockItem($product->getId());
        foreach ($stockData as $key => $value) {
            $stockItem->setData($key, $value);
        }
        $stockItem->save();
        $product->save();
    }

    /**
     * @param $imagePathArr
     * @return array
     */
    public function addMoreCaseImagePath($imagePathArr)
    {
        $result = [];
        foreach ($imagePathArr as $imageItem) {
            $result[] = $imageItem;
            $tmpArr = explode('_', $imageItem);
            $tmpArr1 = $tmpArr2 = $tmpArr;
            unset($tmpArr1[0]);
            $result[] = implode('_', $tmpArr1);
//            unset($tmpArr2[count($tmpArr2) - 1]);
//            $result[] = implode('_', $tmpArr2);
        }
        return $result;
    }

    /**
     * @param $imageName
     * @param $imageExists
     * @return bool
     */
    public function checkAddImageOrNot($imageName, $imageExists)
    {
        if (in_array($imageName, $imageExists)) {
            return false;
        }
        $tmpArr = explode('_', $imageName);
        unset($tmpArr[0]);
        $shorterImageName = implode('_', $tmpArr);
        if (in_array($shorterImageName, $imageExists)) {
            return false;
        }
        return true;
    }
}
