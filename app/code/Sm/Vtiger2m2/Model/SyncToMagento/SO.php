<?php

namespace Sm\Vtiger2m2\Model\SyncToMagento;

class SO
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Zend\Log\Logger
     */
    protected $_logger;

    /**
     * @var \Sm\Vtiger2m2\Model\iConnector
     */
    protected $_connector;

    /**
     * @var \Magesales\VtigerCrm\Model\MapFactory
     */
    protected $_mapFactory;

    /**
     * @var \Sm\Vtiger2m2\Helper\Data
     */
    protected $_vtigerHelper;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var \Magento\Sales\Model\Order\AddressFactory
     */
    protected $_orderAddress;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;

    protected $_invoiceSyncer;


    /**
     * SO constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Sm\Vtiger2m2\Model\iConnector $iConnector
     * @param \Magesales\VtigerCrm\Model\MapFactory $mapFactory
     * @param \Sm\Vtiger2m2\Helper\Data $vtigerHelper
     * @param \Magento\Sales\Model\OrderFactory $orderFactory
     * @param \Magento\Sales\Model\Order\AddressFactory $addressFactory
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Psr\Log\LoggerInterface $logger,
        \Sm\Vtiger2m2\Model\iConnector $iConnector,
        \Magesales\VtigerCrm\Model\MapFactory $mapFactory,
        \Sm\Vtiger2m2\Helper\Data $vtigerHelper,
        \Magento\Sales\Model\OrderFactory $orderFactory,
        \Magento\Sales\Model\Order\AddressFactory $addressFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Sm\Vtiger2m2\Model\SyncToVtiger\Invoice $invoiceSyncer
    )
    {
        $this->_storeManager = $storeManager;
        $this->_connector = $iConnector;
        $this->_mapFactory = $mapFactory;
        $this->_vtigerHelper = $vtigerHelper;
        $this->_orderFactory = $orderFactory;
        $this->_orderAddress = $addressFactory;
        $this->_customerFactory = $customerFactory;
        $this->_invoiceSyncer = $invoiceSyncer;

        // add log file
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/sync_vtiger2m2_so.log');
        $this->_logger = new \Zend\Log\Logger();
        $this->_logger->addWriter($writer);
    }

    public function updateSO($responseData)
    {
        if (!$responseData['success']) {
            $this->_logger->info("{$responseData['message']}", []);
            return null;
        }

        $this->_logger->info('============================== Synchronizing SO from vTiger to Magento =============================', []);

        $soData = $responseData['result'];
        foreach ($soData as $item) {
            try {
                // Only update with item has Updated In vTiger = 1
                if ($item['cf_updated_in_vt'] != 1) {
                    continue;
                }
                $this->_logger->info("Updating so {$item['subject']} ... ", []);
                // get order
                $order = $this->_orderFactory->create()->loadByIncrementId($item['subject']);
                // set new data
                if ($order->getEntityId() != null) {
                    // update account
                    $customerFirstname = $customerLastname = '';
                    $vTigerAccountResponse = $this->_connector->getAccount($item['account_id']);
                    if ($vTigerAccountResponse['success']) {
                        $vTigerAccount = $vTigerAccountResponse['result'][0];
                        // website id
                        $websiteId = $this->_storeManager->getDefaultStoreView()->getWebsiteId();
                        $customer = $this->_customerFactory->create()->setWebsiteId($websiteId)->loadByEmail($vTigerAccount['email1']);;
                        if ($customer->getId()) {
                            // Order with exited account
                            $order->setCustomerEmail($customer->getEmail());
                            $order->setCustomerFirstname($customer->getData('firstname'));
                            $order->setCustomerLastname($customer->getData('lastname'));
                            $order->setCustomerMiddlename($customer->getData('middlename'));
                            $order->setCustomerId($customer->getEntityId());
                            $order->setCustomerDob($customer->getData('dob'));
                            $order->setCustomerGender($customer->getData('gender'));
                            $order->setCustomerPrefix($customer->getData('prefix'));
                            $order->setCustomerSuffix($customer->getData('suffix'));

                            $customerFirstname = $customer->getData('firstname');
                            $customerLastname = $customer->getData('lastname');
                        } else {
                            // Order with guess account
                            $order->setCustomerEmail($vTigerAccount['email1']);
                            $order->setCustomerFirstname($vTigerAccount['accountname']);
                            $order->setCustomerLastname($vTigerAccount['accountname']);

                            $customerFirstname = $vTigerAccount['accountname'];
                            $customerLastname = $vTigerAccount['accountname'];
                        }
                    }

                    // get mapped fields
                    $mappedFields = $this->_vtigerHelper->mapFieldSO($item, 'SalesOrder');

                    $billingAddress = $shippingAddress = null;
                    $countBilling = $countShipping = 0;
                    $billingAddressCollection = $this->_orderAddress->create()->getCollection()
                        ->addFieldToFilter('parent_id', $order->getEntityId())
                        ->addFieldToFilter('address_type', 'billing');
                    if ($billingAddressCollection->count() > 0) {
                        /** @var \Magento\Sales\Model\Order\Address $billingAddress */
                        $billingAddress = $billingAddressCollection->getFirstItem();
                        $billingAddress->setFirstname($customerFirstname);
                        $billingAddress->setLastname($customerLastname);
                    }
                    $shippingAddressCollection = $this->_orderAddress->create()->getCollection()
                        ->addFieldToFilter('parent_id', $order->getEntityId())
                        ->addFieldToFilter('address_type', 'shipping');
                    if ($shippingAddressCollection->count() > 0) {
                        /** @var \Magento\Sales\Model\Order\Address $shippingAddress */
                        $shippingAddress = $shippingAddressCollection->getFirstItem();
                        $shippingAddress->setFirstname($customerFirstname);
                        $shippingAddress->setLastname($customerLastname);
                    }

                    foreach ($mappedFields as $key => $value) {
                        if (strpos($key, 'bill_') !== false) {
                            if ($billingAddress != null) {
                                $billingAddress->setData(str_replace('bill_', '', $key), $value);
                                $countBilling++;
                                continue;
                            }
                        }
                        if (strpos($key, 'ship_') !== false) {
                            if ($shippingAddress != null) {
                                $shippingAddress->setData(str_replace('ship_', '', $key), $value);
                                $countShipping++;
                                continue;
                            }
                        }
                        $order->setData($key, $value);
                    }

                    $billingAddress->save();
                    $shippingAddress->save();

                    // Save
                    $order->save();
                    $this->_logger->info('>>> Updated SO: ', ['increment_id' => $item['subject']]);

                    // Update invoice one more time to update billing+shipping address
                    $invoiceCollection = $order->getInvoiceCollection()->addFieldToFilter('order_id', $order->getEntityId());
                    if ($invoiceCollection->count() > 0) {
                        /** @var \Magento\Sales\Model\Order\Invoice $invoice */
                        $invoice = $invoiceCollection->getFirstItem();
                        // save or call directly to update function
                        $this->_invoiceSyncer->sync($invoice->getIncrementId());
                    }
                } else {
                    $this->_logger->info(">>> SO {$item['subject']} doesn't exists", []);
                }
            } catch (\Exception $e) {
                $this->_logger->info('>>> Error', [$e->getMessage()]);
            }
        }
    }
}
