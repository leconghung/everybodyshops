<?php

namespace Sm\Vtiger2m2\Model;

use Magento\Directory\Model\Country;
use Magento\Tax\Model\ClassModel;
use Magesales\VtigerCrm\Model\Field;
use Magesales\VtigerCrm\Model\MapFactory;
use WeltPixel\GoogleTagManager\lib\Google\Exception;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;

class Data extends \Magesales\VtigerCrm\Model\Data
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $_attribute
     */
    protected $_attribute;

    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $_configurable;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_productModel;

    /**
     * @var  \Sm\Vtiger2m2\Model\iConnector
     */
    protected $_iConnector;

    /**
     * @var \Sm\Vtiger2m2\Helper\Data
     */
    protected $_vTigerHelper;

    /**
     * @var StockItemRepository
     */
    protected $stockItemRepository;

    /**
     * @var CollectionFactory
     */
    protected $productCollectionFactory;

    protected $vmConfigurable = [];

    /**
     * Data constructor.
     * @param MapFactory $map
     * @param Field $field
     * @param Country $country
     * @param ClassModel $tax
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $attribute
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable
     * @param \Magento\Catalog\Model\ProductFactory $product
     * @param iConnector $iConnector
     * @param \Sm\Vtiger2m2\Helper\Data $vTigerHelper
     * @param StockItemRepository $stockItemRepository
     * @param CollectionFactory $productCollectionFactory
     */
    public function __construct(
        MapFactory $map,
        Field $field,
        Country $country,
        ClassModel $tax,
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $attribute,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurable,
        \Magento\Catalog\Model\ProductFactory $product,
        \Sm\Vtiger2m2\Model\iConnector $iConnector,
        \Sm\Vtiger2m2\Helper\Data $vTigerHelper,
        StockItemRepository $stockItemRepository,
        CollectionFactory $productCollectionFactory
    )
    {
        $this->_attribute = $attribute;
        $this->_configurable = $configurable;
        $this->_productModel = $product;
        $this->_iConnector = $iConnector;
        parent::__construct($map, $field, $country, $tax);
        $this->_vTigerHelper = $vTigerHelper;
        $this->stockItemRepository = $stockItemRepository;
        $this->productCollectionFactory = $productCollectionFactory;
    }


    /**
     * @param string $data
     * @param string $_type
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getMapping($data, $_type)
    {
        $model = $this->_mapFactory->create();
        $collection = $model->getResourceCollection()
            ->addFieldToFilter('type', $_type)
            ->addFieldToFilter('status', 1);
        $map = [];
        $result = [];

        foreach ($collection as $key => $value) {
            $vtiger = $value->getVtigerField();
            $magento = $value->getMagentoField();
            $map[$vtiger] = $magento;
        }
        foreach ($map as $key => $value) {
            $attr = $this->_attribute->create()->addFieldToFilter('attribute_code', $value)->getFirstItem();
            // Special case status of order has type: select
            if ($value == 'status' && $_type == "SalesOrder"){
                $result[$key] = $this->_vTigerHelper->getStatusLabelByCode($data[$value]);
                $result[$key] = str_replace('&amp;', 'and', $result[$key]);
                $result[$key] = str_replace('&', 'and', $result[$key]);
                continue;
            }

            // Case: select type
            if ($attr->getData('frontend_input') == 'select') {
                $label = $attr->getSource()->getOptionText($data[$value]);
                if ($data[$value]) {
                    $result[$key] = str_replace(',', ';', $label);
                    $result[$key] = str_replace('&amp;', 'and', $result[$key]);
                    $result[$key] = str_replace('&', 'and', $result[$key]);
                    continue;
                }

            }
            // Case: multi select type
            if ($attr->getData('frontend_input') == 'multiselect') {
                $label = $attr->getSource()->getOptionText($data[$value]);
                if ($data[$value]) {
                    $result[$key] = str_replace(',', ';', $label);
                    $result[$key] = str_replace('&amp;', 'and', $result[$key]);
                    $result[$key] = str_replace('&', 'and', $result[$key]);
                    continue;
                }
            }

            if (isset($data[$value])) {
                $result[$key] = str_replace('&amp;', 'and', $data[$value]);
                $result[$key] = str_replace('&', 'and', $result[$key]);
            }
        }
        return $result;
    }

    /**
     * @param \Magento\Catalog\Model\Product $model
     * @param string $_type
     * @return array
     * @throws \Exception
     */
    public function getProduct($model, $_type)
    {
        try {
            $magento_fields = $this->_field->getMagentoFields('product');
            $data = [];
            $data['type_id'] = $model->getTypeId();

            /*..........Pass data of Product to array..........*/
            foreach ($magento_fields as $key => $item) {
                $sub = substr($key, 0, 5);
                if ($sub == 'stock') {
                    $stockItem = $model->getExtensionAttributes()->getStockItem();
                    $data[$key] = $stockItem->getData(substr($key, 6));
                } else {
                    $data[$key] = $model->getData($key);
                }
            }

            if (!isset($data['stock_qty']) || (isset($data['stock_qty']) && $data['stock_qty'] == null)) {
                $data['stock_qty'] = 0;
            }

            if (!empty($data['country_of_manufacture'])) {
                $country_id = $data['country_of_manufacture'];
                $data['country_of_manufacture'] = $this->getCountryName($country_id);
            }
            if (!empty($data['tax_class_id'])) {
                $tax_id = $data['tax_class_id'];
                if ($tax_id == 0) {
                    $data['tax_class_id'] = "None";
                } else {
                    $data['tax_class_id'] = $this->_tax->load($tax_id)->getClassName();
                }
            }
            /*.............End pass data...............*/

            // 4. Mapping data
            $params = $this->getMapping($data, $_type);

            // Set configurable attribute for configurable product
            if ($data['type_id'] == 'configurable') {
                $configAtt = $model->getTypeInstance()->getConfigurableAttributes($model);
                if ($configAtt != null) {
                    $configAttributes = $configAtt->getData();
                    $attrIds = [];
                    $attrCodes = [];
                    // get magento configurable attribute
                    foreach ($configAttributes as $configAttribute) {
                        $attrIds[] = $configAttribute['attribute_id'];
                    }
                    $attributes = $model->getAttributes();
                    foreach ($attributes as $attribute) {
                        if (in_array($attribute->getAttributeId(), $attrIds)) {
                            $attrCodes[] = $attribute->getAttributeCode();
                        }
                    }
                    $params['cf_attributes_configurations'] = implode(' |##| ', $attrCodes);
                }
            }

            // Set parent product for simple
            if ($data['type_id'] == 'simple') {
                // get parent ids
                $parentIds = $this->_configurable->getParentIdsByChild($model->getId());
                $counter = count($parentIds);
                if ($counter > 0) {
                    $tempArr = [];
                    // If parent id already search, just return
                    foreach ($parentIds as $pid) {
                        if (array_key_exists($pid, $this->vmConfigurable)) {
                            $tempArr[] = $this->vmConfigurable[$pid];
                        }
                    }
                    // If parent match with the last one, just return
                    if (count($tempArr) == $counter) {
                        $params['cf_parent_product'] = implode(', ', $tempArr);
                    } else {
                        // Filter with parent ids got above
                        $parents = $this->productCollectionFactory->create()
                            ->addAttributeToFilter('entity_id', ['in' => $parentIds]);
                        $parentSkus = [];
                        $mapArr = [];
                        // Set sku array, and array map sku with id
                        foreach ($parents as $parent) {
                            /** @var \Magento\Catalog\Model\Product $parent */
                            $sku = $parent->getSku();
                            $parentSkus[] = $sku;
                            $mapArr[$sku] = $parent->getEntityId();
                        }
                        $vTigerIds = [];
                        // Get all product with skus provided
                        $skuImploded = implode(',', $parentSkus);
                        $response = $this->_iConnector->getProductBySkus($skuImploded);
                        if ($response['success']) {
                            if (count($vProducts= $response['result']) > 0) {
                                foreach ($vProducts as $vProduct) {
                                    $vTigerIds[] = $vProduct['id'];
                                    $this->vmConfigurable[$mapArr[$vProduct['productcode']]] = $vProduct['id'];
                                }
                                $params['cf_parent_product'] = implode(', ', $vTigerIds);
                            }
                        }
                    }
                }
            }
            return $params;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @param \Magento\Sales\Model\Order $model
     * @param string $_type
     * @return array
     */
    public function getOrder($model, $_type)
    {
        // get all magento field of order
        $magento_fields = $this->_field->getMagentoFields('order');
        $data = [];

        foreach ($magento_fields as $key => $item) {
            $sub = substr($key, 0, 5);
            if ($sub == 'bill_') {
                /** @var \Magento\Sales\Model\Order\Address $billing */
                $billing = $model->getBillingAddress();
                if ($billing != null) {
                    $data[$key] = $billing->getData(substr($key, 5));
                }
            } elseif ($sub == 'ship_') {
                $shipping = $model->getShippingAddress();
                if ($shipping != null){
                    $data[$key] = $shipping->getData(substr($key, 5));
                }
            } else {
                $data[$key] = $model->getData($key);
            }
        }

        if (!empty($data['bill_country_id'])) {
            $country_id = $data['bill_country_id'];
            $data['bill_country_id'] = $this->getCountryName($country_id);
        }
        if (!empty($data['ship_country_id'])) {
            $country_id = $data['ship_country_id'];
            $data['ship_country_id'] = $this->getCountryName($country_id);
        }

        // Set shipping method
        if ($model->getShippingMethod() != null){
            $data['shipping_method'] = $this->_vTigerHelper->getShippingLabel($model->getShippingMethod());
        }

        if (!isset($data['ship_street']) || (isset($data['ship_street']) && $data['ship_street'] == null)) {
            $data['ship_street'] = "Unknown";
        }

        if (!isset($data['bill_street']) || (isset($data['bill_street']) && $data['bill_street'] == null)) {
            $data['bill_street'] = "Unknown";
        }

        /* Mapping data*/
        $params = $this->getMapping($data, $_type);

        return $params;
    }

    /**
     * @param \Magento\Sales\Model\Order\Invoice $model
     * @param string $_type
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getInvoice($model, $_type)
    {
        // get all magento field of order
        $magento_fields = $this->_field->getMagentoFields('invoice');
        $data = [];

        foreach ($magento_fields as $key => $item) {
            $sub = substr($key, 0, 5);
            if ($sub == 'bill_') {
                /** @var \Magento\Sales\Model\Order\Address $billing */
                $billing = $model->getBillingAddress();
                if ($billing != null) {
                    $data[$key] = $billing->getData(substr($key, 5));
                }
            } elseif ($sub == 'ship_') {
                $shipping = $model->getShippingAddress();
                if ($shipping != null){
                    $data[$key] = $shipping->getData(substr($key, 5));
                }
            } else {
                $data[$key] = $model->getData($key);
            }
        }

        if (!empty($data['bill_country_id'])) {
            $country_id = $data['bill_country_id'];
            $data['bill_country_id'] = $this->getCountryName($country_id);
            ;
        }
        if (!empty($data['ship_country_id'])) {
            $country_id = $data['ship_country_id'];
            $data['ship_country_id'] = $this->getCountryName($country_id);
        }

        if (!isset($data['ship_street']) || (isset($data['ship_street']) && $data['ship_street'] == null)) {
            $data['ship_street'] = "Unknown";
        }

        if (!isset($data['bill_street']) || (isset($data['bill_street']) && $data['bill_street'] == null)) {
            $data['bill_street'] = "Unknown";
        }
        /* Mapping data*/
        $params = $this->getMapping($data, $_type);

        return $params;
    }
}
