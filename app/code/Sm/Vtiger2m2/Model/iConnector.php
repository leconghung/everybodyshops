<?php

namespace Sm\Vtiger2m2\Model;

use Magesales\VtigerCrm\Model\Connector;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\ZendClientFactory;

class iConnector extends Connector
{
    /**
     * Product constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Config $resourceConfig
     * @param ZendClientFactory $httpClientFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Config $resourceConfig,
        ZendClientFactory $httpClientFactory
    )
    {
        parent::__construct($scopeConfig, $resourceConfig, $httpClientFactory);
    }

    /**
     * @param $limit
     * @param $offset
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function getProduct($limit, $offset)
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $query = "SELECT * FROM Products limit {$offset},{$limit};";
        $queryRecord = urlencode($query);
        $params = "sessionName=$sessionName&operation=query&query=$queryRecord";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url . "?" . $params);
        $resp = $client->request()->getBody();
        $response = json_decode($resp, true);
        return $response;
    }

    /**
     * @param $limit
     * @param $offset
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function getSo($limit, $offset)
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $query = "SELECT * FROM SalesOrder limit {$offset},{$limit};";
        $queryRecord = urlencode($query);
        $params = "sessionName=$sessionName&operation=query&query=$queryRecord";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url . "?" . $params);
        $resp = $client->request()->getBody();
        $response = json_decode($resp, true);
        return $response;
    }

    /**
     * @param $limit
     * @param $offset
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function getInvoice($limit, $offset)
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $query = "SELECT * FROM Invoice limit {$offset},{$limit};";
        $queryRecord = urlencode($query);
        $params = "sessionName=$sessionName&operation=query&query=$queryRecord";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url . "?" . $params);
        $resp = $client->request()->getBody();
        $response = json_decode($resp, true);
        return $response;
    }

    /**
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function countProduct()
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $query = "SELECT count(*) FROM Products;";
        $queryRecord = urlencode($query);
        $params = "sessionName=$sessionName&operation=query&query=$queryRecord";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url . "?" . $params);
        $resp = $client->request()->getBody();
        $response = json_decode($resp, true);
        return $response;
    }

    /**
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function countSO()
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $query = "SELECT count(*) FROM SalesOrder;";
        $queryRecord = urlencode($query);
        $params = "sessionName=$sessionName&operation=query&query=$queryRecord";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url . "?" . $params);
        $resp = $client->request()->getBody();
        $response = json_decode($resp, true);
        return $response;
    }

    /**
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function countInvoice()
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $query = "SELECT count(*) FROM Invoice;";
        $queryRecord = urlencode($query);
        $params = "sessionName=$sessionName&operation=query&query=$queryRecord";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url . "?" . $params);
        $resp = $client->request()->getBody();
        $response = json_decode($resp, true);
        return $response;
    }

    /**
     * @param $skus
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function getProductBySkus($skus)
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        if (strpos($skus, ',') !== false) {
            $query = "SELECT * FROM Products WHERE productcode IN ({$skus});";
        } else {
            $query = "SELECT * FROM Products WHERE productcode = '{$skus}';";
        }
        $queryRecord = urlencode($query);
        $params = "sessionName=$sessionName&operation=query&query=$queryRecord";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url . "?" . $params);
        $resp = $client->request()->getBody();
        $response = json_decode($resp, true);
        return $response;
    }

    /**
     * @param $id
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function getAccount($id)
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $query = "SELECT * FROM Accounts WHERE id = '{$id}';";
        $queryRecord = urlencode($query);
        $params = "sessionName=$sessionName&operation=query&query=$queryRecord";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url . "?" . $params);
        $resp = $client->request()->getBody();
        $response = json_decode($resp, true);
        return $response;
    }
}
