<?php

namespace Sm\Vtiger2m2\Model;

use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magesales\VtigerCrm\Model\Connector;
use Magesales\VtigerCrm\Model\ResourceModel\Field as ResourceField;
use Magesales\VtigerCrm\Model\ResourceModel\Field\Collection;

class Field extends \Magesales\VtigerCrm\Model\Field
{
    /**
     * @var \Sm\Vtiger2m2\Helper\Data
     */
    protected $_helper;

    public function __construct(
        Context $context,
        Registry $registry,
        ResourceField $resource,
        Collection $resourceCollection,
        Connector $connector,
        \Sm\Vtiger2m2\Helper\Data $helper,
        array $data = []
    )
    {
        parent::__construct($context, $registry, $resource, $resourceCollection, $connector, $data);
        $this->_helper = $helper;
    }

    /**
     * Get VTiger Fields
     * @param $vtiger_table
     * @return mixed
     */
    public function getVtigerFields($vtiger_table)
    {
        if (!$this->loadByTable($vtiger_table)->getId()) {
            $alltable = $this->getAllTable();
            $m_table = $alltable[$vtiger_table];
            $this->saveFields($vtiger_table, $m_table);
        }

        $vtiger_fields = $this->getVtigercrm();

        $vTigerFields = unserialize($vtiger_fields);
        $arrayLowercase = array_map('strtolower', $vTigerFields);
        array_multisort($arrayLowercase, SORT_ASC, SORT_STRING, $vTigerFields);

        return $vTigerFields;
    }

    /**
     * Set field magento to map
     *
     * @param $table
     * @return array
     */
    public function setMagentoFields($table)
    {
        $m_fields = [];
        switch ($table) {
            case 'customer':
                $m_fields = [
                    'entity_id' => 'ID',
                    'email' => 'Email',
                    'created_at' => 'Created At',
                    'update_at' => 'Updated At',
                    'is_active' => 'is Active',
                    'created_in' => 'Created in',
                    'prefix' => 'Prefix',
                    'firstname' => 'First name',
                    'middlename' => 'Middle Name/Initial',
                    'lastname' => 'Last name',
                    'taxvat' => 'Tax/VAT Number',
                    'gender' => 'Gender',
                    'dob' => 'Date of Birth',
                    'bill_firstname' => 'Billing First Name',
                    'bill_middlename' => 'Billing Middle Name',
                    'bill_lastname' => 'Billing Last Name',
                    'bill_company' => 'Billing Company',
                    'bill_street' => 'Billing Street',
                    'bill_city' => 'Billing City',
                    'bill_region' => 'Billing State/Province',
                    'bill_country_id' => 'Billing Country',
                    'bill_postcode' => 'Billing Zip/Postal Code',
                    'bill_telephone' => 'Billing Telephone',
                    'bill_fax' => 'Billing Fax',
                    'ship_firstname' => 'Shipping First Name',
                    'ship_middlename' => 'Shipping Middle Name',
                    'ship_lastname' => 'Shipping Last Name',
                    'ship_company' => 'Shipping Company',
                    'ship_street' => 'Shipping Street',
                    'ship_city' => 'Shipping City',
                    'ship_region' => 'Shipping State/Province',
                    'ship_country_id' => 'Shipping Country',
                    'ship_postcode' => 'Shipping Zip/Postal Code',
                    'ship_telephone' => 'Shipping Telephone',
                    'ship_fax' => 'Shipping Fax',
                    'vat_id' => 'VAT number',
                ];
                break;

            case 'catalogrule':
                $m_fields = [
                    'rule_id' => 'Rule Id',
                    'description' => 'Description',
                    'from_date' => 'From Date',
                    'to_date' => 'To Date',
                    'is_active' => 'Active',
                    'simple_action' => 'Simple Action(Apply)',
                    'discount_amount' => 'Discount Amount',
                    'sub_is_enable' => 'Enable Discount to Subproducts',
                    'sub_simple_action' => 'Subproducts Simple Action(Apply)',
                    'sub_discount_amount' => 'Subproducts Discount Amount'
                ];
                break;

            case 'product':
                $m_fields = $this->_helper->getProductAttributeArray();
                break;

            case 'order':
                $m_fields = [
                    'state' => 'State',
                    'status' => 'Status',
                    'coupon_code' => 'Coupon Code',
                    'increment_id' => 'Increment ID',
                    'created_at' => 'Created At',
                    'company' => 'Company',
                    'customer_firstname' => 'Customer First Name',
                    'customer_lastname' => 'Customer Last Name',
                    'bill_firstname' => 'Billing First Name',
                    'bill_lastname' => 'Billing Last Name',
                    'bill_company' => 'Billing Company',
                    'bill_street' => 'Billing Street',
                    'bill_city' => 'Billing City',
                    'bill_region' => 'Billing State/Province',
                    'bill_postcode' => 'Billing Zip/Postal Code',
                    'bill_telephone' => 'Billing Telephone',
                    'bill_country_id' => 'Billing Country',
                    'ship_firstname' => 'Shipping First Name',
                    'ship_lastname' => 'Shipping Last Name',
                    'ship_company' => 'Shipping Company',
                    'ship_street' => 'Shipping Street',
                    'ship_city' => 'Shipping City',
                    'ship_region' => 'Shipping State/Province',
                    'ship_postcode' => 'Shipping Zip/Postal Code',
                    'ship_country_id' => 'Shipping Country',
                    'shipping_amount' => 'Shipping Amount',
                    'shipping_description' => 'Shipping Description',
//                    'order_currency_code' => 'Currency Code',
                    'total_item_count' => 'Total Item Count',
//                    'store_currency_code' => 'Store Currency Code',
                    'shipping_discount_amount' => 'Shipping Discount Amount',
                    'discount_description' => 'Discount Description',
                    'shipping_method' => 'Shipping Method',
                    'store_name' => 'Store Name',
                    'discount_amount' => 'Discount Amount',
                    'tax_amount' => 'Tax Amount',
                    'subtotal' => 'Sub Total',
                    'grand_total' => 'Grand Total'
                ];
                break;

            case 'invoice':
                $m_fields = [
                    'state' => 'State',
                    'increment_id' => 'Increment ID',
                    'order_id' => 'Order ID',
                    'created_at' => 'Created At',
                    'company' => 'Company',
                    'customer_firstname' => 'Customer First Name',
                    'customer_lastname' => 'Customer Last Name',
                    'bill_firstname' => 'Billing First Name',
                    'bill_lastname' => 'Billing Last Name',
                    'bill_company' => 'Billing Company',
                    'bill_street' => 'Billing Street',
                    'bill_city' => 'Billing City',
                    'bill_region' => 'Billing State/Province',
                    'bill_postcode' => 'Billing Zip/Postal Code',
                    'bill_telephone' => 'Billing Telephone',
                    'bill_country_id' => 'Billing Country',
                    'ship_firstname' => 'Shipping First Name',
                    'ship_lastname' => 'Shipping Last Name',
                    'ship_company' => 'Shipping Company',
                    'ship_street' => 'Shipping Street',
                    'ship_city' => 'Shipping City',
                    'ship_region' => 'Shipping State/Province',
                    'ship_postcode' => 'Shipping Zip/Postal Code',
                    'ship_country_id' => 'Shipping Country',
                    'shipping_amount' => 'Shipping Amount',
//                    'order_currency_code' => 'Currency Code',
                    'total_qty' => 'Total Qty',
//                    'store_currency_code' => 'Store Currency Code',
                    'discount_description' => 'Discount Description',
                    'shipping_method' => 'Shipping Method',
                    'shipping_incl_tax' => 'Shipping Tax',
                    'discount_amount' => 'Discount Amount',
                    'tax_amount' => 'Tax Amount',
                    'subtotal' => 'Sub Total',
                    'grand_total' => 'Grand Total'
                ];
                break;

            default:
                break;
        }

        $array_lowercase = array_map('strtolower', $m_fields);
        array_multisort($array_lowercase, SORT_ASC, SORT_STRING, $m_fields);

        return $m_fields;
    }
}
