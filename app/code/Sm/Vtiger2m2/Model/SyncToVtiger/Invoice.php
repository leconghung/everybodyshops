<?php

namespace Sm\Vtiger2m2\Model\SyncToVtiger;

use Magento\Catalog\Model\Product;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice as OrderInvoice;
use Magesales\VtigerCrm\Model\Data;
use Magesales\VtigerCrm\Model\Sync\Account as SyncAccount;
use Magesales\VtigerCrm\Model\Sync\Contact as SyncContact;
use Magesales\VtigerCrm\Model\Sync\Product as SyncProduct;
use Sm\Vtiger2m2\Model\iConnector;
use Magento\Customer\Model\CustomerFactory;
use Magento\Sales\Model\Order\InvoiceFactory;
use Magento\Sales\Model\OrderFactory;
use Sm\Vtiger2m2\Model\ResourceModel\SyncHistory\CollectionFactory as SyncHistoryCollectionFactory;
use Sm\Vtiger2m2\Model\SyncHistoryFactory;

class Invoice extends \Magesales\VtigerCrm\Model\Sync\Invoice
{
    const ID_ON_VTIGER = 'cf_magento_id';
    /**
     * @var \Sm\Vtiger2m2\Helper\Data
     */
    protected $_vTigerHelper;

    /**
     * @var \Sm\Vtiger2m2\Model\Data
     */
    protected $_vTigerModelData;

    /**
     * @var iConnector
     */
    protected $_connector;

    /**
     * @var \Zend\Log\Logger
     */
    protected $_logger;

    /**
     * @var \Magento\Catalog\Model\Product|CatalogProduct
     */
    protected $_productFactory;

    /**
     * @var CustomerFactory
     */
    protected $_customerFactory;

    /**
     * @var \Magento\Sales\Model\Order\InvoiceFactory
     */
    protected $invoice;

    /**
     * @var \Magento\Sales\Model\OrderFactory
     */
    protected $order;

    /**
     * @var SyncHistoryCollectionFactory
     */
    protected $syncHistoryCollection;

    /**
     * @var SyncHistoryFactory
     */
    protected $syncHistoryFactory;

    /**
     * Invoice constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Config $resourceConfig
     * @param ZendClientFactory $httpClientFactory
     * @param PhpCookieManager $cookieManager
     * @param PublicCookieMetadata $cookieMetadata
     * @param Data $data
     * @param Product $product
     * @param Order $order
     * @param OrderInvoice $invoice
     * @param SyncContact $contact
     * @param SyncAccount $account
     * @param SyncProduct $syncProduct
     * @param \Sm\Vtiger2m2\Helper\Data $vTigerHelper
     * @param \Sm\Vtiger2m2\Model\Data $vTgierModelData
     * @param iConnector $iConnector
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param CustomerFactory $customerFactory
     * @param InvoiceFactory $invoiceFactory
     * @param OrderFactory $orderFactory
     * @param SyncHistoryCollectionFactory $syncHistoryCollectionFactory
     * @param SyncHistoryFactory $syncHistoryFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Config $resourceConfig,
        ZendClientFactory $httpClientFactory,
        PhpCookieManager $cookieManager,
        PublicCookieMetadata $cookieMetadata,
        Data $data,
        Product $product,
        Order $order,
        OrderInvoice $invoice,
        SyncContact $contact,
        SyncAccount $account,
        SyncProduct $syncProduct,
        \Sm\Vtiger2m2\Helper\Data $vTigerHelper,
        \Sm\Vtiger2m2\Model\Data $vTgierModelData,
        iConnector $iConnector,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        CustomerFactory $customerFactory,
        InvoiceFactory $invoiceFactory,
        OrderFactory $orderFactory,
        SyncHistoryCollectionFactory $syncHistoryCollectionFactory,
        SyncHistoryFactory $syncHistoryFactory
    )
    {
        parent::__construct($scopeConfig, $resourceConfig, $httpClientFactory, $cookieManager, $cookieMetadata, $data, $product, $order, $invoice, $contact, $account, $syncProduct);
        $this->_vTigerHelper = $vTigerHelper;
        $this->_vTigerModelData = $vTgierModelData;
        $this->_connector = $iConnector;
        $this->_productFactory = $productFactory;
        $this->_customerFactory = $customerFactory;
        $this->invoice = $invoiceFactory;
        $this->order = $orderFactory;
        $this->syncHistoryCollection = $syncHistoryCollectionFactory;
        $this->syncHistoryFactory = $syncHistoryFactory;
        // add log file
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/invoice_to_vtiger.log');
        $this->_logger = new \Zend\Log\Logger();
        $this->_logger->addWriter($writer);
    }


    /**
     * @param $id
     * @return mixed|string|void
     */
    public function sync($id)
    {
        try {
            /** @var \Magento\Sales\Model\Order\Invoice $modelInvoice */
            $modelInvoice = $this->invoice->create()->load($id);
            $iid = $modelInvoice->getId();
            $invoiceIncrementId = $modelInvoice->getIncrementId();
            $orderId = $modelInvoice->getOrderId();
            $model = $this->order->create()->load($orderId);
            $this->_logger->info('Invoice Increment Id: ' . $invoiceIncrementId);
            if (count($model->getData()) == 0) {
                $this->_logger->info(">>> Order Id doesn't exist");
                return;
            }
            $email = $model->getCustomerEmail();
            $subject = $model->getIncrementId();
            $productId = "";
            $accountId = null;
            $lineItem = [];

            // Get account id on vtiger
            $checkCus = $this->syncHistoryCollection->create()
                ->addFieldToFilter('type', 'Accounts')
                ->addFieldToFilter('magento_id', $model->getCustomerId())
                ->addFieldToFilter('status', 1);
            if ($checkCus->getSize() > 0) {
                $accountId = $checkCus->getFirstItem()->getData('vtiger_id');
            } else {
                $response = $this->query("Accounts", "email1", $email);
                if (!$response['success'] || (isset($response['result']) && count($response['result']) == 0)) {
                    $account = ['accountname' => $email, 'email1' => $email];
                    $this->create("Accounts", $account);
                    $response = $this->query("Accounts", "email1", $email);
                }
                if (!$response['success'] || (isset($response['result']) && count($response['result']) == 0)) {
                    $messageCus = '';
                    $message = isset($response['error']['message']) ? $response['error']['message'] : $messageCus;
                    $this->_logger->info(">>> Error when getting account", ['email' => $email, 'message' => $message]);
                    return;
                } else {
                    if (isset($response['result'][0]['id'])) {
                        $accountId = $response['result'][0]['id'];
                    } else {
                        $this->_logger->info(">>> Response is null when getting account info");
                        return;
                    }
                }
            }
            // Line items
            $itemCounter = 0;
            $tmpId = [];
            $tmpSku = [];
            foreach ($model->getAllItems() as $item) {
                $prd = $item->getProduct();
                $sku = $item->getSku() != '' ? $prd->getSku() : '';
                $tmpSku[] = $sku;
                $tmpId[] = $prd->getId();
                $price = $item->getPrice();
                if ($price == 0) {
                    $price = $this->_productFactory->create()->load($prd->getId())->getPrice();
                }
                $qty = $item->getQtyOrdered();
                if ($price > 0 && $sku != '') {
                    $checkPrd = $this->syncHistoryCollection->create()
                        ->addFieldToFilter('type', 'Products')
                        ->addFieldToFilter('magento_id', $prd->getId())
                        ->addFieldToFilter('status', 1);
                    if ($checkPrd->getSize() > 0) {
                        $productId = $checkPrd->getFirstItem()->getData('vtiger_id');
                        $lineItem[$itemCounter] = ['productid' => $productId, 'sku' => $sku, 'quantity' => $qty, 'listprice' => $price];
                        $itemCounter++;
                    } else {
                        // get productId
                        $response = $this->query('Products', 'productcode', $sku);
                        if (isset($response['success']) && isset($response['result']) && count($response['result']) > 0) {
                            $productId = $response['result'][0]['id'];
                            $lineItem[$itemCounter] = ['productid' => $productId, 'sku' => $sku, 'quantity' => $qty, 'listprice' => $price];
                            $itemCounter++;
                        }
                    }
                } else {
                    $this->_logger->info(">>> Price = 0 or Sku doesn't exists", ['Product Sku' => $item->getSku()]);
                    return;
                }
            }

            // Check line item
            if (count($lineItem) == 0) {
                $this->_logger->info(">>> Something went wrong with products of this order (deleted or didn't synced to vtiger)", ['pid' => $tmpId, 'sku' => $tmpSku]);
                return;
            }

            // Map data
            $mappedData = [
                'subject' => $model->getIncrementId(),
                'productid' => $productId,
                'account_id' => $accountId,
                'LineItems' => $lineItem,
                'hdnTaxType' => 'group',
                'conversion_rate' => 1,
                'invoicestatus' => 'Paid',
                'currency_id' => '21x1',
                'salesorder_id' => '',
                self::ID_ON_VTIGER => $iid
            ];
            $mappedData += $this->_vTigerModelData->getInvoice($modelInvoice, 'Invoice');

            // Sync history params
            $logData = [
                'type' => 'Invoice',
                'magento_id' => $iid,
            ];

            // Get Invoice data
            $checkInvoice = $this->query('Invoice', 'subject', $subject);

            // Create or Update Invoice
            if (isset($checkInvoice['result'][0])) {
                // Update invoice
                $element = $checkInvoice['result'][0];
                foreach ($mappedData as $key => $value) {
                    $element[$key] = $value;
                }
                $response = $this->update($element);
                if (isset($response['success']) && isset($response['result']) && isset($response['result']['id'])) {
                    $logData['vtiger_id'] = $response['result']['id'];
                    $logData['status'] = 1;
                    $logData['note'] = 'Success';
                    $this->_logger->info(">>> Success");
                } else {
                    $msg = isset($response['error']['message']) ? $response['error']['message']
                        : "Can't update invoice on vtiger";
                    $logData['status'] = 0;
                    $logData['note'] = $msg;
                    $this->_logger->info(">>> Can't update invoice on Vtiger", ['response' => $response]);
                }
            } else {
                // Create new invoice on Vtiger
                $response = $this->create(self::TYPE, $mappedData);
                if (isset($response['success']) && isset($response['result']) && isset($response['result']['id'])) {
                    $logData['vtiger_id'] = $response['result']['id'];
                    $logData['status'] = 1;
                    $logData['note'] = 'Success';
                    $this->_logger->info(">>> Success");
                } else {
                    $this->_logger->info(">>> Can't create Invoice on Vtiger", ['response' => $response]);
                    return;
                }
            }

            // Update sync_history table
            $syncHistoryCollection = $this->syncHistoryCollection->create()
                ->addFieldToFilter('type', 'Invoice')
                ->addFieldToFilter('magento_id', $iid);
            if ($syncHistoryCollection->getSize() > 0) {
                /** @var \Sm\Vtiger2m2\Model\SyncHistory $syncItem */
                $syncItem = $syncHistoryCollection->getFirstItem();
                foreach ($logData as $key => $value) {
                    $syncItem->setData($key, $value);
                }
                $syncItem->save();
            } else {
                $this->syncHistoryFactory->create()->setData($logData)->save();
            }

            return;
        } catch (\Exception $e) {
            $this->_logger->info(">>> Exception", ['message' => $e->getMessage()]);
            return;
        }
    }
}
