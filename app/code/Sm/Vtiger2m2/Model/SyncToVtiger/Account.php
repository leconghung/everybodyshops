<?php

namespace Sm\Vtiger2m2\Model\SyncToVtiger;

use Magesales\VtigerCrm\Model\Data;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Customer\Model\Customer;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;
use Magento\Customer\Model\CustomerFactory;
use Sm\Vtiger2m2\Model\ResourceModel\SyncHistory\CollectionFactory as SyncHistoryCollectionFactory;
use Sm\Vtiger2m2\Model\SyncHistoryFactory;

/**
 * Class Account
 * @package Sm\Vtiger2m2\Model\SyncToVtiger
 */
class Account extends \Magesales\VtigerCrm\Model\Sync\Account
{
    const ID_ON_VTIGER = 'cf_magento_id';

    /**
     * @var CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Zend\Log\Logger
     */
    protected $_logger;

    /**
     * @var SyncHistoryCollectionFactory
     */
    protected $syncHistoryCollection;

    /**
     * @var SyncHistoryFactory
     */
    protected $syncHistoryFactory;

    /**
     * Account constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Config $resourceConfig
     * @param ZendClientFactory $httpClientFactory
     * @param PhpCookieManager $cookieManager
     * @param PublicCookieMetadata $cookieMetadata
     * @param Data $data
     * @param Customer $customer
     * @param CustomerFactory $customerFactory
     * @param SyncHistoryCollectionFactory $syncHistoryCollectionFactory
     * @param SyncHistoryFactory $syncHistoryFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Config $resourceConfig,
        ZendClientFactory $httpClientFactory,
        PhpCookieManager $cookieManager,
        PublicCookieMetadata $cookieMetadata,
        Data $data,
        Customer $customer,
        CustomerFactory $customerFactory,
        SyncHistoryCollectionFactory $syncHistoryCollectionFactory,
        SyncHistoryFactory $syncHistoryFactory
    )
    {
        $this->customerFactory = $customerFactory;
        $this->syncHistoryCollection = $syncHistoryCollectionFactory;
        $this->syncHistoryFactory = $syncHistoryFactory;
        parent::__construct($scopeConfig, $resourceConfig, $httpClientFactory, $cookieManager, $cookieMetadata, $data, $customer);
        // add log file
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/account_to_vtiger.log');
        $this->_logger = new \Zend\Log\Logger();
        $this->_logger->addWriter($writer);
    }

    /**
     * @param int $id
     * @return array|void
     */
    public function sync($id)
    {
        try {
            $model = $this->customerFactory->create()->load($id);
            $accountName = $model->getName();
            $email = $model->getEmail();
            $cid = $model->getId();
            $this->_logger->info($accountName, ['id' => $cid, 'email' => $email]);
            $params = $this->_data->getCustomer($model, self::TYPE);
            $params += [
                'accountname' => $accountName,
                'email1' => $email,
                self::ID_ON_VTIGER => $cid
            ];

            // Sync history params
            $logData = [
                'type' => 'Accounts',
                'magento_id' => $cid,
            ];

            // Get account data
            $accountData = $this->query(self::TYPE, self::FIELD, $email);

            // Create or update
            if (!isset($accountData['result'][0])) {
                // Create new account on Vtiger
                $response = $this->create(self::TYPE, $params);
                if (isset($response['success']) && isset($response['result']) && isset($response['result']['id'])) {
                    $logData['vtiger_id'] = $response['result']['id'];
                    $logData['status'] = 1;
                    $logData['note'] = 'Success';
                    $this->_logger->info(">>> Success");
                } else {
                    $this->_logger->info(">>> Can't create Account on Vtiger", ['response' => $response]);
                    return;
                }
            } else {
                // Update account
                $element = $accountData['result'][0];
                foreach ($params as $key => $value) {
                    $element[$key] = $value;
                }
                $response = $this->update($element);
                if (isset($response['success']) && $response['result']['id']) {
                    $logData['vtiger_id'] = $response['result']['id'];
                    $logData['status'] = 1;
                    $logData['note'] = 'Success';
                    $this->_logger->info(">>> Success");
                } else {
                    $msg = isset($response['error']['message']) ? $response['error']['message']
                        : "Can't update Account on vtiger";
                    $logData['status'] = 0;
                    $logData['note'] = $msg;
                    $this->_logger->info(">>> Can't update Account on Vtiger", ['response' => $response]);
                }
            }

            // Update sync_history table
            $syncHistoryCollection = $this->syncHistoryCollection->create()
                ->addFieldToFilter('type', 'Accounts')
                ->addFieldToFilter('magento_id', $cid);
            if ($syncHistoryCollection->getSize() > 0) {
                /** @var \Sm\Vtiger2m2\Model\SyncHistory $syncItem */
                $syncItem = $syncHistoryCollection->getFirstItem();
                foreach ($logData as $key => $value) {
                    $syncItem->setData($key, $value);
                }
                $syncItem->save();
            } else {
                $this->syncHistoryFactory->create()->setData($logData)->save();
            }
            return;
        } catch (\Exception $e) {
            $this->_logger->info(">>> Exception", ['message' => $e->getMessage()]);
            return;
        }
    }
}
