<?php

namespace Sm\Vtiger2m2\Model\SyncToVtiger;

use Magento\Catalog\Model\Product as CatalogProduct;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;
use Magesales\VtigerCrm\Model\Data;
use Magento\Store\Model\StoreManagerInterface;
use Sm\Vtiger2m2\Model\Data as VtigerData;
use Sm\Vtiger2m2\Model\ResourceModel\SyncHistory\CollectionFactory as SyncHistoryCollectionFactory;
use Sm\Vtiger2m2\Model\SyncHistoryFactory;
use Magento\Catalog\Model\CategoryFactory;

class Product extends \Magesales\VtigerCrm\Model\Sync\Product
{
    const ID_ON_VTIGER = 'cf_magento_id';

    /**
     * @var \Magento\Catalog\Model\Product|CatalogProduct
     */
    protected $_productFactory;

    /**
     * @var \Sm\Vtiger2m2\Model\iConnector
     */
    protected $_connector;

    /**
     * @var \Zend\Log\Logger
     */
    protected $_logger;

    /**
     * Store Manager
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var VtigerData
     */
    protected $data;

    /**
     * @var SyncHistoryCollectionFactory
     */
    protected $syncHistoryCollection;

    /**
     * @var SyncHistoryFactory
     */
    protected $syncHistoryFactory;

    /**
     * @var CategoryFactory
     */
    protected $categoryFactory;

    /**
     * Product constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Config $resourceConfig
     * @param ZendClientFactory $httpClientFactory
     * @param PhpCookieManager $cookieManager
     * @param PublicCookieMetadata $cookieMetadata
     * @param Data $data
     * @param CatalogProduct $product
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Sm\Vtiger2m2\Model\iConnector $iConnector
     * @param StoreManagerInterface $storeManager
     * @param VtigerData $vtigerData
     * @param SyncHistoryCollectionFactory $syncHistoryCollectionFactory
     * @param SyncHistoryFactory $syncHistoryFactory
     * @param CategoryFactory $categoryFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Config $resourceConfig,
        ZendClientFactory $httpClientFactory,
        PhpCookieManager $cookieManager,
        PublicCookieMetadata $cookieMetadata,
        Data $data,
        CatalogProduct $product,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Sm\Vtiger2m2\Model\iConnector $iConnector,
        StoreManagerInterface $storeManager,
        VtigerData $vtigerData,
        SyncHistoryCollectionFactory $syncHistoryCollectionFactory,
        SyncHistoryFactory $syncHistoryFactory,
        CategoryFactory $categoryFactory
    )
    {
        $this->_productFactory = $productFactory;
        $this->_connector = $iConnector;
        $this->_storeManager = $storeManager;
        parent::__construct($scopeConfig, $resourceConfig, $httpClientFactory, $cookieManager, $cookieMetadata, $data, $product);
        // add log file
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/product_to_vtiger.log');
        $this->_logger = new \Zend\Log\Logger();
        $this->_logger->addWriter($writer);
        $this->data = $vtigerData;
        $this->syncHistoryCollection = $syncHistoryCollectionFactory;
        $this->syncHistoryFactory = $syncHistoryFactory;
        $this->categoryFactory = $categoryFactory;
    }

    /**
     * @param int $id
     * @param bool $syncAll
     * @return string|void
     */
    public function sync($id, $syncAll = false)
    {
        try {
            $model = $this->_productFactory->create()->load($id);
            $pid = $model->getId();
            $name = $model->getName();
            $code = $model->getSku();
            $status = $model->getStatus();
            $baseUrl = $this->_storeManager->getStore()->getBaseUrl();
            $yoastUrl = $baseUrl . 'admin/catalog/product/edit/id/' . $model->getId() . '/tab/yoast-seo-wrapper';
            $this->_logger->info($name, ['id' => $pid, 'sku' => $code]);
            $params = $this->data->getProduct($model, self::TYPE);
            $params += [
                'productname' => $name,
                'productcode' => $code,
                'discontinued' => $status == 1 ? true : false,
                'cf_magento_url' => $yoastUrl
            ];
            $params['imagename'] = $this->getImageArray($model);
            $params[self::ID_ON_VTIGER] = $pid;
            // Product Category
            if (isset($params['productcategory'])) {
                $arrName = [];
                foreach ($params['productcategory'] as $catId) {
                    $category = $this->categoryFactory->create()->load($catId);
                    $arrName[] = $category->getName();
                }
                $params['productcategory'] = implode(' |##| ', $arrName);
                $params['productcategory'] = str_replace('&amp;', 'and', $params['productcategory']);
                $params['productcategory'] = str_replace('&', 'and', $params['productcategory']);
            } else {
                $params['productcategory'] = "";
            }

            // Stock Status
            if (isset($params['cf_stock_status'])) {
                if ($params['cf_stock_status'] == 1) {
                    $params['cf_stock_status'] = 'In Stock';
                } else {
                    $params['cf_stock_status'] = 'Out of Stock';
                }
            }
            if (isset($params['imagename']) && is_array($params['imagename'])) {
                $params['imagename'] = reset($params['imagename']);
            }

            // Sync history params
            $logData = [
                'type' => 'Products',
                'magento_id' => $pid,
            ];

            // Get product data
            $productData = $this->query(self::TYPE, self::FIELD, $code);

            // Create or update
            if (isset($productData['result'][0])) {
                // Update
                $element = $productData['result'][0];
                foreach ($params as $key => $value) {
                    $element[$key] = $value;
                }
                $response = $this->update($element);
                if (isset($response['success']) && isset($response['result']) && isset($response['result']['id'])) {
                    $logData['vtiger_id'] = $response['result']['id'];
                    $logData['status'] = 1;
                    $logData['note'] = 'Success';
                    $this->_logger->info(">>> Success");
                } else {
                    $msg = isset($response['error']['message']) ? $response['error']['message']
                        : "Can't update product on vtiger";
                    $logData['status'] = 0;
                    $logData['note'] = $msg;
                    $this->_logger->info(">>> Can't update Product on Vtiger", ['response' => $response]);
                }
            } else {
                // Create new product on Vtiger
                $response = $this->create(self::TYPE, $params);
                if (isset($response['success']) && isset($response['result']) && isset($response['result']['id'])) {
                    $logData['vtiger_id'] = $response['result']['id'];
                    $logData['status'] = 1;
                    $logData['note'] = 'Success';
                    $this->_logger->info(">>> Success");
                } else {
                    $this->_logger->info(">>> Can't create Product on Vtiger", ['response' => $response]);
                    return;
                }
            }

            // Update sync_history table
            $syncHistoryCollection = $this->syncHistoryCollection->create()
                ->addFieldToFilter('type', 'Products')
                ->addFieldToFilter('magento_id', $pid);
            if ($syncHistoryCollection->getSize() > 0) {
                /** @var \Sm\Vtiger2m2\Model\SyncHistory $syncItem */
                $syncItem = $syncHistoryCollection->getFirstItem();
                foreach ($logData as $key => $value) {
                    $syncItem->setData($key, $value);
                }
                $syncItem->save();
            } else {
                $this->syncHistoryFactory->create()->setData($logData)->save();
            }

            // Set parent product for associated product on vTiger
            if (!$syncAll && $model->getTypeId() == 'configurable') {
                $usedProducts = $model->getTypeInstance()->getUsedProducts($model);
                // Return if don't have child products
                if (count($usedProducts) == 0) {
                    return;
                }
                $parentSku = $model->getSku();
                $vTigerParent = $this->_connector->getProductBySkus($parentSku);
                if ($vTigerParent['success'] && count($vTigerParent['result']) > 0) {
                    $vTigerParentId = $this->_connector->getProductBySkus($parentSku)['result'][0]['id'];
                    foreach ($usedProducts as $product) {
                        /** @var \Magento\Catalog\Model\Product $product */
                        $vTigerProduct = $this->_connector->getProductBySkus($product->getSku());
                        if ($vTigerProduct['success'] && isset($vTigerProduct['result']) && count($vTigerProduct['result']) > 0) {
                            $ele = $vTigerProduct['result'][0];
                            $ele['cf_parent_product'] = $vTigerParentId;
                            $this->update($ele);
                        }
                    }
                }
            }
            return;
        } catch (\Exception $e) {
            $this->_logger->info(">>> Exception", ['message' => $e->getMessage()]);
            return;
        }
    }

    /**
     * @param \Magento\Catalog\Model\Product $product
     * @return array|string
     */
    public function getImageArray($product)
    {
        $result = [];
        if ($product->getData('image') == null) {
            return "";
        }
        $items = $product->getMediaGalleryImages()->getItems();
        if (!empty($items)) {
            foreach ($items as $item) {
                $result[$item['id']] = $item['path'];
            }
        }
        return $result;
    }
}
