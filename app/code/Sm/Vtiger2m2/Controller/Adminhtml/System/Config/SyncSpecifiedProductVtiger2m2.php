<?php

namespace Sm\Vtiger2m2\Controller\Adminhtml\System\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class SyncSpecifiedProductVtiger2m2 extends Action
{
    /**
     * Json factory
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * IConnector
     * @var \Sm\Vtiger2m2\Model\iConnector
     */
    protected $_connector;

    /**
     * Product syncer
     * @var \Sm\Vtiger2m2\Model\SyncToMagento\Product
     */
    protected $_productSyncer;

    /**
     * Scope interface
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * Config writer
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $_configWriter;

    /**
     * Cache type list
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * Cache frontend pool
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * SyncProductVtiger2m2 constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param \Sm\Vtiger2m2\Model\iConnector $iConnector
     * @param \Sm\Vtiger2m2\Model\SyncToMagento\Product $productSyncer
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        \Sm\Vtiger2m2\Model\iConnector $iConnector,
        \Sm\Vtiger2m2\Model\SyncToMagento\Product $productSyncer,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
    )
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_connector = $iConnector;
        $this->_productSyncer = $productSyncer;
        $this->_scopeConfig = $scopeConfig;
        $this->_configWriter = $configWriter;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
    }

    /**
     * Get product data from vtiger
     * create/update product with data above
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $specifiedProducts = $this->getRequest()->getParam('specified_products');
        if ($specifiedProducts == null) {
            return $result->setData(['success' => false, 'message' => 'Please input product\'s sku']);
        } else {
            $syncSpecified = true;
            $specifiedProductsArr = explode(',', $specifiedProducts);
        }
        try {
            $startTime = microtime(true);
            $configurableList = [];
            $skuUsed = [];

            // get product data from vtiger
            $productData = $this->_connector->getProductBySkus($specifiedProducts);
            // create/update product on magento side
            $this->_productSyncer->createProduct($productData, $skuUsed, $configurableList, $syncSpecified, $specifiedProductsArr, 0, '');

            // create configurable product
            if (count($configurableList) > 0) {
                $this->_productSyncer->createConfigurableProduct($configurableList);
            }

            // clear config cache
            $types = array('config', 'layout', 'block_html', 'collections', 'reflection', 'db_ddl', 'eav', 'config_integration', 'config_integration_api', 'full_page', 'translate', 'config_webservice');
            foreach ($types as $type) {
                $this->_cacheTypeList->cleanType($type);
            }
            foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                $cacheFrontend->getBackend()->clean();
            }

            // create message
            $message = "Synchronized. Total time: " . number_format(microtime(true) - $startTime, 2, '.', ',') . 's 
            . Check log at "var/log/sync_vtiger2m2_product.log" for more detail.';

            return $result->setData(['success' => true, 'message' => $message]);
        } catch (\Exception $e) {
            return $result->setData(['success' => false, 'message' => $e->getMessage()]);
        }
    }
}

?>