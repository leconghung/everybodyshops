<?php

namespace Sm\Vtiger2m2\Controller\Adminhtml\System\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class ClearVtigerFields extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Sm\Vtiger2m2\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Sm\Vtiger2m2\Model\iConnector
     */
    protected $_connector;

    /**
     * @var \Magesales\VtigerCrm\Model\Field
     */
    protected $_vtigerFields;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        \Sm\Vtiger2m2\Helper\Data $helper,
        \Sm\Vtiger2m2\Model\iConnector $iConnector,
        \Magesales\VtigerCrm\Model\Field $vtigerField
    )
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_helper = $helper;
        $this->_connector = $iConnector;
        $this->_vtigerFields = $vtigerField;
    }

    /**
     * Clear saved vtiger attribute fields which were saved on database
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        try {
            $vitgerFields = $this->_vtigerFields->getCollection();
            foreach ($vitgerFields as $field) {
                $field->delete();
            }
            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => true, 'message' => 'Cleared']);
        } catch (\Exception $e) {
            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => false, 'message' => $e->getMessage()]);
        }

    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Sm_Vtiger2m2::config');
    }
}

?>