<?php

namespace Sm\Vtiger2m2\Controller\Adminhtml\System\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Customer\Model\ResourceModel\Customer\CollectionFactory as CustomerCollectionFactory;
use Sm\Vtiger2m2\Model\SyncToVtiger\Account;

class SyncAccountToVtiger extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var CustomerCollectionFactory
     */
    protected $customerCollectionFactory;

    /**
     * @var Account
     */
    protected $accountSyncer;

    /**
     * @var \Zend\Log\Logger
     */
    protected $logger;

    /**
     * SyncAccountToVtiger constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param CustomerCollectionFactory $customerCollectionFactory
     * @param Account $accountSyncer
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        CustomerCollectionFactory $customerCollectionFactory,
        Account $accountSyncer
    )
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->customerCollectionFactory = $customerCollectionFactory;
        $this->accountSyncer = $accountSyncer;
        // add log file
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/account_to_vtiger.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    public function execute()
    {
        try {
            $startTime = microtime(true);
            $allCustomer = $this->customerCollectionFactory->create();
            $totalRecords = $allCustomer->getSize();
            $counter = 0;
            foreach ($allCustomer as $customer) {
                $counter++;
                $this->logger->info("Index {$counter}/{$totalRecords}");
                $start = microtime(true);
                /** @var \Magento\Customer\Model\Customer $customer */
                $this->accountSyncer->sync($customer->getId());
                $time = number_format(microtime(true) - $start, 5);
                $this->logger->info("Time: {$time}s \n");
            }
            // create message
            $message = "Synchronized - " . number_format(microtime(true) - $startTime, 2, '.', ',') . 's';
            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => true, 'message' => $message]);
        } catch (\Exception $e) {
            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => false, 'message' => $e->getMessage()]);
        }
    }
}
