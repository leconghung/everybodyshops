<?php

namespace Sm\Vtiger2m2\Controller\Adminhtml\System\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\Framework\Controller\Result\JsonFactory;
use Sm\Vtiger2m2\Model\SyncToVtiger\Product;
use Magento\Framework\App\Config\ScopeConfigInterface;

class SyncProductToVtiger extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var CollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var Product
     */
    protected $_productSyncer;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Zend\Log\Logger
     */
    protected $_logger;

    CONST SYNC_TO_VTIGER_USE_INDEX = 'vtigercrm/manually_sync_to_vtiger/start_with_index';
    CONST SYNC_TO_VTIGER_INDEX = 'vtigercrm/manually_sync_to_vtiger/index';

    /**
     * SyncProductToVtiger constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param CollectionFactory $collectionFactory
     * @param Product $productSyncer
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        CollectionFactory $collectionFactory,
        Product $productSyncer,
        ScopeConfigInterface $scopeConfig
    )
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_productCollectionFactory = $collectionFactory;
        $this->_productSyncer = $productSyncer;
        $this->scopeConfig = $scopeConfig;
        // add log file
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/product_to_vtiger.log');
        $this->_logger = new \Zend\Log\Logger();
        $this->_logger->addWriter($writer);
    }

    /**
     * return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        try {
            // Check config
            $useIndex = $this->scopeConfig->getValue(self::SYNC_TO_VTIGER_USE_INDEX);
            $index = $this->scopeConfig->getValue(self::SYNC_TO_VTIGER_INDEX);
            $i = 0;

            // Load all products then save each one
            $startTime = microtime(true);

            // sync simple product first
            /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $simpleProducts */
            $pCollection = $this->_productCollectionFactory->create()
                ->addAttributeToFilter('type_id', ['in' => ['simple', 'virtual', 'configurable']]);
            $totalRecords = $pCollection->getSize();
            $simpleProducts = $this->_productCollectionFactory->create()
                ->addAttributeToFilter('type_id', ['in' => ['virtual', 'simple']])->addAttributeToSelect("*");
            $this->_logger->info("==== Simple products ====");
            foreach ($simpleProducts as $simpleProduct) {
                $i++;
                if ($useIndex == 1 && $index != NULL && $i < $index) {
                    continue;
                }
                $start = microtime(true);
                $this->_logger->info("Index: {$i}/{$totalRecords}");
                $this->_productSyncer->sync($simpleProduct->getId(), true);
                $time = number_format(microtime(true) - $start, 5);
                $this->_logger->info("Time: {$time}s \n");
            }
            // sync configurable product after
            /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $configProducts */
            $configProducts = $this->_productCollectionFactory->create()
                ->addAttributeToFilter('type_id', 'configurable')->addAttributeToSelect("*");;
            $this->_logger->info("==== Configurable products ====");
            foreach ($configProducts as $configProduct) {
                if ($configProduct->getTypeId() == 'configurable') {
                    $i++;
                    if ($useIndex == 1 && $index != NULL && $i < $index) {
                        continue;
                    }
                    $start = microtime(true);
                    $this->_logger->info("Index: {$i}/{$totalRecords}");
                    $this->_productSyncer->sync($configProduct->getId(), true);
                    $time = number_format(microtime(true) - $start, 5);
                    $this->_logger->info("Time: {$time}s \n");
                }
            }
            $message = "Synchronized - " . number_format(microtime(true) - $startTime, 5) . 's';
            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => true, 'message' => $message]);
        } catch (\Exception $e) {
            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => false, 'message' => $e->getMessage()]);
        }
    }
}
