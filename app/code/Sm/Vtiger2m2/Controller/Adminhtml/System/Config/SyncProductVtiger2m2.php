<?php

namespace Sm\Vtiger2m2\Controller\Adminhtml\System\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class SyncProductVtiger2m2 extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Sm\Vtiger2m2\Model\iConnector
     */
    protected $_connector;

    /**
     * @var \Sm\Vtiger2m2\Model\SyncToMagento\Product
     */
    protected $_productSyncer;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $_configWriter;

    /**
     * Cache type list
     * @var \Magento\Framework\App\Cache\TypeListInterface
     */
    protected $_cacheTypeList;

    /**
     * Cache frontend pool
     * @var \Magento\Framework\App\Cache\Frontend\Pool
     */
    protected $_cacheFrontendPool;

    /**
     * SyncProductVtiger2m2 constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param \Sm\Vtiger2m2\Model\iConnector $iConnector
     * @param \Sm\Vtiger2m2\Model\SyncToMagento\Product $productSyncer
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
     * @param \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList
     * @param \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        \Sm\Vtiger2m2\Model\iConnector $iConnector,
        \Sm\Vtiger2m2\Model\SyncToMagento\Product $productSyncer,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool
    )
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_connector = $iConnector;
        $this->_productSyncer = $productSyncer;
        $this->_scopeConfig = $scopeConfig;
        $this->_configWriter = $configWriter;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
    }

    /**
     * Get product data from vtiger
     * create/update product with data above
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        try {
            // get configs
            $allowDelete = $this->_scopeConfig->getValue('vtigercrm/sync_additional_settings/allow_delete_product');
            $configDate = $this->_scopeConfig->getValue('vtigercrm/sync_additional_settings/product_allowed_date');
            $baseOnModified = $this->_scopeConfig->getValue('vtigercrm/sync_additional_settings/sync_base_on_modified');
            $lastSyncTime = $this->_scopeConfig->getValue('vtigercrm/sync_additional_settings/last_sync_time');
            if ($configDate == null) {
                $configDate = date('Y-m-d');
                $this->_configWriter->save('vtigercrm/sync_additional_settings/product_allowed_date', $configDate);
            }

            $limited = 100; // We only get max 100 records when query into vtiger system
            $startTime = microtime(true);
            $countCreate = $countUpdate = $countIgnore = $countError = $countConfig = 0;
            $configurableList = [];
            $skuUsed = [];

            // count products
            $response = $this->_connector->countProduct();

            if (!$response['success']) {
                $result = $this->resultJsonFactory->create();
                return $result->setData(['success' => false, 'message' => 'Please check the connection/query again!']);
            }

            $countProduct = $response['result'][0]['count'];
            $loops = ceil($countProduct / $limited);
            for ($i = 0; $i < $loops; $i++) {
                // get product data from vtiger
                $productData = $this->_connector->getProduct($limited, $limited * $i);
                // create/update product on magento side
                $resultData = $this->_productSyncer->createProduct($productData, $skuUsed, $configurableList, false, null, $baseOnModified, $lastSyncTime);
                $countUpdate += $resultData['update'];
                $countCreate += $resultData['create'];
                $countIgnore += $resultData['ignore'];
                $countError += $resultData['error'];
            }

            // create configurable product
            if (count($configurableList) > 0) {
                $configResult = $this->_productSyncer->createConfigurableProduct($configurableList);
                $countConfig = $configResult['config'];
            }

            // delete redundant product
            $countDelete = $this->_productSyncer->deleteProduct($skuUsed, $allowDelete, $configDate);

            // save last sync time
            $this->_configWriter->save('vtigercrm/sync_additional_settings/last_sync_time', date('Y-m-d H:i:s'));

            // clear config cache
            $types = array('config','layout','block_html','collections','reflection','db_ddl','eav','config_integration','config_integration_api','full_page','translate','config_webservice');
            foreach ($types as $type) {
                $this->_cacheTypeList->cleanType($type);
            }
            foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                $cacheFrontend->getBackend()->clean();
            }

            // create message
            $syncInfo = 'created: ' . $countCreate . ' products, updated: ' . $countUpdate . ' products, ignored: '
                . $countIgnore . ' products' . ', deleted: ' . $countDelete . ', error: ' . $countError .
                ' Config product created/updated: ' . $countConfig .
                '. Total time: ' . number_format(microtime(true) - $startTime, 2, '.', ',') . 's';
            $message = "Synchronized. Total time: " . number_format(microtime(true) - $startTime, 2, '.', ',') . 's 
            . Check log at "var/log/sync_vtiger2m2_product.log" for more detail.';

            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => true, 'message' => $message]);
        } catch (\Exception $e) {
            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => false, 'message' => $e->getMessage()]);
        }
    }
}

?>