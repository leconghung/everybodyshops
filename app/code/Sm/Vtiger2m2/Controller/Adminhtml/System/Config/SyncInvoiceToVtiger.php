<?php

namespace Sm\Vtiger2m2\Controller\Adminhtml\System\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Sales\Model\Order\InvoiceFactory;
use Sm\Vtiger2m2\Model\SyncToVtiger\Invoice;

class SyncInvoiceToVtiger extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var InvoiceFactory
     */
    protected $_invoiceFactory;

    /**
     * @var Invoice
     */
    protected $_invoiceSyncer;

    /**
     * @var \Zend\Log\Logger
     */
    protected $logger;

    /**
     * SyncInvoiceToVtiger constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param InvoiceFactory $invoiceFactory
     * @param Invoice $invoiceSyncer
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        InvoiceFactory $invoiceFactory,
        Invoice $invoiceSyncer
    )
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_invoiceFactory = $invoiceFactory;
        $this->_invoiceSyncer = $invoiceSyncer;
        // add log file
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/invoice_to_vtiger.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    public function execute()
    {
        try {
            // TODO: load all invoice then save each one
            $startTime = microtime(true);
            $allInvoices = $this->_invoiceFactory->create()->getCollection();
            $totalRecords = $allInvoices->getSize();
            $counter = 0;
            foreach ($allInvoices as $aInvoice) {
                /** @var \Magento\Sales\Model\Order\Invoice $aInvoice */
                $counter++;
                $this->logger->info("Index {$counter}/{$totalRecords}");
                $start = microtime(true);
                $this->_invoiceSyncer->sync($aInvoice->getId());
                $time = number_format(microtime(true) - $start, 5);
                $this->logger->info("Time: {$time}s \n");
            }
            // create message
            $message = "Synchronized - " . number_format(microtime(true) - $startTime, 2, '.', ',') . 's';
            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => true, 'message' => $message]);
        } catch (\Exception $e) {
            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => false, 'message' => $e->getMessage()]);
        }
    }
}

