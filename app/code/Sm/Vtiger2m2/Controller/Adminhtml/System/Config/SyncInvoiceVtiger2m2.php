<?php

namespace Sm\Vtiger2m2\Controller\Adminhtml\System\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class SyncInvoiceVtiger2m2 extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Sm\Vtiger2m2\Model\iConnector
     */
    protected $_connector;

    /**
     * @var \Sm\Vtiger2m2\Model\SyncToMagento\Invoice
     */
    protected $_invoiceSyncer;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $_configWriter;

    /**
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        \Sm\Vtiger2m2\Model\iConnector $iConnector,
        \Sm\Vtiger2m2\Model\SyncToMagento\Invoice $invoiceSyncer,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\Config\Storage\WriterInterface $configWriter
    )
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_connector = $iConnector;
        $this->_invoiceSyncer = $invoiceSyncer;
        $this->_scopeConfig = $scopeConfig;
        $this->_configWriter = $configWriter;
    }

    public function execute()
    {
        try {
            $limited = 100; // We only get max 100 records when query into vtiger system
            $startTime = microtime(true);

            $response = $this->_connector->countInvoice();
            if (!$response['success']) {
                $result = $this->resultJsonFactory->create();
                return $result->setData(['success' => false, 'message' => 'Please check the connection/query again!']);
            }

            $countInvoice = $response['result'][0]['count'];
            $loops = ceil($countInvoice / $limited);
            for ($i = 0; $i < $loops; $i++) {
                // get invoice data from vtiger
                $invoiceData = $this->_connector->getInvoice($limited, $limited * $i);
                $this->_invoiceSyncer->updateInvoice($invoiceData);
            }

            // create message
            $message = "Synchronized. Total time: " . number_format(microtime(true) - $startTime, 2, '.', ',') . 's
            . Check log at "var/log/sync_vtiger2m2_invoice.log" for more detail.';
            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => true, 'message' => $message]);
        } catch (\Exception $e) {
            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => false, 'message' => $e->getMessage()]);
        }
    }
}
?>