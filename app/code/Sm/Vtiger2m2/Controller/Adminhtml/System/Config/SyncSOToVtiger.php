<?php

namespace Sm\Vtiger2m2\Controller\Adminhtml\System\Config;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Sales\Model\OrderFactory;
use Sm\Vtiger2m2\Model\SyncToVtiger\SalesOrder;

class SyncSOToVtiger extends Action
{
    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var OrderFactory
     */
    protected $_orderFactory;

    /**
     * @var SalesOrder
     */
    protected $_soSyncer;

    /**
     * @var \Zend\Log\Logger
     */
    protected $logger;

    /**
     * SyncSOToVtiger constructor.
     * @param Context $context
     * @param JsonFactory $resultJsonFactory
     * @param OrderFactory $orderFactory
     * @param SalesOrder $soSyncer
     */
    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        OrderFactory $orderFactory,
        SalesOrder $soSyncer
    )
    {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_orderFactory = $orderFactory;
        $this->_soSyncer = $soSyncer;
        // add log file
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/so_to_vtiger.log');
        $this->logger = new \Zend\Log\Logger();
        $this->logger->addWriter($writer);
    }

    public function execute()
    {
        try {
            // TODO: load all orders then save each one
            $startTime = microtime(true);
            $allOrders = $this->_orderFactory->create()->getCollection();
            $totalRecords = $allOrders->getSize();
            $counter = 0;
            foreach ($allOrders as $aOrder) {
                /** @var \Magento\Sales\Model\Order $aOrder */
                $counter++;
                $this->logger->info("Index {$counter}/{$totalRecords}");
                $start = microtime(true);
                $this->_soSyncer->sync($aOrder->getIncrementId());
                $time = number_format(microtime(true) - $start, 5);
                $this->logger->info("Time: {$time}s \n");
            }
            // create message
            $message = "Synchronized - " . number_format(microtime(true) - $startTime, 2, '.', ',') . 's';
            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => true, 'message' => $message]);
        } catch (\Exception $e) {
            $result = $this->resultJsonFactory->create();
            return $result->setData(['success' => false, 'message' => $e->getMessage()]);
        }
    }
}

