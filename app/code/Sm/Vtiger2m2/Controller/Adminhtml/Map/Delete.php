<?php

namespace Sm\Vtiger2m2\Controller\Adminhtml\Map;

class Delete extends \Magesales\VtigerCrm\Controller\Adminhtml\Map
{
    public function execute()
    {
        try {
            $resultRedirect = $this->resultRedirectFactory->create();
            $id = $this->getRequest()->getParam('id');
            $map = $this->_mapFactory->create()->load($id);
            $map->delete();
            $this->messageManager->addSuccess(__('Delete success!'));
            return $resultRedirect->setPath('vtigercrm/map/index');
        } catch (\Exception $exception) {
            throw $exception;
        }
    }
}

?>