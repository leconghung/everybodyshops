<?php

namespace Sm\Vtiger2m2\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Sm\Vtiger2m2\Model\iConnector
     */
    protected $_connector;

    /**
     * @var \Sm\Vtiger2m2\Helper\Data
     */
    protected $_helper;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Sm\Vtiger2m2\Model\iConnector $connector,
        \Sm\Vtiger2m2\Helper\Data $helper
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_connector = $connector;
        $this->_helper = $helper;
    }

    public function execute()
    {

    }
}
