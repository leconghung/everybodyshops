<?php

namespace Sm\Vtiger2m2\Helper;

use \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;

class Data
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory
     */
    protected $_productAttributeCollectionFactory;
    /**
     * @var \Magesales\VtigerCrm\Model\Map
     */
    protected $_vtigerMap;

    /**
     * @var \Magesales\VtigerCrm\Model\MapFactory
     */
    protected $_mapFactory;

    /**
     * @var \Magento\Sales\Model\Order\StatusFactory
     */
    protected $_orderStatuses;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magento\Payment\Helper\Data
     */
    protected $_paymentHelper;

    /**
     * @var CategoryCollectionFactory
     */
    protected $categoryCollectionFactory;

    /**
     * Data constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory
     * @param \Magesales\VtigerCrm\Model\Map $map
     * @param \Magesales\VtigerCrm\Model\MapFactory $mapFactory
     * @param \Magento\Sales\Model\Order\StatusFactory $statusFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Payment\Helper\Data $paymentHelper
     * @param CategoryCollectionFactory $categoryCollectionFactory
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\Attribute\CollectionFactory $collectionFactory,
        \Magesales\VtigerCrm\Model\Map $map,
        \Magesales\VtigerCrm\Model\MapFactory $mapFactory,
        \Magento\Sales\Model\Order\StatusFactory $statusFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Helper\Data $paymentHelper,
        CategoryCollectionFactory $categoryCollectionFactory
    )
    {
        $this->_productAttributeCollectionFactory = $collectionFactory;
        $this->_vtigerMap = $map;
        $this->_mapFactory = $mapFactory;
        $this->_orderStatuses = $statusFactory;
        $this->_scopeConfig = $scopeConfig;
        $this->_paymentHelper = $paymentHelper;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    /**
     * @return array
     */
    public function getMagentoProductAttributeList()
    {
        $collection = $this->_productAttributeCollectionFactory;
        $attributeList = $collection->create()->addVisibleFilter();
        return $attributeList->getData();
    }

    /**
     * @return array
     */
    public function getProductAttributeArray()
    {
        $attributeData = $this->getMagentoProductAttributeList();
        $result = [];
        foreach ($attributeData as $item) {
            $result[$item['attribute_code']] = $item['frontend_label'];
        }

        // Add some files of inventory_stock
        $result['stock_stock_id'] = 'Stock';
        $result['stock_qty'] = 'Qty';
        $result['stock_min_qty'] = 'Min Qty';
        $result['entity_id'] = 'ID';
        return $result;
    }

    /**
     * @param $fields
     * @return array
     * convert vtiger attribute to magento attribute
     */
    public function mapField($fields, $type)
    {
        // get map field of vtiger
        $map = $this->_vtigerMap->getCollection()->addFieldToFilter('type', $type)->getData();
        $convertedMap = [];
        foreach ($map as $item) {
            // convert to key => value
            $convertedMap[$item['vtiger_field']] = $item['magento_field'];
        }

        // convert vtiger attribute to magento attribute
        $mappedField = [];
        foreach ($convertedMap as $key => $value) {
            if (key_exists($key, $fields)) {
                /** @var  $attr */
                $attr = $this->_productAttributeCollectionFactory->create()->addFieldToFilter('attribute_code', $value)->getFirstItem();
                if ($attr->getData('frontend_input') == 'select') {
                    $options = $attr->getSource()->getAllOptions();
                    $count = 0;
                    foreach ($options as $option) {
                        if ($option['label'] == str_replace(';', ',', $fields[$key])) {
                            $count++;
                            $mappedField[$value] = $option['value'];
                        }
                    }
                    if ($count == 0) {
                        $mappedField[$value] = '';
                    }
                } else {
                    $mappedField[$value] = $fields[$key];
                }
            } else {
                $mappedField[$value] = '';
            }
        }
        return $mappedField;
    }

    /**
     * @param $vTigerAttributes
     * @return array
     */
    public function getMagentoAttributeArr($vTigerAttributes)
    {
        $vTigerAttributes = str_replace(' ', '', $vTigerAttributes);
        $vTigerAttrArr = [];
        if (strpos($vTigerAttributes, '|##|') !== false) {
            $vTigerAttrArr = explode('|##|', $vTigerAttributes);
        }
        if (strpos($vTigerAttributes, ',') !== false) {
            $vTigerAttrArr = explode(',', $vTigerAttributes);
        }
        if (count($vTigerAttrArr) == 0) {
            $vTigerAttrArr[] = $vTigerAttributes;
        }
        return $vTigerAttrArr;
    }

    /**
     * @param $strName
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCategoryIds($strName)
    {
        $strName = 'Fishing |##| Camping';
        $catIds = [];
        $arrName = explode('|##|', $strName);
        $categoryCollection = $this->categoryCollectionFactory->create()->addAttributeToFilter('name', ['in' => $arrName]);
        foreach ($categoryCollection as $cat) {
            /** @var \Magento\Catalog\Model\Category $cat */
            $catIds[] = $cat->getId();
        }
        return $catIds;
    }

    /**
     * @param $label
     * @return mixed|null
     */
    public function getStatusCodeByLabel($label)
    {
        $code = null;
        $status = $this->_orderStatuses->create()->getCollection()->addFieldToFilter('label', $label);
        if ($status->count() > 0) {
            $code = $status->getFirstItem()->getData('status');
        }
        return $code;
    }

    /**
     * @param $code
     * @return mixed|null
     */
    public function getStatusLabelByCode($code)
    {
        $label = null;
        $status = $this->_orderStatuses->create()->getCollection()->addFieldToFilter('status', $code);
        if ($status->count() > 0) {
            $label = $status->getFirstItem()->getData('label');
        }
        return $label;
    }

    /**
     * @param $label
     * @return int|null|string
     */
    public function getPaymentCode($label)
    {
        $paymentList = $this->_paymentHelper->getPaymentMethodList();
        foreach ($paymentList as $key => $value) {
            if ($value == $label) {
                return $key;
            }
        }
        return null;
    }

    /**
     * @param $label
     * @return array|null
     */
    public function getShippingDetail($label)
    {
        $shippingList = $this->_scopeConfig->getValue('carriers');
        foreach ($shippingList as $key => $value) {
            if (isset($value['title']) && $value['title'] == $label) {
                return [
                    'code' => $key,
                    'description' => $value['title']
                ];
            }
        }
        return null;
    }

    /**
     * @param $code
     * @return array|null
     */
    public function getShippingLabel($code)
    {
        if (strpos($code, '_') !== false) {
            $code = explode('_', $code)[0];
        }
        $shippingList = $this->_scopeConfig->getValue('carriers');
        foreach ($shippingList as $key => $value) {
            if ($key == $code) {
                return isset($value['title']) ? $value['title'] : null;
            }
        }
        return null;
    }

    public function mapFieldSO($fields, $type)
    {
        // get map field of vtiger
        $map = $this->_vtigerMap->getCollection()->addFieldToFilter('type', $type)->getData();
        $convertedMap = [];
        foreach ($map as $item) {
            // convert to key => value
            $convertedMap[$item['vtiger_field']] = $item['magento_field'];
        }

        // convert vtiger attribute to magento attribute
        $mappedField = [];
        foreach ($convertedMap as $key => $value) {
            if (key_exists($key, $fields)) {
                // Set status
                if ($value == 'status' && $type == 'SalesOrder') {
                    $mappedField['status'] = $this->getStatusCodeByLabel($fields[$key]);
                    continue;
                }
                // Set shipping method & shipping description
                if ($value == 'shipping_method' && $type == 'SalesOrder') {
                    $mappedField['shipping_method'] = $this->getShippingDetail($fields[$key])['code'];
                    $mappedField['shipping_description'] = $this->getShippingDetail($fields[$key])['description'];
                    continue;
                }

                $attr = $this->_productAttributeCollectionFactory->create()->addFieldToFilter('attribute_code', $value)->getFirstItem();
                if ($attr->getData('frontend_input') == 'select') {
                    $options = $attr->getSource()->getAllOptions();
                    $count = 0;
                    foreach ($options as $option) {
                        if ($option['label'] == $fields[$key]) {
                            $count++;
                            $mappedField[$value] = $option['value'];
                        }
                    }
                    if ($count == 0) {
                        $mappedField[$value] = '';
                    }
                } else {
                    $mappedField[$value] = $fields[$key];
                }
            } else {
                $mappedField[$value] = '';
            }
        }
        return $mappedField;
    }
}