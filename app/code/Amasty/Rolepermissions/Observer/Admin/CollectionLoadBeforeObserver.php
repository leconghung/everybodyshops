<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Rolepermissions
 */


namespace Amasty\Rolepermissions\Observer\Admin;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\DB\Select;

class CollectionLoadBeforeObserver implements ObserverInterface
{
    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry;

    /** @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection $entity */
    protected $entity;

    /** @var \Amasty\Rolepermissions\Helper\Data $helper */
    protected $helper;

    /**
     * CollectionLoadBeforeObserver constructor.
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection $entity
     * @param \Amasty\Rolepermissions\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\Registry $registry,
        \Magento\Eav\Model\ResourceModel\Entity\Attribute\Collection $entity,
        \Amasty\Rolepermissions\Helper\Data $helper
    ) {
        $this->_coreRegistry = $registry;
        $this->entity = $entity;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $collection = $observer->getCollection();

        /** @var \Amasty\Rolepermissions\Model\Rule $rule */
        $rule = $this->_coreRegistry->registry('current_amrolepermissions_rule');

        if (!$rule) {
            return;
        }

        $config = $rule->getCollectionConfig();

        if ($rule->getScopeStoreviews()) {
            //restriction by stores on All Stores page
            if ($collection instanceof \Magento\Store\Model\ResourceModel\Store\Collection
                || $collection instanceof \Magento\Store\Model\ResourceModel\Website\Collection
            ) {
                /** Sometimes we shouldn't change store collection to load default values from default stores */
                if (!$this->_coreRegistry->registry('am_dont_change_collection')) {
                    $collectionClone = clone $collection;//for not loading of main collection before filters
                    $collectionData = $collectionClone->getData();
                    if (isset($collectionData[0])) {
                        if (key_exists('store_id', $collectionData[0])) {
                            $allowedStoreViews = $rule->getScopeStoreviews();
                            $collection->addFieldToFilter('store_id', ['in' => $allowedStoreViews]);
                        }
                    }
                } else {
                    $this->_coreRegistry->unregister('am_dont_change_collection');
                }
            }
            //restriction by stores on reports -> by customers page
            if ($collection instanceof \Magento\Reports\Model\ResourceModel\Review\Customer\Collection) {
                $collection->getSelect()
                    ->joinLeft(
                        ['review_detail' => $collection->getTable('review_detail')],
                        'review_detail.review_id = main_table.review_id',
                        []
                    )
                    ->joinLeft(
                        ['customer_entity' => $collection->getTable('customer_entity')],
                        'customer_entity.entity_id = review_detail.customer_id',
                        ['store_id']
                    );
                $allowedStoreViews = $rule->getScopeStoreviews();
                $collection->addFieldToFilter('customer_entity.store_id', ['in' => $allowedStoreViews]);
            }

            if ($collection instanceof \Magento\Sales\Model\ResourceModel\Order\Payment\Transaction\Collection) {
                $collection->getSelect()->join(
                    ['am_rp_order' => $collection->getTable('sales_order')],
                    'am_rp_order.entity_id = main_table.order_id',
                    []
                );

                $config = ['internal' => ['store' => ['Magento\Sales\Model\ResourceModel\Order\Payment\Transaction' => 'am_rp_order.store_id']]];
            } else if ($collection instanceof \Magento\Review\Model\ResourceModel\Review\Product\Collection) {
                $config = ['internal' => ['store' => ['Magento\Catalog\Model\ResourceModel\Product' => 'store.store_id']]];
            } else if ($collection instanceof \Amasty\Groupcat\Model\ResourceModel\Rules\Collection) {
                $config = ['internal' => ['store' => ['\Amasty\Groupcat\Model\ResourceModel\Rules' => 'main_table.stores']]];
            }

            foreach ($config as $joinType => $scopes) {
                foreach ($scopes as $scope => $collectionsConfig) {
                    foreach ($collectionsConfig as $modelType => $field) {
                        if (!($collection->getresource() instanceof $modelType)) {
                            continue;
                        }

                        if ($scope == 'store') {
                            $ids = $rule->getScopeStoreviews();
                            $ids []= 0;
                        } else {
                            $ids = $rule->getPartiallyAccessibleWebsites();
                        }

                        if ($joinType == 'internal') {
                            $select = $collection->getSelect();
                            if (false === strpos($field, '.')) {
                                if ($alias = $this->getMainAlias($select)) {
                                    $field = "$alias.$field";
                                }
                            }
                            // sets intersection
                            $conditionSql = "";
                            foreach ($ids as $id) {
                                $conditionSql .= " OR $id IN ($field)";
                            }

                            $select->where("0 IN ($field) $conditionSql");
                        } else {
                            $idField = $collection->getResource()->getIdFieldName();
                            $select = $collection->getSelect();
                            $storeSelect = clone $select;
                            $storeSelect->reset()
                                ->from(['amrolepermissions_join' => $collection->getResource()->getTable($field)], $idField)
                                ->where("amrolepermissions_join.{$scope}_id IN (?)", $ids);
                            $select->where("main_table.$idField IN ($storeSelect)");
                        }

                        return;
                    }
                }
            }
        }

        $ruleCategories = $rule->getCategories();
        if ($ruleCategories) {
            $ruleCategories = $this->helper->getParentCategoriesIds($ruleCategories);

            if ($collection instanceof \Magento\Catalog\Model\ResourceModel\Category\Collection) {
                $collection->addFieldToFilter('entity_id', ['in' => $ruleCategories]);
            }
        }

        $ruleAttributes = $rule->getAttributes();
        if ($ruleAttributes && !$this->_coreRegistry->registry('its_amrolepermissions')) {
            if ($collection instanceof \Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection) {
                $collection->addFieldToFilter('main_table.attribute_id', ['in' => $ruleAttributes]);
            }
        }
    }

    /**
     * @param Select $select
     * @return bool|int|string
     */
    protected function getMainAlias(Select $select)
    {
        $from = $select->getPart(\Zend_Db_Select::FROM);
        foreach ($from as $alias => $data) {
            if ($data['joinType'] == 'from') {
                return $alias;
            }
        }
        return false;
    }
}
