<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Rolepermissions
 */


namespace Amasty\Rolepermissions\Observer\Admin\Product;

use Magento\Framework\Event\ObserverInterface;

class EditPostdispatchObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\ViewInterface
     */
    protected $_view;

    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    protected $_authorization;

    /** @var \Amasty\Rolepermissions\Helper\Data $helper */
    protected $helper;

    /**
     * EditPostdispatchObserver constructor.
     * @param \Magento\Framework\App\ViewInterface $view
     * @param \Magento\Framework\AuthorizationInterface $authorization
     * @param \Amasty\Rolepermissions\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\App\ViewInterface $view,
        \Magento\Framework\AuthorizationInterface $authorization,
        \Amasty\Rolepermissions\Helper\Data $helper
    ) {
        $this->_view = $view;
        $this->_authorization = $authorization;
        $this->helper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->helper->restrictAttributeSets()) {
            $toolbar = $this->_view->getLayout()->getBlock('page.actions.toolbar');
            $toolbar->unsetChild('addButton');
        }
    }
}
