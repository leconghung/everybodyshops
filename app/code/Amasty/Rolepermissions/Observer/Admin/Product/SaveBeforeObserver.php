<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Rolepermissions
 */


namespace Amasty\Rolepermissions\Observer\Admin\Product;

use Magento\Framework\Event\ObserverInterface;

class SaveBeforeObserver implements ObserverInterface
{
    /** @var \Amasty\Rolepermissions\Helper\Data */
    protected $helper;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Magento\Framework\AuthorizationInterface
     */
    protected $_authorization;

    /**
     * @var \Magento\Backend\Model\Auth\Session
     */
    protected $_authSession;


    public function __construct(
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Amasty\Rolepermissions\Helper\Data $helper,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\AuthorizationInterface $authorization,
        \Magento\Backend\Model\Auth\Session $authSession
    ) {
        $this->_objectManager = $objectManager;
        $this->helper = $helper;
        $this->_request = $request;
        $this->_authorization = $authorization;
        $this->_authSession = $authSession;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->_request->getModuleName() == 'api')
            return;

        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getProduct();

        $rule = $this->helper->currentRule();

        if (!$this->_authorization->isAllowed('Amasty_Rolepermissions::save_products')) {
            $this->helper->redirectHome();
        }

        if (!$rule->checkProductPermissions($product)
            && !$rule->checkProductOwner($product)
        ) {
            $this->helper->redirectHome();
        }

        if ($rule->getScopeStoreviews()) {
            $orig = $this->_objectManager->get('Magento\Catalog\Model\ResourceModel\Product')->getWebsiteIds($product);
            $new = $product->getData('website_ids');

            if ($orig != $new && !is_null($new)) {
                $ids = $this->helper->combine($orig, $new, $rule->getPartiallyAccessibleWebsites());
                $product->setWebsiteIds($ids);
            }

            if (!$product->getId()) {
                $product->setData('amrolepermissions_disable');
            }
        }

        if ($rule->getCategories()) {
            $ids = $this->helper->combine(
                $product->getOrigData('category_ids'),
                $product->getCategoryIds(),
                $rule->getCategories()
            );
            $product->setCategoryIds($ids);
        }

        if (!$this->_authorization->isAllowed('Amasty_Rolepermissions::product_owner')
            && $this->_authSession->getUser()) {
            $product->unsetData('amrolepermissions_owner');
        }
    }
}
