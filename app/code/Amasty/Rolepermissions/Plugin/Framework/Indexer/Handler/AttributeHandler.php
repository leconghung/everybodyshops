<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2017 Amasty (https://www.amasty.com)
 * @package Amasty_Rolepermissions
 */


namespace Amasty\Rolepermissions\Plugin\Framework\Indexer\Handler;

use Magento\Framework\App\ResourceConnection\SourceProviderInterface;

class AttributeHandler extends \Magento\Framework\Indexer\Handler\AttributeHandler
{
    /**
     * @param $subject
     * @param SourceProviderInterface $source
     * @param $alias
     * @param $fieldInfo
     * @return array
     */
    public function beforePrepareSql($subject, SourceProviderInterface $source, $alias, $fieldInfo)
    {
        if (!isset($fieldInfo['bind'])) {
            $fieldInfo['bind'] = '';
            $source->addFieldToSelect($fieldInfo['origin'], $alias);
        }

        return [$source, $alias, $fieldInfo];
    }
}

