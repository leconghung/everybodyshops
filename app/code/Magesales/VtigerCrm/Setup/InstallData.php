<?php

namespace Magesales\VtigerCrm\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        // Get magesales_vtigercrm_map table
        $tableName = $setup->getTable('magesales_vtigercrm_map');
        // Check if the table already exists
        if ($setup->getConnection()->isTableExists($tableName) == true) {
            // Declare data
            $data = [
                // Accounts
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'email1',
                    'magento_field' => 'email',
                    'description' => 'Email',
                    'status' => 1
                ],
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'createdtime',
                    'magento_field' => 'created_at',
                    'description' => 'Created At',
                    'status' => 1
                ],
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'modifiedtime',
                    'magento_field' => 'update_at',
                    'description' => 'Updated At',
                    'status' => 1
                ],
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'fax',
                    'magento_field' => 'bill_fax',
                    'description' => 'Fax',
                    'status' => 1
                ],
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'bill_street',
                    'magento_field' => 'bill_street',
                    'description' => 'Billing Street',
                    'status' => 1
                ],
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'bill_city',
                    'magento_field' => 'bill_city',
                    'description' => 'Billing City',
                    'status' => 1
                ],
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'bill_country',
                    'magento_field' => 'bill_country_id',
                    'description' => 'Billing Country',
                    'status' => 1
                ],
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'bill_state',
                    'magento_field' => 'bill_region',
                    'description' => 'Billing State/Province',
                    'status' => 1
                ],
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'bill_code',
                    'magento_field' => 'bill_postcode',
                    'description' => 'Billing Zip/Postal Code',
                    'status' => 1
                ],
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'ship_street',
                    'magento_field' => 'ship_street',
                    'description' => 'Shipping Street',
                    'status' => 1
                ],
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'ship_city',
                    'magento_field' => 'ship_city',
                    'description' => 'Shipping City',
                    'status' => 1
                ],
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'ship_country',
                    'magento_field' => 'ship_country_id',
                    'description' => 'Shipping Country',
                    'status' => 1
                ],
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'ship_state',
                    'magento_field' => 'ship_region',
                    'description' => 'Shipping State/Province',
                    'status' => 1
                ],
                [
                    'type' => 'Accounts',
                    'vtiger_field' => 'ship_code',
                    'magento_field' => 'ship_postcode',
                    'description' => 'Shipping Zip/Postal Code',
                    'status' => 1
                ],
                // Leads
                [
                    'type' => 'Leads',
                    'vtiger_field' => 'email',
                    'magento_field' => 'email',
                    'description' => 'Email',
                    'status' => 1
                ],
                [
                    'type' => 'Leads',
                    'vtiger_field' => 'firstname',
                    'magento_field' => 'firstname',
                    'description' => 'First Name',
                    'status' => 1
                ],
                [
                    'type' => 'Leads',
                    'vtiger_field' => 'lastname',
                    'magento_field' => 'lastname',
                    'description' => 'Last Name',
                    'status' => 1
                ],
                [
                    'type' => 'Leads',
                    'vtiger_field' => 'createdtime',
                    'magento_field' => 'created_at',
                    'description' => 'Created At',
                    'status' => 1
                ],
                [
                    'type' => 'Leads',
                    'vtiger_field' => 'modifiedtime',
                    'magento_field' => 'update_at',
                    'description' => 'Updated At',
                    'status' => 1
                ],
                [
                    'type' => 'Leads',
                    'vtiger_field' => 'cf_759',
                    'magento_field' => 'dob',
                    'description' => 'Date of Birth',
                    'status' => 1
                ],
                [
                    'type' => 'Leads',
                    'vtiger_field' => 'cf_757',
                    'magento_field' => 'gender',
                    'description' => 'Gender',
                    'status' => 1
                ],
                // Contacts
                [
                    'type' => 'Contacts',
                    'vtiger_field' => 'email',
                    'magento_field' => 'email',
                    'description' => 'Email',
                    'status' => 1
                ],
                [
                    'type' => 'Contacts',
                    'vtiger_field' => 'firstname',
                    'magento_field' => 'firstname',
                    'description' => 'First Name',
                    'status' => 1
                ],
                [
                    'type' => 'Contacts',
                    'vtiger_field' => 'lastname',
                    'magento_field' => 'lastname',
                    'description' => 'Last Name',
                    'status' => 1
                ],
                [
                    'type' => 'Contacts',
                    'vtiger_field' => 'createdtime',
                    'magento_field' => 'created_at',
                    'description' => 'Created At',
                    'status' => 1
                ],
                [
                    'type' => 'Contacts',
                    'vtiger_field' => 'modifiedtime',
                    'magento_field' => 'update_at',
                    'description' => 'Updated At',
                    'status' => 1
                ],
                [
                    'type' => 'Contacts',
                    'vtiger_field' => 'fax',
                    'magento_field' => 'bill_fax',
                    'description' => 'Fax',
                    'status' => 1
                ],
                [
                    'type' => 'Contacts',
                    'vtiger_field' => 'birthday',
                    'magento_field' => 'dob',
                    'description' => 'Date of Birth',
                    'status' => 1
                ],
                // Products
                [
                    'type' => 'Products',
                    'vtiger_field' => 'productname',
                    'magento_field' => 'name',
                    'description' => 'Product Name',
                    'status' => 1
                ],
                [
                    'type' => 'Products',
                    'vtiger_field' => 'productcode',
                    'magento_field' => 'sku',
                    'description' => 'Product Sku',
                    'status' => 1
                ],
                [
                    'type' => 'Products',
                    'vtiger_field' => 'unit_price',
                    'magento_field' => 'price',
                    'description' => 'Product Price',
                    'status' => 1
                ],
                [
                    'type' => 'Products',
                    'vtiger_field' => 'sales_start_date',
                    'magento_field' => 'special_from_date',
                    'description' => 'Special From Date',
                    'status' => 1
                ],
                [
                    'type' => 'Products',
                    'vtiger_field' => 'sales_end_date',
                    'magento_field' => 'special_to_date',
                    'description' => 'Special To Date',
                    'status' => 1
                ],
                [
                    'type' => 'Products',
                    'vtiger_field' => 'qtyinstock',
                    'magento_field' => 'stock_qty',
                    'description' => 'Qty. in Stock',
                    'status' => 1
                ],
                [
                    'type' => 'Products',
                    'vtiger_field' => 'description',
                    'magento_field' => 'description',
                    'description' => 'Description',
                    'status' => 1
                ],
                [
                    'type' => 'Products',
                    'vtiger_field' => 'imagename',
                    'magento_field' => 'image',
                    'description' => 'Product Image',
                    'status' => 1
                ],
            ];

            // Insert data to table
            foreach ($data as $item) {
                $setup->getConnection()->insert($tableName, $item);
            }
        }

        $setup->endSetup();
    }
}
