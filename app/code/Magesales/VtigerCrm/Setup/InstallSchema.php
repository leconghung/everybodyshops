<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Magesales\VtigerCrm\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();
        /**
         * Create table 'magesales_vtigerintegration_field'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('magesales_vtigercrm_field'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'type',
                Table::TYPE_TEXT,
                null,
                [],
                'Type'
            )
            ->addColumn(
                'vtigercrm',
                Table::TYPE_TEXT,
                null,
                [],
                'Vtiger Field'
            )
            ->addColumn(
                'magento',
                Table::TYPE_TEXT,
                null,
                [],
                'Magento'
            );
        $installer->getConnection()->createTable($table);

        /**
         * Create table 'magesales_vtigerintegration_map'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('magesales_vtigercrm_map'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )
            ->addColumn(
                'type',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Type'
            )
            ->addColumn(
                'vtiger_field',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Vtiger Field'
            )
            ->addColumn(
                'magento_field',
                Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Magento Field'
            )
            ->addColumn(
                'description',
                Table::TYPE_TEXT,
                null,
                ['nullable' => true],
                'Description'
            )
            ->addColumn(
                'status',
                Table::TYPE_SMALLINT,
                null,
                ['nullable' => false, 'default' => 0],
                'Status'
            );
        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
