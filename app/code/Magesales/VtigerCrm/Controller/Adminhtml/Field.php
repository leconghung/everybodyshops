<?php
namespace Magesales\VtigerCrm\Controller\Adminhtml;

use Magesales\VtigerCrm\Model\Connector;
use Magesales\VtigerCrm\Model\FieldFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;

abstract class Field extends Action
{
    /**
     * @var \Magento\Backend\App\Action\Context;
     */
    protected $_context;

    /**
     * @var \Magesales\VtigerCrm\Model\Connector
     */
    protected $_connector;

    /**
     * Array of actions which can be processed without secret key validation
     *
     * @var array
     */
    protected $_publicActions = ['edit'];

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry = null;

    /**
     * Review model factory
     *
     * @var \Magesales\VtigerCrm\Model\MapFactory
     */

    protected $_fieldFactory;

    /**
     * Field constructor.
     * @param Context $context
     * @param Registry $coreRegistry
     * @param FieldFactory $fieldFactory
     * @param Connector $connector
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FieldFactory $fieldFactory,
        Connector $connector
    ) {
        $this->_context = $context;
        $this->coreRegistry = $coreRegistry;
        $this->_fieldFactory = $fieldFactory;
        $this->_connector = $connector;
        parent::__construct($context);
    }
}
