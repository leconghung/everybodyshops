<?php
/**
 * Copyright © 2015 Magesales. All rights reserved.
 * See COPYING.txt for license details.

 */
namespace Magesales\VtigerCrm\Controller\Adminhtml\Field;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magesales\VtigerCrm\Controller\Adminhtml\Field as FieldController;
use Magesales\VtigerCrm\Model\Connector;
use Magesales\VtigerCrm\Model\FieldFactory;

class Retrieve extends FieldController
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    public function __construct(
        Context $context,
        Registry $coreRegistry,
        FieldFactory $fieldFactory,
        Connector $connector,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    )
    {
        parent::__construct($context, $coreRegistry, $fieldFactory, $connector);
        $this->resultJsonFactory = $resultJsonFactory;
    }

    /**
     * execute
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $type = $data['type'];
            $out = array();
            $fieldModel = $this->_fieldFactory->create();
            $table = $fieldModel->getAllTable();
            $m_table = $table[$type];
            $vtigerFields = $fieldModel->getVtigerFields($type);
            $vtigerOption = '';
            foreach ($vtigerFields as $value => $label) {
                $vtigerOption .= "<option value='$value' >" . $label . "</option>";
            }
            $out['vtiger_options'] = $vtigerOption;
            $magentoFields = $fieldModel->getMagentoFields($m_table);
            $magentoOption = '';
            if ($magentoFields) {
                foreach ($magentoFields as $value => $label) {
                    $magentoOption .= "<option value ='$value' >" . $label . "</option>";
                }
            }
            $out['magento_options'] = $magentoOption;
            return $this->resultJsonFactory->create()->setData($out);
        }
    }
}
