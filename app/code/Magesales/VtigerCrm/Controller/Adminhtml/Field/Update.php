<?php
/**
 * Copyright © 2015 Magesales. All rights reserved.
 * See COPYING.txt for license details.
 
 */
namespace Magesales\VtigerCrm\Controller\Adminhtml\Field;

use Magesales\VtigerCrm\Model\FieldFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

class Update extends Action
{
    /**
     * @var \Magesales\VtigerCrm\Model\FieldFactory
     */
    protected $_fieldFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magesales\VtigerCrm\Model\FieldFactory $fieldFactory
     */
    public function __construct(
        Context $context,
        FieldFactory $fieldFactory
    ) {
        parent::__construct($context);
        $this->_fieldFactory = $fieldFactory;
    }

    /**
     * execute
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $model = $this->_fieldFactory->create();
            $table = $model->getAllTable();
            foreach ($table as $v_table => $m_table) {
                $model = $this->_fieldFactory->create();
                $model->saveFields($v_table, $m_table, true);
            }
        }
    }
}
