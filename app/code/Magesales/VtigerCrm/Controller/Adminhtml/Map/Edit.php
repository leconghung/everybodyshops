<?php
/**
 * Copyright © 2015 Magesales. All rights reserved.
 * See COPYING.txt for license details.
 *
  */
namespace Magesales\VtigerCrm\Controller\Adminhtml\Map;

use Magesales\VtigerCrm\Controller\Adminhtml\Map as MapController;
use Magesales\VtigerCrm\Model\MapFactory;
use Magesales\VtigerCrm\Model\ResourceModel\Map\CollectionFactory as MapCollectionFactory;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\ForwardFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

class Edit extends MapController
{
    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;
    /**
     * @var \Magento\Backend\Model\View\Result\Forward
     */
    protected $resultForwardFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magesales\VtigerCrm\Model\MapFactory $mapFactory
     * @param \Magesales\VtigerCrm\Model\ResourceModel\Map\CollectionFactory $collectionFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        MapFactory $mapFactory,
        MapCollectionFactory $collectionFactory,
        ForwardFactory $resultForwardFactory
    ) {
        parent::__construct($context, $coreRegistry, $resultPageFactory, $mapFactory, $collectionFactory);
        $this->coreRegistry = $coreRegistry;
        $this->resultForwardFactory = $resultForwardFactory;
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Magesales_VtigerCrm::vtigercrm_integration');
        $id = $this->getRequest()->getParam('id');
        $model = $this->_mapFactory->create();
        if ($id) {
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This mapping no longer exists.'));
                /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();

                return $resultRedirect->setPath('*/*/');
            }
        }

        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        $this->coreRegistry->register('vtiger_mapping', $model);
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_initAction();
        $resultPage->getConfig()->getTitle()
            ->prepend($model->getId() ? __('Edit Mapping') : __('New Mapping'));

        $resultPage->addContent($resultPage->getLayout()->createBlock('Magesales\VtigerCrm\Block\Adminhtml\Map\Edit'));
        return $resultPage;
    }
}
