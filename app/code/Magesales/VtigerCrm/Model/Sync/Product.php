<?php
namespace Magesales\VtigerCrm\Model\Sync;

use Magesales\VtigerCrm\Model\Connector;
use Magesales\VtigerCrm\Model\Data;
use Magento\Catalog\Model\Product as CatalogProduct;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Customer\Model\Customer;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;

class Product extends Connector
{
    const TYPE = "Products";
    const FIELD = "productcode";
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param PhpCookieManager $cookieManager
     * @param PublicCookieMetadata $cookieMetadata
     * @param \Magesales\VtigerCrm\Model\Data $data
     * @param \Magento\Catalog\Model\Product $product
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Config $resourceConfig,
        ZendClientFactory $httpClientFactory,
        PhpCookieManager $cookieManager,
        PublicCookieMetadata $cookieMetadata,
        Data $data,
        CatalogProduct $product
    ) {
        parent::__construct($scopeConfig, $resourceConfig, $httpClientFactory);
        $this->_data = $data;
        $this->_product = $product;
    }

    /**
     * Update or create new a record
     *
     * @param int $id
     * @return string
     */
    public function sync($id)
    {
        $model = $this->_product->load($id);
        $name = $model->getName();
        $code = $model->getSku();
        $status = $model->getStatus();

        $params = $this->_data->getProduct($model, self::TYPE);
        $params += [
            'productname' => $name,
            'productcode' => $code,
            'discontinued' => $status == 1 ? true : false
        ];
        /**
         * $response = ["success" => true,
         *              "result" => [
         *                      0 => ["id" => "..."],
         *                      1 => []
         *              ]
         * ];
         */
        $response = $this->query(self::TYPE, self::FIELD, $code);
        if (!isset($response['result'][0]['id'])) {

            $response = $this->create(self::TYPE, $params);
            return $response;
        } else {
            $element = $response['result']['0'];
            foreach ($params as $key => $value) {
                $element[$key] = $value;
            }
            $response = $this->update($element);
            return $response;
        }
    }

    /**
     * Delete Record
     * @param array $data
     */
    public function deleteProduct($data)
    {
        $id = $this->query(self::TYPE, self::FIELD, $data);
        if ($id['id']) {
            $this->delete($id);
        }
    }
}
