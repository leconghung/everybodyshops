<?php
namespace Magesales\VtigerCrm\Model\Sync;

use Magesales\VtigerCrm\Model\Connector;
use Magesales\VtigerCrm\Model\Data;
use Magesales\VtigerCrm\Model\Sync\Account as SyncAccount;
use Magesales\VtigerCrm\Model\Sync\Contact as SyncContact;
use Magesales\VtigerCrm\Model\Sync\Product as SyncProduct;
use Magento\Catalog\Model\Product;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Invoice as OrderInvoice;

/**
 * Class Invoice using to sync to Invoices table
 *
 * @package Magesales\VtigerCrm\Model\Sync
 */
class Invoice extends Connector
{
    const TYPE = "Invoice";

    /**
     * @var \Magesales\VtigerCrm\Model\Data
     */
    protected $_data;

    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;

    /**
     * @var \Magento\Sales\Model\Order
     */
    protected $_order;

    /**
     * @var \Magento\Sales\Model\Order\Invoice
     */
    protected $_invoice;

    /**
     * @var \Magesales\VtigerCrm\Model\Sync\Contact
     */
    protected $_contact;

    /**
     * @var \Magesales\VtigerCrm\Model\Sync\Account
     */
    protected $_account;
    /**
     * @var \Magesales\VtigerCrm\Model\Sync\Product
     */
    protected $_syncProduct;

    /**
     * Invoice constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Config $resourceConfig
     * @param ZendClientFactory $httpClientFactory
     * @param PhpCookieManager $cookieManager
     * @param PublicCookieMetadata $cookieMetadata
     * @param Data $data
     * @param Product $product
     * @param Order $order
     * @param OrderInvoice $invoice
     * @param Contact $contact
     * @param Account $account
     * @param \Magesales\VtigerCrm\Model\Sync\Product $syncProduct
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Config $resourceConfig,
        ZendClientFactory $httpClientFactory,
        PhpCookieManager $cookieManager,
        PublicCookieMetadata $cookieMetadata,
        Data $data,
        Product $product,
        Order $order,
        OrderInvoice $invoice,
        SyncContact $contact,
        SyncAccount $account,
        SyncProduct $syncProduct
    ) {
        parent::__construct($scopeConfig, $resourceConfig, $httpClientFactory);
        $this->_data = $data;
        $this->_order = $order;
        $this->_invoice = $invoice;
        $this->_product = $product;
        $this->_account = $account;
        $this->_contact = $contact;
        $this->_syncProduct = $syncProduct;
    }

    /**
     * @param $id
     * @return mixed|string
     */
    public function sync($id)
    {
        $model = $this->_invoice->load($id);
        $orderId = $model->getOrderId();
        $model = $this->_order->load($orderId);
        $customerId = $model->getCustomerId();
        $email = $model->getCustomerEmail();
        $subject = $model->getIncrementId();
        $discount_amount = $model->getDiscountAmount();
        $shipping_amount = $model->getShippingAmount();
        $productId = "";
        $lineItem = [];

        /* Sync Account and Contact */
        if ($customerId) {
            $this->_account->sync($customerId);
            $this->_contact->sync($customerId);
        } else {
            $account = ['accountname' => $email, 'email1' => $email];
            $this->create("Accounts", $account);
        }
        $response = $this->query("Accounts", "email1", $email);
        $accountId = $response['result'][0]['id'];
        $bill_address = $model->getBillingAddress();
        $ship_address = $model->getShippingAddress();
        $bill_street = $bill_address->getStreet();
        $ship_street = $ship_address->getStreet();

        $i = 0;
        foreach ($model->getAllItems() as $item) {
            $product_id = $item->getProductId();
            $price = $item->getPrice();
            $qty = $item->getQtyOrdered();

            if ($price > 0) {
                // get productId
                $response = $this->_syncProduct->sync($product_id);
                $productId = $response['result']['id'];
                $lineItem[$i] = ['productid' => $productId, 'quantity' => $qty, 'listprice' => $price];
                $i++;
            }
        }
        $data = [
            'subject' => $subject,
            'sostatus' => 'Created',
            'productid' => $productId,
            'account_id' => $accountId,
            'bill_street' => $bill_street[0],
            'bill_city' => $bill_address->getCity(),
            'bill_stage' => $bill_address->getRegion(),
            'bill_code' => $bill_address->getPostcode(),
            'bill_country' => $bill_address->getCountryId(),
            'ship_street' => $ship_street[0],
            'ship_city' => $ship_address->getCity(),
            'ship_stage' => $ship_address->getRegion(),
            'ship_code' => $ship_address->getPostcode(),
            'ship_country' => $ship_address->getCountryId(),
            'invoicestatus' => 'Paid',
            'hdnTaxType' => 'group',
            'conversion_rate' => 1,
            'hdnS_H_Amount' => $shipping_amount,
            'LineItems' => $lineItem,
            'currency_id' => '21x1',
            'hdnDiscountAmount' => $discount_amount
        ];
        $response = $this->create(self::TYPE, $data);

        return $response;
    }
}
