<?php
namespace Magesales\VtigerCrm\Model\Sync;

use Magesales\VtigerCrm\Model\Connector;
use Magesales\VtigerCrm\Model\Data;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Customer\Model\Customer;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;

/**
 * Class Lead using to sync Leads table
 *
 * @property string _table
 * @package Magesales\VtigerCrm\Model\Sync
 */
class Lead extends Connector
{
    const TYPE = "Leads";
    const FIELD = "email";
    /**
     * @var \Magento\Customer\Model\Customer
     */
    protected $_customer;

    /**
     * @var \Magesales\VtigerCrm\Model\Data
     */
    protected $_data;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param PhpCookieManager $cookieManager
     * @param PublicCookieMetadata $cookieMetadata
     * @param Data $data
     * @param \Magento\Customer\Model\Customer $customer
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Config $resourceConfig,
        ZendClientFactory $httpClientFactory,
        PhpCookieManager $cookieManager,
        PublicCookieMetadata $cookieMetadata,
        Data $data,
        Customer $customer
    ) {
        parent::__construct($scopeConfig, $resourceConfig, $httpClientFactory);
        $this->_data = $data;
        $this->_customer = $customer;
    }

    /**
     * Update or create new a record
     *
     * @param int $id
     * @return string
     */
    public function sync($id)
    {
        $model = $this->_customer->load($id);
        $email = $model->getEmail();
        $firstname = $model->getFirstname();
        $lastname = $model->getLastname();

        $params = $this->_data->getCustomer($model, self::TYPE);
        $params += [
            'lastname' => $lastname,
            'firstname' => $firstname,
            'email' => $email,
        ];
        // Check Email in Contact
        $response = $this->query(self::TYPE, self::FIELD, $email);
        if (!isset($response['result'][0]['id'])) {
            $response = $this->create(self::TYPE, $params);
            return $response;
        } else {
            $element = $response['result']['0'];
            ;
            foreach ($params as $key => $value) {
                $element[$key] = $value;
            }
            $response = $this->update($element);
            return $response;
        }
    }

    /**
     * Delete Record
     * @param string $email
     */
    public function deleteLead($email)
    {
        $response = $this->query(self::TYPE, self::FIELD, $email);
        if ($response['result'][0]['id']) {
            $this->delete($response['result'][0]['id']);
        }
    }
}
