<?php
namespace Magesales\VtigerCrm\Model\Sync;

use Magesales\VtigerCrm\Model\Connector;
use Magesales\VtigerCrm\Model\Data;
use Magento\CatalogRule\Model\Rule;
use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;

class Campaign extends Connector
{
    const TYPE = "Campaigns";
    const FIELD = "campaignname";
    /**
     * @var \Magento\Catalog\Model\Product
     */
    protected $_product;

    /**
     * @var \Magento\CatalogRule\Model\Rule
     */
    protected $_rule;

    /**
     * @var \Magesales\VtigerCrm\Model\Data
     */
    protected $_data;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Config\Model\ResourceModel\Config $resourceConfig
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param \Magento\Framework\Stdlib\Cookie\PhpCookieManager $cookieManager
     * @param \Magento\Framework\Stdlib\Cookie\PublicCookieMetadata $cookieMetadata
     * @param \Magesales\VtigerCrm\Model\Data $data
     * @param \Magento\CatalogRule\Model\Rule $rule
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Config $resourceConfig,
        ZendClientFactory $httpClientFactory,
        PhpCookieManager $cookieManager,
        PublicCookieMetadata $cookieMetadata,
        Data $data,
        Rule $rule
    ) {
        parent::__construct($scopeConfig, $resourceConfig, $httpClientFactory);
        $this->_data = $data;
        $this->_rule = $rule;
    }

    /**
     * Create new a record
     *
     * @param int $id
     * @return string
     */
    public function sync($id)
    {
        $model = $this->_rule->load($id);
        $status = $model->getIsActive();

        if ($status == 0) {
            $status = "Inactive";
        } else {
            $status = "Active";
        }

        $params = [
            'campaignname' => $model->getName(),
            'closingdate' => $model->getToDate(),
            'campaignstatus' => $status
        ];
        $response = $this->create(self::TYPE, $params);
        return $response;
    }

    /*
     * Delete Records Campaign
     */

    public function deleteCampaign($nameCam)
    {
        $response = $this->query(self::TYPE, self::FIELD, $nameCam);
        if ($response['result'][0]['id']) {
            $this->delete($response['result'][0]['id']);
        }
    }
}
