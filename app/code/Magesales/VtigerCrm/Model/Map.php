<?php
namespace Magesales\VtigerCrm\Model;

use Magesales\VtigerCrm\Model\ResourceModel\Map as ResourceMap;
use Magesales\VtigerCrm\Model\ResourceModel\Map\Collection;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;

/**
 * Class Map: save Mapping
 *
 * @package Magesales\VtigerCrm\Model
 */
class Map extends AbstractModel
{
    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'blog_post';

    protected $_eventPrefix = 'map';

    /**
     * Map constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ResourceMap $resource
     * @param Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ResourceMap $resource,
        Collection $resourceCollection,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }
}
