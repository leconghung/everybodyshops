<?php
namespace Magesales\VtigerCrm\Model\ResourceModel\Map;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * ID Field Name
     *
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'magesales_vtigercrm_map_collection';

    /**
     * Event object
     *
     * @var string
     */
    protected $_eventObject = 'map_collection';

    /**
     * Initialize resource
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magesales\VtigerCrm\Model\Map', 'Magesales\VtigerCrm\Model\ResourceModel\Map');
    }
}
