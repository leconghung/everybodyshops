<?php
namespace Magesales\VtigerCrm\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Map
 * @package Magesales\VtigerCrm\Model\ResourceModel
 */
class Map extends AbstractDb
{
    /**
     * Set main entity table name and primary key field name
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('magesales_vtigercrm_map', 'id');
    }
}
