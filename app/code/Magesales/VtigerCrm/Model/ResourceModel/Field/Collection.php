<?php
namespace Magesales\VtigerCrm\Model\ResourceModel\Field;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Initialize resource
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Magesales\VtigerCrm\Model\Field', 'Magesales\VtigerCrm\Model\ResourceModel\Field');
    }
}
