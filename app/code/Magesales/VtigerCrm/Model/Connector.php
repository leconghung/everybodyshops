<?php

namespace Magesales\VtigerCrm\Model;

use Magento\Config\Model\ResourceModel\Config;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Store\Model\ScopeInterface;

/**
 * Connect to VtigerCrm using API
 *
 * Class Connector
 * @package Magesales\VtigerCrm\Model
 */
class Connector
{
    const XML_PATH_VTIGER_CONFIG_USERNAME = 'vtigercrm/vtigerconfig/vtigerusername';
    const XML_PATH_VTIGER_CONFIG_URL = 'vtigercrm/vtigerconfig/vtigerurl';
    const XML_PATH_VTIGER_CONFIG_ACCESSKEY = 'vtigercrm/vtigerconfig/vtigeraccesskey';

    const POST = "POST";
    const GET = "GET";

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $_httpClientFactory;

    /**
     * Core Config Data
     *
     * @var $_scopeConfig \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var string
     */
    protected $token;

    /**
     * @var int
     */
    protected $expiredTime = 300;

    /**
     * @var
     */
    protected $tokenExpiredTime;

    /**
     * @var string
     */
    protected $sessionName;

    /** @var string */
    protected $assignedUserId;

    /**
     * Connector constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Config $resourceConfig
     * @param ZendClientFactory $httpClientFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Config $resourceConfig,
        ZendClientFactory $httpClientFactory
    )
    {
        $this->_scopeConfig = $scopeConfig;
        $this->_resourceConfig = $resourceConfig;
        $this->_httpClientFactory = $httpClientFactory;
        $this->_username = $this->_scopeConfig->getValue(
            self::XML_PATH_VTIGER_CONFIG_USERNAME,
            ScopeInterface::SCOPE_STORE
        );
        $this->_url = $this->_scopeConfig->getValue(self::XML_PATH_VTIGER_CONFIG_URL, ScopeInterface::SCOPE_STORE);
        $this->_accessKey = $this->_scopeConfig->getValue(
            self::XML_PATH_VTIGER_CONFIG_ACCESSKEY,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function challenge()
    {
        $params = "operation=getchallenge&username=$this->_username";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url . "?" . $params);
        $resp = $client->request()->getBody();
        $response = json_decode($resp, true);
        if (isset($response['result']['token'])) {
            $expiredTime = ($this->expiredTime - 1);
            $this->token = $response['result']['token'];
            $this->tokenExpiredTime = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s')) + $expiredTime);
        }

        return $response;
    }

    /**
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function login()
    {
        if (isset($this->token) && date('Y-m-d H:i:s') <= $this->tokenExpiredTime) {
            $token = $this->token;
        } else {
            $challenge = $this->challenge();
            $token = $challenge['result']['token'];
        }
        $accessKey = md5($token . $this->_accessKey);
        $params = "operation=login&username=$this->_username&accessKey=$accessKey";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url);
        $client->setRawData($params);
        $resp = $client->request(self::POST)->getBody();
        $response = json_decode($resp, true);
        $this->sessionName = $response['result']['sessionName'];
        $this->assignedUserId = $response['result']['userId'];
        return $response;
    }

    /**
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function logout()
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $params = "operation=logout&sessionName=$sessionName";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url);
        $client->setRawData($params);
        $resp = $client->request(self::POST)->getBody();
        $response = json_decode($resp, true);

        return $response;
    }

    /**
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function listtype()
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $params = "operation=listtypes&sessionName=$sessionName";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url . "?" . $params);
        $resp = $client->request()->getBody();
        $response = json_decode($resp, true);

        return $response;
    }

    /**
     * @param string $type
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function describe($type = "Leads")
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $params = "operation=describe&sessionName=$sessionName&elementType=$type";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url . "?" . $params);
        $resp = $client->request()->getBody();
        $response = json_decode($resp, true);

        return $response;
    }

    /**
     * @param $moduleName
     * @param $element
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function create($moduleName, $element)
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
            $element['assigned_user_id'] = $this->assignedUserId;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
            $element['assigned_user_id'] = $login['result']['userId'];
        }
        $element = json_encode($element);
        $params = "operation=create&sessionName=$sessionName&element=$element&elementType=$moduleName";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url);
        $client->setRawData($params);
        $resp = $client->request(self::POST)->getBody();
        $response = json_decode($resp, true);

        return $response;
    }

    /**
     * @param $id
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function retrieve($id)
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $params = "operation=retrieve&sessionName=$sessionName&id=$id";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url . "?" . $params);
        $resp = $client->request()->getBody();
        $response = json_decode($resp, true);

        return $response;
    }

    /**
     * @param $element
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function update($element)
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $element = json_encode($element);
        $params = "operation=update&sessionName=$sessionName&element=$element";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url);
        $client->setRawData($params);
        $resp = $client->request(self::POST)->getBody();
        $response = json_decode($resp, true);

        return $response;
    }

    /**
     * @param $type
     * @param $field
     * @param $value
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function query($type, $field, $value)
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $query = "SELECT * FROM " . $type . " WHERE " . $field . " = '" . $value . "';";
        $queryRecord = urlencode($query);
        $params = "sessionName=$sessionName&operation=query&query=$queryRecord";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url . "?" . $params);
        $resp = $client->request()->getBody();
        $response = json_decode($resp, true);

        return $response;
    }

    /**
     * @param $id
     * @return array|mixed|object
     * @throws \Zend_Http_Client_Exception
     */
    public function delete($id)
    {
        if (isset($this->sessionName)) {
            $sessionName = $this->sessionName;
        } else {
            $login = $this->login();
            $sessionName = $login['result']['sessionName'];
        }
        $params = "operation=delete&sessionName=$sessionName&id=$id";
        $client = $this->_httpClientFactory->create();
        $client->setUri($this->_url);
        $client->setRawData($params);
        $resp = $client->request(self::POST)->getBody();
        $response = json_decode($resp, true);

        return $response;
    }
}
