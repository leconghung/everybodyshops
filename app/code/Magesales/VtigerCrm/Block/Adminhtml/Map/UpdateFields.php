<?php
/**
 * Copyright © 2015 Magesales. All rights reserved.
 * See COPYING.txt for license details.
 
 */
namespace Magesales\VtigerCrm\Block\Adminhtml\Map;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;

class UpdateFields extends Template
{

    /**
     * @param Context $context
     * @param array $data
     */
    public function __construct(
        Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * Get Url retrieve
     *
     * @return string
     */
    public function getUpdateUrl()
    {
        return $this->getUrl('vtigercrm/field/retrieve', ['_current' => false]);
    }

    /**
     * Get Url Update All Fields
     *
     * @return string
     */
    public function getUpdateAllFields()
    {
        return $this->getUrl('vtigercrm/field/update', ['_current' => false]);
    }
}
