<?php
/**
 * Copyright © 2015 Magesales. All rights reserved.
 * See COPYING.txt for license details.
 *
 */
namespace Magesales\VtigerCrm\Block\Adminhtml\Map\Edit;

use Magesales\VtigerCrm\Model\FieldFactory;
use Magesales\VtigerCrm\Model\Status;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;

class Form extends Generic
{
    /**
     * Status
     *
     * @var \Magesales\VtigerCrm\Model\Status
     */
    protected $_status;

    /**
     * Type of mapping , it can be Leads,Contacts...
     * @var  string
     */
    protected $_type;

    /**
     * @var \Magesales\VtigerCrm\Model\FieldFactory
     */
    protected $_fieldFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magesales\VtigerCrm\Model\FieldFactory $fieldFactory
     * @param \Magesales\VtigerCrm\Model\Status $status
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        FieldFactory $fieldFactory,
        Status $status,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->_fieldFactory = $fieldFactory;
        $this->_status = $status;
    }

    /**
     * Prepare edit review form
     *
     * @return $this
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('vtiger_mapping');
        $_model = $this->_fieldFactory->create();
        $_type = $_model->changeFields();
        $isElementDisabled = false;
        $vtigerFields = [];
        $magentoFields = [];

        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Mapping  Details')]);
        if ($model->getId()) {
            $type = $model->getType();
            $vtigerFields = $_model->getVtigerFields($type);
            $table = $_model->getAllTable();
            $m_table = $table[$type];
            $magentoFields = $_model->getMagentoFields($m_table);
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
            $isElementDisabled = true;
        }

        $fieldset->addField(
            'type',
            'select',
            [
                'label' => __('Type'),
                'required' => true,
                'name' => 'type',
                'options' => $_type,
                'disabled' => $isElementDisabled,
            ]
        );
        $fieldset->addField(
            'magento_field',
            'select',
            [
                'label' => __('Magento Field'),
                'required' => true,
                'name' => 'magento_field',
                'data-role' => 'magento_field',
                'values' => $magentoFields
            ]
        );
        $fieldset->addField(
            'vtiger_field',
            'select',
            [
                'label' => __('Vtiger Field'),
                'required' => true,
                'name' => 'vtiger_field',
                'data-role' => 'vtiger_field',
                'values' => $vtigerFields
            ]
        );
        $fieldset->addField(
            'description',
            'textarea',
            [
                'name' => 'description',
                'title' => __('Description'),
                'label' => __('Description'),
                'maxlength' => '255',
                'required' => false
            ]
        );
        $fieldset->addField(
            'status',
            'select',
            [
                'label' => __('Status'),
                'required' => true,
                'name' => 'status',
                'values' => $this->_status->getOptionArray(),
            ]
        );
        $form->setUseContainer(true);
        $form->setValues($model->getData());
        $this->setForm($form);
        $form->setAction($this->getUrl('vtigercrm/map/save'));
        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');
        return parent::_prepareForm();
    }
}
