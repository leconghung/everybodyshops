define([
    "jquery",
    "Magento_Ui/js/lib/core/class",
    "Magesales_VtigerCrm/js/fields",
    "underscore"
], function ($, Class, Fields, _) {
    "use strict";
    return Class.extend({
        defaults: {
            /**
             * Initialized solutions
             */
            url: '',
            config: {'lead': 'Leads'},
            /**
             * The elements of created solutions
             */
            solutionsElements: {},
            /**
             * The selector element responsible for configuration of payment method (CSS class)
             */
            buttonRefresh: '.button.action-refresh'
        },

        /**
         * Constructor
         */
        initialize: function (url) {
            this.initConfig(url)
                .initMappings(url);
            return this;
        },
        /**
         * Initialization and configuration solutions
         */
        initMappings: function (url) {
            var url = url.url;
            $('#type').change(function () {
                var type = $(this).val();
                var data = {'type': type};
                $.ajax({
                    type: "POST",
                    url: url,
                    data: data,
                    showLoader: true,
                    success: function (response) {
                        $('[data-role="vtiger_field"]').html(response.vtiger_options);
                        $('[data-role="magento_field"]').html(response.magento_options);
                    }
                });
            });
            return this;
        }

    });
});
