<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magesales\VtigerCrm\Observer\Customer;

use Magesales\VtigerCrm\Model\Data as Data;
use Magesales\VtigerCrm\Model\Sync\Account as Account;
use Magesales\VtigerCrm\Model\Sync\Contact as Contact;
use Magesales\VtigerCrm\Model\Sync\Lead as Lead;
use Magento\Framework\App\Config\ScopeConfigInterface as ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface as ScopeInterface;

/**
 * Class Update
 */
class Register implements ObserverInterface
{
    /**
     * Core Config Data
     *
     * @var $_scopeConfig \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magesales\VtigerCrm\Model\Sync\Account
     */
    protected $_account;

    /**
     * @var \Magesales\VtigerCrm\Model\Sync\Lead
     */
    protected $_lead;

    /**
     * @var \Magesales\VtigerCrm\Model\Sync\Contact
     */
    protected $_contact;

    /**
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magesales\VtigerCrm\Model\Sync\Account $account
     * @param \Magesales\VtigerCrm\Model\Sync\Lead $lead
     * @param \Magesales\VtigerCrm\Model\Sync\Contact $contact
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Account $account,
        Lead $lead,
        Contact $contact
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_account = $account;
        $this->_lead = $lead;
        $this->_contact = $contact;
    }

    /**
     * Admin/Cutomer edit information address
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Customer\Model\Customer $customer */
        $customer = $observer->getEvent()->getCustomer();
        $id = $customer->getId();
        if ($this->_scopeConfig->isSetFlag(Data::XML_PATH_ALLOW_SYNC_CONTACT, ScopeInterface::SCOPE_STORE)) {
            $this->_contact->sync($id);
        }
        if ($this->_scopeConfig->isSetFlag(Data::XML_PATH_ALLOW_SYNC_LEAD, ScopeInterface::SCOPE_STORE)) {
            $this->_lead->sync($id);
        }
        if ($this->_scopeConfig->isSetFlag(Data::XML_PATH_ALLOW_SYNC_ACCOUNT, ScopeInterface::SCOPE_STORE)) {
            $this->_account->sync($id);
        }
    }
}
