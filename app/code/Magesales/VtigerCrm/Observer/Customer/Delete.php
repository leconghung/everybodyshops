<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magesales\VtigerCrm\Observer\Customer;

use Magesales\VtigerCrm\Model\Data as Data;
use Magesales\VtigerCrm\Model\Sync\Account as SyncAccount;
use Magesales\VtigerCrm\Model\Sync\Contact as SyncContact;
use Magesales\VtigerCrm\Model\Sync\Lead as SyncLead;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Delete
 */
class Delete implements ObserverInterface
{
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var SyncLead
     */
    protected $_lead;

    /**
     * @var SyncContact
     */
    protected $_contact;

    /**
     * @var SyncAccount
     */
    protected $_account;

    /**
     * Delete constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param SyncLead $lead
     * @param SyncContact $contact
     * @param SyncAccount $account
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        SyncLead $lead,
        SyncContact $contact,
        SyncAccount $account
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_lead = $lead;
        $this->_contact = $contact;
        $this->_account = $account;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var \Magento\Customer\Model\Customer $customer */
        $email = $observer->getEvent()->getCustomer()->getEmail();
        if ($this->_scopeConfig->isSetFlag(Data::XML_PATH_ALLOW_SYNC_LEAD, ScopeInterface::SCOPE_STORE)) {
            $this->_lead->deleteLead($email);
        }
        if ($this->_scopeConfig->isSetFlag(Data::XML_PATH_ALLOW_SYNC_CONTACT, ScopeInterface::SCOPE_STORE)) {
            $this->_contact->deleteContact($email);
        }
        if ($this->_scopeConfig->isSetFlag(Data::XML_PATH_ALLOW_SYNC_ACCOUNT, ScopeInterface::SCOPE_STORE)) {
            $this->_account->deleteAccount($email);
        }
    }
}
