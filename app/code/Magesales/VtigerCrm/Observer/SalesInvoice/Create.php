<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magesales\VtigerCrm\Observer\SalesInvoice;

use Magesales\VtigerCrm\Model\Data;
use Magesales\VtigerCrm\Model\Sync\Invoice as SyncInvoice;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Create
 */
class Create implements ObserverInterface
{
    /**
     * Core Config Data
     *
     * @var $_scopeConfig \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magesales\VtigerCrm\Model\Sync\Invoice
     */
    protected $_invoice;

    /**
     * Create constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param SyncInvoice $invoice
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        SyncInvoice $invoice
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_invoice = $invoice;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $increment_id = $observer->getEvent()->getInvoice()->getIncrementId();
        $this->_invoice->sync($increment_id);
        if ($this->_scopeConfig->isSetFlag(Data::XML_PATH_ALLOW_SYNC_INVOICE, ScopeInterface::SCOPE_STORE)) {
            $this->_invoice->sync($increment_id);
        }
    }
}
