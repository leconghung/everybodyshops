<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magesales\VtigerCrm\Observer\SalesOrder;

use Magesales\VtigerCrm\Model\Data;
use Magesales\VtigerCrm\Model\Sync\SalesOrder as SyncSalesOrder;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Create
 */
class Create implements ObserverInterface
{
    /**
     * Core Config Data
     *
     * @var $_scopeConfig \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magesales\VtigerCrm\Model\Sync\SalesOrder
     */
    protected $_order;

    /**
     * Create constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param SyncSalesOrder $order
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        SyncSalesOrder $order
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_order = $order;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $increment_id = $observer->getEvent()->getOrder()->getIncrementId();
        if ($this->_scopeConfig->isSetFlag(Data::XML_PATH_ALLOW_SYNC_ORDER, ScopeInterface::SCOPE_STORE)) {
            $this->_order->sync($increment_id);
        }
    }
}
