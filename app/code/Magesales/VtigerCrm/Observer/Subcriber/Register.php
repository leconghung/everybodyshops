<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magesales\VtigerCrm\Observer\Subcriber;

use Magesales\VtigerCrm\Model\Data as Data;
use Magesales\VtigerCrm\Model\Sync\Subcriber as SyncSubcriber;
use Magento\Framework\App\Config\ScopeConfigInterface as ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface as ScopeInterface;

/**
 * Class Update
 */
class Register implements ObserverInterface
{
    /**
     * Core Config Data
     *
     * @var $_scopeConfig \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magesales\VtigerCrm\Model\Sync\Subcriber
     */
    protected $_subcriber;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        SyncSubcriber $subcriber
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_subcriber = $subcriber;
    }

    /**
     * Admin/Cutomer edit information address
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $email = $observer->getEvent()->getSubscriber()->getEmail();
        if ($this->_scopeConfig->isSetFlag(Data::XML_PATH_ALLOW_SYNC_SUBCRIBER, ScopeInterface::SCOPE_STORE)) {
            $this->_subcriber->sync($email);
        }
    }
}
