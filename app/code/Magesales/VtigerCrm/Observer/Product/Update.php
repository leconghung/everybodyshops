<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magesales\VtigerCrm\Observer\Product;

use Magesales\VtigerCrm\Model\Data as Data;
use Magesales\VtigerCrm\Model\Sync\Product as SyncProduct;
use Magento\Framework\App\Config\ScopeConfigInterface as ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface as ScopeInterface;

/**
 * Class Update
 */
class Update implements ObserverInterface
{
    /**
     * Core Config Data
     *
     * @var $_scopeConfig \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magesales\VtigerCrm\Model\Sync\Product
     */
    protected $_product;

    /**
     * Update constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param SyncProduct $product
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        SyncProduct $product
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_product = $product;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $id = $product->getId();
        if ($this->_scopeConfig->isSetFlag(Data::XML_PATH_ALLOW_SYNC_PRODUCT, ScopeInterface::SCOPE_STORE)) {
            $this->_product->sync($id);
        }
    }
}
