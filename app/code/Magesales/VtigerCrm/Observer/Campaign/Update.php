<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magesales\VtigerCrm\Observer\Campaign;

use Magesales\VtigerCrm\Model\Data;
use Magesales\VtigerCrm\Model\Sync\Campaign;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Update
 */
class Update implements ObserverInterface
{
    /**
     * Core Config Data
     *
     * @var $_scopeConfig \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * @var \Magesales\VtigerCrm\Model\Sync\Campaign
     */
    protected $_campaign;

    /**
     * Update constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param Campaign $campaign
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Campaign $campaign
    ) {
        $this->_scopeConfig = $scopeConfig;
        $this->_campaign = $campaign;
    }

    /**
     * Admin edit Catalog Rule Price
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        $id = $observer->getEvent()->getRule()->getId();
        if ($this->_scopeConfig->isSetFlag(Data::XML_PATH_ALLOW_SYNC_CAMPAIGN, ScopeInterface::SCOPE_STORE)) {
            $this->_campaign->sync($id);
//            return;
        }
    }
}
