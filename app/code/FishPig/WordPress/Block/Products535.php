<?php
namespace FishPig\WordPress\Block;

use Magento\Catalog\Model\Product\Visibility;
use Magento\Framework\ObjectManagerInterface;

class Products535 extends \Magento\Framework\View\Element\Template
{
    protected $_categoryFactory;
	protected $_productCollectionFactory;
	protected $_productStatus;
	protected $_productVisibility;
	protected $_objectManager;
	
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
		ObjectManagerInterface $objectManager,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
		\Magento\Catalog\Model\Product\Visibility $productVisibility,
		\Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
		
		array $data = []
    )
    {    
        $this->_categoryFactory = $categoryFactory;
		$this->_productCollectionFactory = $productCollectionFactory;
		$this->_productStatus = $productStatus;
		$this->_productVisiblity = $productVisibility;
        $this->_objectManager = $objectManager;
		
		
		parent::__construct($context);
    }
 

    public function getCategory($categoryId) 
    {
        $category = $this->_categoryFactory->create();
        $category->load($categoryId);
        return $category;
    }
    
    public function getCategoryProducts($categoryId) 
    {
        $products = $this->getCategory($categoryId)->getProductCollection();
        $products->addAttributeToSelect('*');	
        return $products;
    }
	public function getCategoryIDbyTitle($categoryTitle)
	{	
		$categoryId = 0;
		$collection = $this->_categoryFactory->create()->getCollection()->addAttributeToFilter('name',$categoryTitle)->setPageSize(1);
		
		if ($collection->getSize()) {
			$categoryId = $collection->getFirstItem()->getId();
		}
		
		return $categoryId;
	}
	public function getCategoryProductsRand($categoryId)
	{
        $collection = $this->_productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
		$collection->addCategoriesFilter(array('in' => $categoryId));
		$collection->addUrlRewrite($categoryId);
    	$collection->addAttributeToFilter('status', ['in' => $this->_objectManager->get('\Magento\Catalog\Model\Product\Attribute\Source\Status')->getVisibleStatusIds()]);
		$collection->setVisibility($this->_objectManager->get('\Magento\Catalog\Model\Product\Visibility')->getVisibleInCatalogIds());		
		$collection->setPageSize(12); // fetching only 3 products
		$collection->getSelect()->orderRand();

		
        return $collection;
		
	}
	
}
?>