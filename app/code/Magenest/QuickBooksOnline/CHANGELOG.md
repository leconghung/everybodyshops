#Change Log#

All notable changes to this extension will be documented in this file.
This extension adheres to [Magenest](http://magenest.com/).

[2.2.0] - 2017-08-29
### Added
*   Compatible with Magento 2.1 and Magento 2.2
*   Allow synchronizing and updating All Product from QuickBooks Online store into Magento 2.
*   Allow synchronizing and updating All Customer from QuickBooks Online store into Magento 2.
*   Allow synchronizing and updating All Orders from QuickBooks Online store into Magento 2.
*   Add mapping tax
*   Fix Bug
[2.1.1] - 2016-03-15
### Added
*   Allow synchronizing and updating All Product from Magento 2 store into QuickBooks Online.
*   Allow synchronizing and updating All Customer from Magento 2 store into QuickBooks Online.
*   Allow synchronizing and updating All Orders from Magento 2 store into QuickBooks Online.
*   Add queue mode.
*   Allow choosing auto sync or add to queue sync.
*   Add Cron job mode.
*   Improve interface.

### Fixed bug

*   Fixed some bugs

[2.1.0] - 2016-06-23
### Added

*   Allow synchronizing and updating Customer from Magento 2 store into QuickBooks Online.
*   Allow synchronizing and updating Product from Magento 2 store into QuickBooks Online.
*   Allow synchronizing and updating Order from Magento 2 store into QuickBooks Online.
*   Allow synchronizing and updating Invoice from Magento 2 store into QuickBooks Online.

