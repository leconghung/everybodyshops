<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\QuickBooksOnline\Model;

use Magenest\QuickBooksOnline\Helper\Oauth as OauthHelper;
use Magento\Catalog\Model\Product\Exception;
use Magento\Framework\HTTP\ZendClientFactory;
use Magento\Config\Model\Config as ConfigModel;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Client
 *
 * @package Magenest\QuickBooksOnline\Model
 */
class Client
{
    
    /**#@+
     * Constants
     */
    const URL_QBO_RECONNECT = 'https://appcenter.intuit.com/api/v1/Connection/Reconnect';
    const URL_QBO_DISCONNECT = 'https://appcenter.intuit.com/api/v1/Connection/Disconnect';
    const URL_APP_MENU          = 'https://appcenter.intuit.com/api/v1/Account/AppMenu';
    const URL_REQUEST_PRODUCTION_LINK = 'https://quickbooks.api.intuit.com/v3/company/';
    const URL_REQUEST_SANDBOX_LINK = 'https://sandbox-quickbooks.api.intuit.com/v3/company/';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var OauthHelper
     */
    protected $_oauthHelper;

    /**
     * @var ZendClientFactory
     */
    protected $_httpClientFactory;

    /**
     * @var Oauth
     */
    protected $oauthModel;

    /**
     * Client constructor.
     *
     * @param ZendClientFactory $httpClientFactory
     * @param Oauth $oauthModel
     * @param ScopeConfigInterface $scopeConfig
     * @param OauthHelper $oauthHelper
     */
    public function __construct(
        ZendClientFactory $httpClientFactory,
        Oauth $oauthModel,
        ScopeConfigInterface $scopeConfig,
        OauthHelper $oauthHelper
    ) {
        $this->_httpClientFactory = $httpClientFactory;
        $this->oauthModel         = $oauthModel;
        $this->_oauthHelper       = $oauthHelper;
        $this->scopeConfig        = $scopeConfig;
    }

    /**
     * @return \Magento\Framework\HTTP\ZendClient
     */
    public function getZendClient()
    {
        return $this->_httpClientFactory->create();
    }

    /**
     * Using ZendClient to request authenticate to QBO
     *
     * @param $url
     * @param $params
     * @return string
     * @throws \Zend_Http_Client_Exception
     */
    public function request($url, $params)
    {
        $this->_oauthHelper->sign($url, $params);
        $desUrl = $this->_oauthHelper->getDestinationUrl();
        $client = $this->getZendClient();
        $client->setUri($desUrl);
        $client->setConfig([
            'timeout' => 300
        ]);

        return $client->request()->getBody();
    }

    /**
     * Send Request to QBO
     *
     * @param string $method
     * @param $path
     * @param array $params
     * @return mixed|string
     * @throws LocalizedException
     * @throws \Zend_Http_Client_Exception
     */
    public function sendRequest($method, $path, $params = [])
    {
        $url = $this->getRequestUrl($path);
        $oauthHelper = $this->_oauthHelper->setMethod($method);
        $oauthHelper->sign($url, $this->getAccessTokenParameter());
        $client = $this->getZendClient()->setUri($url);

        $header = [
            "Authorization: ". $oauthHelper->getHeaders(),
            "Content-type: application/json",
            "Accept: application/json",
        ];
        $client->setHeaders($header);
        $client->setConfig(['timeout' => 300]);

        if (!empty($params)) {
            $dataBody = json_encode($params);
            if ($path == 'taxservice/taxcode') {
                $string1 = str_replace('"TaxRateDetails":', '"TaxRateDetails":[', $dataBody);
                $string2 = str_replace('"Sales"}', '"Sales"}]', $string1);
                $client->setRawData($string2);
            } else {
                $client->setRawData($dataBody);
            }
        }
        $response = $client->request($method)->getBody();
        $responseArray = json_decode($response, true);
        if ($client->getLastResponse()->getStatus() >= 400) {
            $errorMsg = __($response);
            if (isset($responseArray['Fault']['Error'][0]['Detail'])) {
                $errorMsg = __($responseArray['Fault']['Error'][0]['Detail']);
            }
            throw new LocalizedException($errorMsg);
        }

        return $responseArray;
    }

    /**
     * Request Url
     *
     * @param $path
     * @return string
     */
    protected function getRequestUrl($path)
    {
        $isSandbox = $this->scopeConfig->isSetFlag(Config::XML_PATH_QBONLINE_APP_MODE);
        if ($isSandbox) {
            $url = self::URL_REQUEST_SANDBOX_LINK;
        } else {
            $url = self::URL_REQUEST_PRODUCTION_LINK;
        }

        $url = rtrim($url, '/') . '/' . $this->getCompanyId() . '/' . ltrim($path, '/');
        return $url;
    }

    /**
     * Company Id
     *
     * @return string
     */
    protected function getCompanyId()
    {
        $model = $this->oauthModel->getCurrentConnection();
        
        return $model->getQbRealm();
    }

    /**
     * Get Access Token from database
     *
     * @return array
     * @throws LocalizedException
     */
    protected function getAccessTokenParameter()
    {
        $model = $this->oauthModel->getCurrentConnection();
        $token       = $model->getOauthAccessToken();
        $tokenSecret = $model->getOauthAccessTokenSecret();
        if (!$token || !$tokenSecret) {
            throw new LocalizedException(
                __('We can\'t get the access token in the database')
            );
        }
        $parameter = [
            'oauth_token' => $token,
            'oauth_secret' => $tokenSecret
        ];

        return $parameter;
    }

    /**
     * Expired Access Token
     *
     * @return bool
     */
    public function isExpiredToken()
    {
        $model = $this->oauthModel->getCurrentConnection();
        $accessDatetime = $model->getAccessDatetime();
        $timestamp = new \DateTime($accessDatetime);
        if (($timestamp->getTimestamp() + 15552000 ) < time()) {
            return true;
        }

        return false;
    }

    public function reconnect()
    {
        //TODO
    }

    public function disconnect()
    {
        $this->request(self::URL_QBO_DISCONNECT, $this->getAccessTokenParameter());
    }

    public function widgetMenu()
    {
        try {
            return $this->request(self::URL_APP_MENU, $this->getAccessTokenParameter());
        } catch (\Exception $e) {
            return '';
        }
    }
}
