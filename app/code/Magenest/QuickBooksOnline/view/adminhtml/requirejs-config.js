/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {

    paths: {
        'intuit.ipp.anywhere': 'https://appcenter.intuit.com/Content/IA/intuit.ipp.anywhere'
    },
    shim:{
        'intuit.ipp.anywhere':{
            'deps':['jquery']
        }
    }
};
