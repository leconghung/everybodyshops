<?php
/**
 * Copyright © 2017 Magenest. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magenest\QuickBooksOnline\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magenest\QuickBooksOnline\Model\Config\Source\AppMode as AppModeSource;
use Magenest\QuickBooksOnline\Model\Config;

/**
 * Class Mode
 *
 * @package Magenest\QuickBooksOnline\Helper
 */
class Mode
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var int
     */
    protected $mode;

    /**
     * Parameters constructor.
     * @param ScopeConfigInterface $scopeConfig
     * @param RequestInterface $request
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        RequestInterface $request
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->request = $request;
    }

    /**
     * @return int
     */
    protected function getMode()
    {
        if ($mode = $this->scopeConfig->getValue(Config::XML_PATH_QBONLINE_APP_MODE)) {
            $this->mode = $mode;
        }
        
        $modeParam = (int)$this->request->getParam('qbo_mode');
        if ($modeParam > 0) {
            $this->mode = $modeParam;
        }

        return $this->mode;
    }

    /**
     * @return mixed
     */
    public function getWebsiteId()
    {
        return $this->request->getParam('website');
    }

    /**
     * @return string
     */
    public function getApplicationToken()
    {
        if ($this->getMode() == AppModeSource::PRODUCTION_MODE) {
            return $this->getProductionApplicationToken();
        } else {
            return $this->getSandboxApplicationToken();
        }
    }

    /**
     * @return string
     */
    public function getConsumerKey()
    {
        if ($this->getMode() == AppModeSource::PRODUCTION_MODE) {
            return $this->getProductionConsumerKey();
        } else {
            return $this->getSandboxConsumerKey();
        }
    }
    
    /**
     * @return string
     */
    public function getConsumerSecret()
    {
        if ($this->getMode() == AppModeSource::PRODUCTION_MODE) {
            return $this->getProductionConsumerSecret();
        } else {
            return $this->getSandboxConsumerSecret();
        }
    }

    /**
     * @param $path
     * @return mixed
     */
    protected function getConfig($path)
    {
        return $this->scopeConfig->getValue($path);
    }

    /**
     * @return string
     */
    protected function getProductionApplicationToken()
    {
        return $this->getConfig(Config::XML_PATH_PRODUCTION_APPLICATION_TOKEN);
    }

    /**
     * @return string
     */
    protected function getProductionConsumerKey()
    {
        return $this->getConfig(Config::XML_PATH_PRODUCTION_CONSUMER_KEY);
    }

    /**
     * @return string
     */
    protected function getProductionConsumerSecret()
    {
        return $this->getConfig(Config::XML_PATH_PRODUCTION_CONSUMER_SECRET);
    }

    /**
     * @return string
     */
    protected function getSandboxApplicationToken()
    {
        return $this->getConfig(Config::XML_PATH_SANDBOX_APPLICATION_TOKEN);
    }

    /**
     * @return string
     */
    protected function getSandboxConsumerKey()
    {
        return $this->getConfig(Config::XML_PATH_SANDBOX_CONSUMER_KEY);
    }

    /**
     * @return string
     */
    protected function getSandboxConsumerSecret()
    {
        return $this->getConfig(Config::XML_PATH_SANDBOX_CONSUMER_SECRET);
    }
}
