<?php
/* Class additions to body before export of data. - SMY*/
function pn_body_class_add_categories( $classes ) {
 
	// Only proceed if we're on a single post page
	if ( !is_single() )
		return $classes;
 
	// Get the categories that are assigned to this post
	$post_categories = get_the_category();
 
	// Loop over each category in the $categories array
	foreach( $post_categories as $current_category ) {
 
		// Add the current category's slug to the $body_classes array
		$classes[] = 'category-' . $current_category->slug;
 
	}
 
	// Finally, return the $body_classes array
	return $classes;
}
add_filter( 'body_class', 'pn_body_class_add_categories' );




/*OR*/

function add_category_name($classes = '') {
   if(is_single()) {
      $category = get_the_category();
      $classes[] = 'category-'.$category[0]->slug; 
   }
   return $classes;
}
add_filter('body_class','add_category_name');