<?php

class VTWebservice
{
    public $_vtigerUrl = '';
    public $_accessKey = '';
    public $_userName = 'admin';
    public $_sessionName = '';

    function VTWebservice($vtigerurl, $username, $accesskey)
    {
        $this->_vtigerUrl = $this->processVtigerUrl($vtigerurl);
        $this->_accessKey = $accesskey;
        $this->_userName = $username;
        $this->getChallenge();
    }

    function processVtigerUrl($url){
        //Set connection url
        if(substr($url, -1) != '/') {
            $url .= '/';
        }
        return $url;
    }

    function getOperation($url, $resfunc = '')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $res = curl_exec($ch);
        curl_close($ch);
        if ($resfunc != '') {
            $this->$resfunc($res);
        } else {
            return $res;
        }
    }

    function postOperation($url, $params, $resfunc = '')
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        $res = curl_exec($ch);
        curl_close($ch);
        if ($resfunc != '') {
            $this->$resfunc($res);
        } else {
            return $res;
        }
    }

    function getChallenge()
    {
        $url = $this->_vtigerUrl . "webservice.php?operation=getchallenge&username=" . $this->_userName;
        $this->getOperation($url, 'challengeResponse');
    }

    function challengeResponse($res)
    {
        $res = json_decode($res, true);
        $token = $res['result']['token'];
        $hash = md5($token . $this->_accessKey);
        $params = array('operation' => 'login', 'username' => $this->_userName, 'accessKey' => $hash);
        $url = $this->_vtigerUrl . "webservice.php";
        $this->postOperation($url, $params, 'loginResponse');
    }

    function loginResponse($res)
    {
        $res = json_decode($res, true);
        if ($res['success'] == 1) {
            $this->_sessionName = $res['result']['sessionName'];
        }
    }

    function create($moduleName, $data){
        $params = array(
            'operation'=>'create',
            'sessionName'=>$this->_sessionName,
            'element'=>json_encode($data),
            'elementType'=>$moduleName
        );
        if(empty($data)){
            return false;
        }

        return $this->postOperation($this->_vtigerUrl.'webservice.php', $params);
    }

    function query($query){
        if(empty($query)){
            return false;
        }
        $params = 'operation=query&sessionName='.$this->_sessionName.'&query='.urlencode($query);


        return $this->getOperation($this->_vtigerUrl.'webservice.php?'.$params);
    }
}